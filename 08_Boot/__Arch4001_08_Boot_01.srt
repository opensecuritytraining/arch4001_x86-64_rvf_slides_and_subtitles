1
00:00:00,320 --> 00:00:03,840
so while we've covered a lot of stuff in

2
00:00:01,839 --> 00:00:06,960
this class i just want to do a quick

3
00:00:03,840 --> 00:00:08,880
skim over how a general boot should work

4
00:00:06,960 --> 00:00:11,040
because it'll show you how much other

5
00:00:08,880 --> 00:00:12,880
stuff there really is to still know so

6
00:00:11,040 --> 00:00:14,559
to that end there's a nice document

7
00:00:12,880 --> 00:00:17,279
called the intel minimal architecture

8
00:00:14,559 --> 00:00:19,680
bootloader and i highly recommend you go

9
00:00:17,279 --> 00:00:21,520
check this out after this video because

10
00:00:19,680 --> 00:00:23,760
it gives you a sense of all the other

11
00:00:21,520 --> 00:00:25,359
stuff that we didn't cover in this class

12
00:00:23,760 --> 00:00:26,560
so let's you know see what we covered

13
00:00:25,359 --> 00:00:27,760
and what we didn't

14
00:00:26,560 --> 00:00:29,279
well it says that you know you have to

15
00:00:27,760 --> 00:00:31,279
power up and you have you know the

16
00:00:29,279 --> 00:00:33,600
16-bit real mode code starting at the

17
00:00:31,279 --> 00:00:35,200
reset vector great we covered that

18
00:00:33,600 --> 00:00:37,200
and then there's sort of an operating

19
00:00:35,200 --> 00:00:38,719
mode transition so it may transition

20
00:00:37,200 --> 00:00:40,800
from real mode to protective mode if it

21
00:00:38,719 --> 00:00:42,160
was a super legacy bios it might just

22
00:00:40,800 --> 00:00:44,079
stick around in real mode for a long

23
00:00:42,160 --> 00:00:45,760
time but for any modern bios we would

24
00:00:44,079 --> 00:00:47,280
expect it to transition from real mode

25
00:00:45,760 --> 00:00:48,239
to protective mode and we saw that as

26
00:00:47,280 --> 00:00:50,000
well

27
00:00:48,239 --> 00:00:51,600
then there's something like cpu

28
00:00:50,000 --> 00:00:54,320
microcode updates well we didn't talk

29
00:00:51,600 --> 00:00:57,199
about that at all right this microcode

30
00:00:54,320 --> 00:00:59,359
is a mechanism that intel cpu can use to

31
00:00:57,199 --> 00:01:01,280
kind of change the sort of micro

32
00:00:59,359 --> 00:01:03,520
operations that occur behind the scenes

33
00:01:01,280 --> 00:01:05,199
we know that these intel instructions

34
00:01:03,520 --> 00:01:07,360
are extremely complex and so they're

35
00:01:05,199 --> 00:01:10,080
generally made up of a bunch of smaller

36
00:01:07,360 --> 00:01:12,479
sub micro instructions and that's what

37
00:01:10,080 --> 00:01:14,960
the microcode is and so the bios the

38
00:01:12,479 --> 00:01:16,720
firmware is responsible for updating the

39
00:01:14,960 --> 00:01:18,720
microcode at boot time because if

40
00:01:16,720 --> 00:01:20,320
there's any sort of errata or fixes or

41
00:01:18,720 --> 00:01:22,000
anything like that you want to make sure

42
00:01:20,320 --> 00:01:24,000
that those are applied as soon as

43
00:01:22,000 --> 00:01:25,360
humanly possible so that you don't screw

44
00:01:24,000 --> 00:01:26,720
up the rest of your boot

45
00:01:25,360 --> 00:01:29,439
then there's things like setting up the

46
00:01:26,720 --> 00:01:31,680
cpu for caches ram or car

47
00:01:29,439 --> 00:01:33,600
at such time as you see the code

48
00:01:31,680 --> 00:01:36,320
transitioning to what you would consider

49
00:01:33,600 --> 00:01:38,400
traditional c code compiled by a normal

50
00:01:36,320 --> 00:01:40,640
compiler you can only do that if you

51
00:01:38,400 --> 00:01:41,759
have something like a stack right so a c

52
00:01:40,640 --> 00:01:43,360
compiler is going to assume that you

53
00:01:41,759 --> 00:01:45,280
have a stack well if you're just

54
00:01:43,360 --> 00:01:47,200
executing directly off the flash chip

55
00:01:45,280 --> 00:01:49,200
you don't have a stack you can't just

56
00:01:47,200 --> 00:01:51,439
read and write directly to the flash

57
00:01:49,200 --> 00:01:54,159
chip we saw the elaborate mechanism

58
00:01:51,439 --> 00:01:56,640
using the spy registers in order to

59
00:01:54,159 --> 00:01:58,479
write to the flash chip so cache as ram

60
00:01:56,640 --> 00:02:01,280
is a nice sort of workaround where

61
00:01:58,479 --> 00:02:04,079
basically the cpu cache which is

62
00:02:01,280 --> 00:02:05,680
trivially read writable is modified to

63
00:02:04,079 --> 00:02:07,600
act as if it was ram and then you set

64
00:02:05,680 --> 00:02:09,119
the instruction pointer into that area

65
00:02:07,600 --> 00:02:10,319
so that you know can just operate like a

66
00:02:09,119 --> 00:02:12,400
stack

67
00:02:10,319 --> 00:02:14,640
then there's a chipset initialization

68
00:02:12,400 --> 00:02:16,800
for things like the pci x bar for memory

69
00:02:14,640 --> 00:02:18,800
mapped i o there's also still a thing

70
00:02:16,800 --> 00:02:20,959
called the mch bar even on you know

71
00:02:18,800 --> 00:02:22,800
modern pch systems

72
00:02:20,959 --> 00:02:24,959
then the processor moves on to memory

73
00:02:22,800 --> 00:02:26,800
initialization which is setting up the

74
00:02:24,959 --> 00:02:28,640
memory controller itself

75
00:02:26,800 --> 00:02:30,000
oh hello

76
00:02:28,640 --> 00:02:31,840
all right so

77
00:02:30,000 --> 00:02:33,440
then after you have the memory

78
00:02:31,840 --> 00:02:36,000
controller initialized and so you can

79
00:02:33,440 --> 00:02:37,840
nominally access ram i potentially want

80
00:02:36,000 --> 00:02:39,120
to do a memory test this is something

81
00:02:37,840 --> 00:02:40,400
you might see if you you know boot a

82
00:02:39,120 --> 00:02:42,720
server you'll often see it taking

83
00:02:40,400 --> 00:02:44,560
forever to do a memory test because they

84
00:02:42,720 --> 00:02:45,840
have like a ton of memory

85
00:02:44,560 --> 00:02:48,239
and then

86
00:02:45,840 --> 00:02:51,280
firmware we typically want to copy from

87
00:02:48,239 --> 00:02:53,280
the spy flash chip out to ram because

88
00:02:51,280 --> 00:02:54,959
this is just a faster faster place to

89
00:02:53,280 --> 00:02:56,640
execute from right this execute in place

90
00:02:54,959 --> 00:02:58,159
from the flash is very slow you're

91
00:02:56,640 --> 00:03:00,720
continuously fetching assembly

92
00:02:58,159 --> 00:03:03,280
instructions from this you know 66

93
00:03:00,720 --> 00:03:04,959
megahertz bus or whatever and so in the

94
00:03:03,280 --> 00:03:06,640
document they call it firmware shadowing

95
00:03:04,959 --> 00:03:08,480
and so you want to copy it as soon as

96
00:03:06,640 --> 00:03:10,159
possible into memory there's other

97
00:03:08,480 --> 00:03:12,080
things like memory transaction

98
00:03:10,159 --> 00:03:13,200
redirection the pam registers this has

99
00:03:12,080 --> 00:03:15,440
to do with

100
00:03:13,200 --> 00:03:17,599
some certain low address space ranges

101
00:03:15,440 --> 00:03:19,920
can be you know redirected from memory

102
00:03:17,599 --> 00:03:22,000
to hardware then they're setting up the

103
00:03:19,920 --> 00:03:24,720
normal stack which actually points into

104
00:03:22,000 --> 00:03:26,879
normal dram and then you

105
00:03:24,720 --> 00:03:29,519
transition from executing in place out

106
00:03:26,879 --> 00:03:31,360
of flash to this copy of

107
00:03:29,519 --> 00:03:32,720
firmware that you've loaded into ram

108
00:03:31,360 --> 00:03:34,480
there's a bunch of miscellaneous

109
00:03:32,720 --> 00:03:36,239
platform enabling which is going to be

110
00:03:34,480 --> 00:03:38,480
dependent on the particular platform and

111
00:03:36,239 --> 00:03:40,799
that's why you know uh intel creates

112
00:03:38,480 --> 00:03:43,040
those documents called the the you know

113
00:03:40,799 --> 00:03:44,640
whatever skylake bios writer's guide

114
00:03:43,040 --> 00:03:46,000
that kind of thing that tells people

115
00:03:44,640 --> 00:03:48,159
running on a skylight system you know

116
00:03:46,000 --> 00:03:50,159
what they have to do to set up you know

117
00:03:48,159 --> 00:03:51,440
clocks and set up you know general

118
00:03:50,159 --> 00:03:53,439
purpose io

119
00:03:51,440 --> 00:03:55,840
then there's interrupt enabling the

120
00:03:53,439 --> 00:03:57,280
programmable interrupt controller local

121
00:03:55,840 --> 00:03:59,680
advanced programmable interrupt

122
00:03:57,280 --> 00:04:01,840
controller io advanced programmable

123
00:03:59,680 --> 00:04:03,680
interrupt controller and so if it was

124
00:04:01,840 --> 00:04:06,080
something like a legacy bios they might

125
00:04:03,680 --> 00:04:08,319
set up the interrupt vector table that

126
00:04:06,080 --> 00:04:10,239
was what's used for legacy bios to do

127
00:04:08,319 --> 00:04:12,000
things like reads and writes from the

128
00:04:10,239 --> 00:04:14,000
hard drive to read the master boot

129
00:04:12,000 --> 00:04:15,599
record or you know if it's going to be

130
00:04:14,000 --> 00:04:16,720
more of a protective mode code execution

131
00:04:15,599 --> 00:04:18,799
they might set up the interrupt

132
00:04:16,720 --> 00:04:19,680
descriptor table that is the same sort

133
00:04:18,799 --> 00:04:21,359
of thing we learned about in

134
00:04:19,680 --> 00:04:22,880
architecture 2001.

135
00:04:21,359 --> 00:04:24,800
then the firmware might want to do

136
00:04:22,880 --> 00:04:26,960
things like setting up timers the you

137
00:04:24,800 --> 00:04:29,120
know real-time clock the programmable

138
00:04:26,960 --> 00:04:30,880
interrupt timer the tico timer using it

139
00:04:29,120 --> 00:04:32,639
as a watchdog this can be you know

140
00:04:30,880 --> 00:04:34,800
watchdog would be for instance to know

141
00:04:32,639 --> 00:04:36,880
that like if this thing took too long to

142
00:04:34,800 --> 00:04:38,320
boot then it would just sort of stop and

143
00:04:36,880 --> 00:04:39,840
restart and you know maybe something

144
00:04:38,320 --> 00:04:41,840
went wrong maybe it was a transient or

145
00:04:39,840 --> 00:04:44,720
maybe it wasn't maybe they set something

146
00:04:41,840 --> 00:04:46,560
in you know the rtc the the cmos in

147
00:04:44,720 --> 00:04:48,960
order to you know keep track of how many

148
00:04:46,560 --> 00:04:50,639
failures there were to you know halt or

149
00:04:48,960 --> 00:04:52,240
do some alternate boot path if you know

150
00:04:50,639 --> 00:04:53,440
it was failing over and over again so

151
00:04:52,240 --> 00:04:54,560
the timers can be used for a lot of

152
00:04:53,440 --> 00:04:56,400
different things

153
00:04:54,560 --> 00:04:57,840
then there's the memory cache control

154
00:04:56,400 --> 00:04:59,680
which is important if you want to

155
00:04:57,840 --> 00:05:01,440
actually boot fast if you ever you know

156
00:04:59,680 --> 00:05:03,680
screw with these and don't set them up

157
00:05:01,440 --> 00:05:05,919
at boot time you will see that cache is

158
00:05:03,680 --> 00:05:07,199
very important you boot super super slow

159
00:05:05,919 --> 00:05:08,960
if you don't have that

160
00:05:07,199 --> 00:05:11,600
and you know we saw mtr's a little bit

161
00:05:08,960 --> 00:05:13,680
in the context of smrrs as a you know

162
00:05:11,600 --> 00:05:16,160
way the mtrs were modified for an attack

163
00:05:13,680 --> 00:05:18,160
on smm and smrr's are the thing that

164
00:05:16,160 --> 00:05:20,479
would also have to be set up to change

165
00:05:18,160 --> 00:05:22,800
the caching behavior for smm at some

166
00:05:20,479 --> 00:05:24,240
point in boot the processor is going to

167
00:05:22,800 --> 00:05:26,080
have to figure out what kind of

168
00:05:24,240 --> 00:05:28,160
processor it's running on how many cores

169
00:05:26,080 --> 00:05:30,160
it has that kind of thing because at up

170
00:05:28,160 --> 00:05:32,800
to this point it's actually operating on

171
00:05:30,160 --> 00:05:35,120
a single core the bootstrap processor

172
00:05:32,800 --> 00:05:36,880
and so at a certain point it provides it

173
00:05:35,120 --> 00:05:38,720
issues the startup inner processor

174
00:05:36,880 --> 00:05:40,240
interrupts and these are actually going

175
00:05:38,720 --> 00:05:42,240
to be sent to other cores so you're

176
00:05:40,240 --> 00:05:44,080
running single core up to a point and

177
00:05:42,240 --> 00:05:45,680
then if you'd like to wake up the other

178
00:05:44,080 --> 00:05:48,400
cores and tell them to run some

179
00:05:45,680 --> 00:05:50,000
particular code then you use zippy's

180
00:05:48,400 --> 00:05:51,840
start up into processor interrupts in

181
00:05:50,000 --> 00:05:53,440
order to wake up other colors and get

182
00:05:51,840 --> 00:05:54,639
them running they're going to all start

183
00:05:53,440 --> 00:05:56,000
in real mode and so they might

184
00:05:54,639 --> 00:05:58,400
transition in protective mode their

185
00:05:56,000 --> 00:05:59,520
right might just you know spin and wait

186
00:05:58,400 --> 00:06:02,000
you know it depends on whether you

187
00:05:59,520 --> 00:06:04,000
expect your firmware to actually be

188
00:06:02,000 --> 00:06:05,919
multi-core supporting or whether you

189
00:06:04,000 --> 00:06:07,600
just can run a single core until you get

190
00:06:05,919 --> 00:06:09,280
up to a bootloader then the firmware

191
00:06:07,600 --> 00:06:11,039
might need to start setting up a bunch

192
00:06:09,280 --> 00:06:13,840
of you know i o devices your peripherals

193
00:06:11,039 --> 00:06:16,400
your embedded controllers super i o usb

194
00:06:13,840 --> 00:06:19,199
sata etc so you know start interacting

195
00:06:16,400 --> 00:06:21,919
with both built-in and external devices

196
00:06:19,199 --> 00:06:23,280
then pci discovery occurs for things

197
00:06:21,919 --> 00:06:25,120
that you know might not be built in

198
00:06:23,280 --> 00:06:26,960
might not have you know hard-coded ways

199
00:06:25,120 --> 00:06:28,639
of discovering them something that was

200
00:06:26,960 --> 00:06:29,680
just you know slotted into a particular

201
00:06:28,639 --> 00:06:31,759
pc

202
00:06:29,680 --> 00:06:33,360
so you know enumerate all the devices

203
00:06:31,759 --> 00:06:35,759
check whether or not they need you know

204
00:06:33,360 --> 00:06:37,520
port i o or memory mapped i o bars

205
00:06:35,759 --> 00:06:39,520
filled in check whether or not they have

206
00:06:37,520 --> 00:06:41,840
option roms that need to be executed to

207
00:06:39,520 --> 00:06:43,600
fully configure them then the firmware

208
00:06:41,840 --> 00:06:45,280
has to play that memory map tetris in

209
00:06:43,600 --> 00:06:47,919
order to set up things like general

210
00:06:45,280 --> 00:06:49,199
memory reserve memory ecpi reclaim

211
00:06:47,919 --> 00:06:51,120
that's some stuff that's used for the

212
00:06:49,199 --> 00:06:53,599
power management which can be reclaimed

213
00:06:51,120 --> 00:06:56,000
and reused later on by the os versus

214
00:06:53,599 --> 00:06:57,520
acpi nvs the nominal storage this is the

215
00:06:56,000 --> 00:07:00,080
stuff that was supposed to be reserved

216
00:06:57,520 --> 00:07:02,720
this is that area that you know the

217
00:07:00,080 --> 00:07:04,000
s3 resume scripts used to get stuck into

218
00:07:02,720 --> 00:07:05,120
and just you know were trivially

219
00:07:04,000 --> 00:07:07,120
rewritable because there's nothing

220
00:07:05,120 --> 00:07:08,800
special or protected about this so

221
00:07:07,120 --> 00:07:11,039
getting up all these sort of memory

222
00:07:08,800 --> 00:07:12,960
regions to start interacting with and

223
00:07:11,039 --> 00:07:15,919
convey information to

224
00:07:12,960 --> 00:07:17,840
uh operating systems bootloaders etc and

225
00:07:15,919 --> 00:07:20,160
of course avoiding any you know special

226
00:07:17,840 --> 00:07:21,759
reserved regions or setting up any

227
00:07:20,160 --> 00:07:23,680
special reserved regions which again

228
00:07:21,759 --> 00:07:25,199
depends on the processor

229
00:07:23,680 --> 00:07:26,880
finally toward the end there might be

230
00:07:25,199 --> 00:07:29,039
you know interactions with non-volatile

231
00:07:26,880 --> 00:07:30,160
storage be it the sort of legacy cmos

232
00:07:29,039 --> 00:07:32,080
which only gives you a little bit of

233
00:07:30,160 --> 00:07:34,560
memory or you know non-volatile spy

234
00:07:32,080 --> 00:07:36,000
flash so especially ua5 systems are

235
00:07:34,560 --> 00:07:38,800
generally going to make

236
00:07:36,000 --> 00:07:40,240
extreme use of you know non-volatile efi

237
00:07:38,800 --> 00:07:42,240
variables right

238
00:07:40,240 --> 00:07:44,000
but really anything you know legacy bios

239
00:07:42,240 --> 00:07:45,919
core boot et cetera just you know the

240
00:07:44,000 --> 00:07:48,160
spy flash is there and so they can

241
00:07:45,919 --> 00:07:50,479
choose to have some convention by which

242
00:07:48,160 --> 00:07:52,960
they store information there finally

243
00:07:50,479 --> 00:07:54,800
this is sort of the end of the firmware

244
00:07:52,960 --> 00:07:57,039
you know now say it's time to hand off

245
00:07:54,800 --> 00:07:59,199
to boot loader so it's just so simple

246
00:07:57,039 --> 00:08:00,879
right 16 steps which is so easy to

247
00:07:59,199 --> 00:08:02,560
summarize and super not easy to

248
00:08:00,879 --> 00:08:04,000
implement and you know what's an

249
00:08:02,560 --> 00:08:05,919
interesting thing that's left out of

250
00:08:04,000 --> 00:08:08,000
this intel document well there's no

251
00:08:05,919 --> 00:08:09,680
mention of security right there's no you

252
00:08:08,000 --> 00:08:11,280
know quick start guide of you know the

253
00:08:09,680 --> 00:08:13,199
bare minimum security stuff you got to

254
00:08:11,280 --> 00:08:16,319
do so there's nothing about initializing

255
00:08:13,199 --> 00:08:17,759
smm or locking it down etc so that guide

256
00:08:16,319 --> 00:08:20,240
is interesting and you know i think you

257
00:08:17,759 --> 00:08:21,840
should definitely go read it

258
00:08:20,240 --> 00:08:23,680
the general point is a lot of that stuff

259
00:08:21,840 --> 00:08:26,080
we talked about is not necessarily or

260
00:08:23,680 --> 00:08:28,800
not theoretically or not as we far as we

261
00:08:26,080 --> 00:08:31,120
know security relevant but that's where

262
00:08:28,800 --> 00:08:32,640
the research lies right often going in

263
00:08:31,120 --> 00:08:34,959
and investigating these things that

264
00:08:32,640 --> 00:08:36,399
today no one thinks is security relevant

265
00:08:34,959 --> 00:08:38,640
really understanding it looking at it

266
00:08:36,399 --> 00:08:40,320
with an attacker's perspective and then

267
00:08:38,640 --> 00:08:42,159
you know you might find some new

268
00:08:40,320 --> 00:08:43,839
research applicable uh way of

269
00:08:42,159 --> 00:08:45,279
compromising the system

270
00:08:43,839 --> 00:08:46,959
so how do you learn about all that

271
00:08:45,279 --> 00:08:48,720
different stuff well you can go read the

272
00:08:46,959 --> 00:08:50,640
intel guide and they have pointers to a

273
00:08:48,720 --> 00:08:52,320
bunch of specifications and so forth you

274
00:08:50,640 --> 00:08:55,040
could go read something like you know

275
00:08:52,320 --> 00:08:57,600
open source tiano core uefi reference

276
00:08:55,040 --> 00:08:59,360
code or core boot you can sign the ndas

277
00:08:57,600 --> 00:09:01,440
with intel or find the leaked bios

278
00:08:59,360 --> 00:09:03,839
writer's guide and read those other you

279
00:09:01,440 --> 00:09:06,160
know hundreds of pages of documentation

280
00:09:03,839 --> 00:09:08,640
lots of it dry and again most of it not

281
00:09:06,160 --> 00:09:10,080
seemingly security relevant but you know

282
00:09:08,640 --> 00:09:11,279
at the end of the day

283
00:09:10,080 --> 00:09:13,760
you know there's different ways to

284
00:09:11,279 --> 00:09:15,920
understand all of this and as i said

285
00:09:13,760 --> 00:09:18,240
checking it out finding this is going to

286
00:09:15,920 --> 00:09:19,839
be you know potential areas for fruitful

287
00:09:18,240 --> 00:09:21,680
research there are things that people

288
00:09:19,839 --> 00:09:23,519
don't think is security relevant today

289
00:09:21,680 --> 00:09:26,880
if you you know put an attacker's eye on

290
00:09:23,519 --> 00:09:26,880
it it could be tomorrow

