1
00:00:00,399 --> 00:00:05,359
all right now a little pit stop here at

2
00:00:02,480 --> 00:00:07,919
pci optionram attacks on our way to

3
00:00:05,359 --> 00:00:11,759
flashlight protection and attacks

4
00:00:07,919 --> 00:00:12,799
so first a brief history of oram attacks

5
00:00:11,759 --> 00:00:14,719
by

6
00:00:12,799 --> 00:00:16,960
this guy

7
00:00:14,719 --> 00:00:19,439
now i can't wear my hat normally during

8
00:00:16,960 --> 00:00:21,199
class because otherwise you can't see my

9
00:00:19,439 --> 00:00:23,119
raised eyebrow and

10
00:00:21,199 --> 00:00:24,560
forehead wrinkles which communicate so

11
00:00:23,119 --> 00:00:26,640
much information which is the whole

12
00:00:24,560 --> 00:00:28,160
point of putting my face in these videos

13
00:00:26,640 --> 00:00:31,199
in the first place

14
00:00:28,160 --> 00:00:33,200
so 2007 john heesman

15
00:00:31,199 --> 00:00:35,760
implementing and detecting a pci root

16
00:00:33,200 --> 00:00:38,640
kit so this bit talk basically was about

17
00:00:35,760 --> 00:00:41,040
using option roms in order to infect the

18
00:00:38,640 --> 00:00:43,120
kernel with a root kit a relevant

19
00:00:41,040 --> 00:00:45,920
excerpt being the fact that he talks

20
00:00:43,120 --> 00:00:47,920
about how pci cards often provide tools

21
00:00:45,920 --> 00:00:50,079
that can be u reused

22
00:00:47,920 --> 00:00:52,719
can be used to flash the card from

23
00:00:50,079 --> 00:00:55,600
within windows and that older tools

24
00:00:52,719 --> 00:00:57,600
would be run from within dos but those

25
00:00:55,600 --> 00:01:00,320
wouldn't work within the dos emulation

26
00:00:57,600 --> 00:01:02,399
because of things like the i o privilege

27
00:01:00,320 --> 00:01:05,119
level iopl that thing you learned about

28
00:01:02,399 --> 00:01:08,880
in architecture 2001 which restricts

29
00:01:05,119 --> 00:01:10,960
port io access port io mapped bars from

30
00:01:08,880 --> 00:01:13,200
pci devices were probably used on some

31
00:01:10,960 --> 00:01:16,720
of these older cards

32
00:01:13,200 --> 00:01:19,840
2012 snare had looked at how he could

33
00:01:16,720 --> 00:01:22,159
use option roms in order to infect mac

34
00:01:19,840 --> 00:01:24,240
systems in fact their kernel so in this

35
00:01:22,159 --> 00:01:25,680
talk he talked about normal boot kit

36
00:01:24,240 --> 00:01:27,439
type attacks where you infect the

37
00:01:25,680 --> 00:01:29,680
bootloader and hop your way into the

38
00:01:27,439 --> 00:01:31,759
kernel talked about optionram attacks to

39
00:01:29,680 --> 00:01:34,079
do the same thing and he talked about

40
00:01:31,759 --> 00:01:36,880
efi attacks and basically while

41
00:01:34,079 --> 00:01:38,560
optionram was 7 out of 10 efi attacks

42
00:01:36,880 --> 00:01:40,880
were 11 out of 10 and that's where we're

43
00:01:38,560 --> 00:01:43,040
trying to get with these getting to the

44
00:01:40,880 --> 00:01:44,560
flash access control portion of this

45
00:01:43,040 --> 00:01:47,439
class

46
00:01:44,560 --> 00:01:49,920
in 2013 pierre chefflere talked about

47
00:01:47,439 --> 00:01:52,399
using option roms in order to again

48
00:01:49,920 --> 00:01:54,479
bounce your way down to the operating

49
00:01:52,399 --> 00:01:56,560
system this is a good diagram that sort

50
00:01:54,479 --> 00:01:59,280
of indicates again that principle of

51
00:01:56,560 --> 00:02:01,840
they who run first run best this is a

52
00:01:59,280 --> 00:02:03,920
standard sort of uefi view of the world

53
00:02:01,840 --> 00:02:06,640
in different phases of boot and you can

54
00:02:03,920 --> 00:02:08,080
see that the option rom runs earlier

55
00:02:06,640 --> 00:02:11,200
than the operating system and

56
00:02:08,080 --> 00:02:13,280
consequently can infect it in 2013 a

57
00:02:11,200 --> 00:02:15,840
student paper by saul st john at the

58
00:02:13,280 --> 00:02:17,680
university of wisconsin at madison had

59
00:02:15,840 --> 00:02:20,480
presented a very interesting thing

60
00:02:17,680 --> 00:02:22,080
indeed now this was before thunderstrike

61
00:02:20,480 --> 00:02:24,319
which you might have heard of this was

62
00:02:22,080 --> 00:02:25,200
before pci leach which you may have

63
00:02:24,319 --> 00:02:27,840
heard of

64
00:02:25,200 --> 00:02:30,400
and basically this was thundergate and

65
00:02:27,840 --> 00:02:32,560
he set up a website thundergate.io

66
00:02:30,400 --> 00:02:34,480
unfortunately nobody really heard about

67
00:02:32,560 --> 00:02:36,640
this until much later because he didn't

68
00:02:34,480 --> 00:02:38,239
present it at any conferences now the

69
00:02:36,640 --> 00:02:40,400
important thing about this work is that

70
00:02:38,239 --> 00:02:42,319
he released a tool at thundergate.io

71
00:02:40,400 --> 00:02:45,280
which is still available on github and

72
00:02:42,319 --> 00:02:47,599
this tool allowed you to rewrite the

73
00:02:45,280 --> 00:02:50,800
option rom on a apple thunderbolt

74
00:02:47,599 --> 00:02:53,280
ethernet adapter and also rewrite the

75
00:02:50,800 --> 00:02:55,760
firmware of the broadcom chip that runs

76
00:02:53,280 --> 00:02:57,599
on this and so rewriting the optionrom

77
00:02:55,760 --> 00:02:59,360
allowed the opsharam type attacks that

78
00:02:57,599 --> 00:03:01,599
we talked about in this section and

79
00:02:59,360 --> 00:03:04,080
rewriting the firmware allowed for

80
00:03:01,599 --> 00:03:06,239
actually doing arbitrary dma attacks

81
00:03:04,080 --> 00:03:08,159
which are the type of things that tools

82
00:03:06,239 --> 00:03:09,920
like pci leach do

83
00:03:08,159 --> 00:03:12,159
so this actually provides an extremely

84
00:03:09,920 --> 00:03:14,800
powerful experimental platform that you

85
00:03:12,159 --> 00:03:17,680
can use to both understand optionram

86
00:03:14,800 --> 00:03:20,720
attacks and dma attacks

87
00:03:17,680 --> 00:03:23,760
in 2014 trammell hudson presented

88
00:03:20,720 --> 00:03:25,760
thunder strike which was using the

89
00:03:23,760 --> 00:03:28,720
option rom on a apple thunderbolt

90
00:03:25,760 --> 00:03:30,879
ethernet adapter to actually infect efi

91
00:03:28,720 --> 00:03:33,200
itself so recall that snare had talked

92
00:03:30,879 --> 00:03:36,000
about using option roms to infect the

93
00:03:33,200 --> 00:03:38,239
operating system itself and had tried to

94
00:03:36,000 --> 00:03:40,480
infect the efi but ultimately wasn't

95
00:03:38,239 --> 00:03:42,959
able to do that well trammell used the

96
00:03:40,480 --> 00:03:44,000
optionram to end up infecting the efi

97
00:03:42,959 --> 00:03:46,640
itself

98
00:03:44,000 --> 00:03:49,840
now if you ask me tremble sort of missed

99
00:03:46,640 --> 00:03:51,920
out by using a ac dc reference of

100
00:03:49,840 --> 00:03:54,720
thunderstruck for this thunder strike

101
00:03:51,920 --> 00:03:57,920
work when instead he could have used 90s

102
00:03:54,720 --> 00:03:59,840
not a thor thor thunder strike his name

103
00:03:57,920 --> 00:04:01,760
it was thunder strike eric's masterson

104
00:03:59,840 --> 00:04:05,040
but oh well so it goes

105
00:04:01,760 --> 00:04:07,280
all right then in 2015 trammell myself

106
00:04:05,040 --> 00:04:10,000
and cory callenberg teamed up to use

107
00:04:07,280 --> 00:04:12,879
what travel knew about infecting max

108
00:04:10,000 --> 00:04:14,560
with what we knew about infecting pcs so

109
00:04:12,879 --> 00:04:16,320
specifically we showed that macs were

110
00:04:14,560 --> 00:04:18,959
vulnerable to a bunch of different

111
00:04:16,320 --> 00:04:21,120
vulnerabilities that we had previously

112
00:04:18,959 --> 00:04:23,360
disclosed for pcs they hadn't got fixed

113
00:04:21,120 --> 00:04:25,840
on macs things like the darth vanamous

114
00:04:23,360 --> 00:04:26,639
attack that corey and rafal voijk worked

115
00:04:25,840 --> 00:04:28,720
on

116
00:04:26,639 --> 00:04:30,560
and so we basically showed you could use

117
00:04:28,720 --> 00:04:31,919
these in combination with the optionram

118
00:04:30,560 --> 00:04:33,759
attacks that

119
00:04:31,919 --> 00:04:36,320
trammell had previously done to again

120
00:04:33,759 --> 00:04:39,040
reinfect the actual firmware the proper

121
00:04:36,320 --> 00:04:41,759
efi of a mac system

122
00:04:39,040 --> 00:04:45,120
and finally the most interesting thing

123
00:04:41,759 --> 00:04:48,080
is that in 2017 wikileaks released

124
00:04:45,120 --> 00:04:50,880
information about cia capabilities to

125
00:04:48,080 --> 00:04:53,360
infect systems amongst that information

126
00:04:50,880 --> 00:04:56,400
was a particular attack called sonic

127
00:04:53,360 --> 00:04:59,520
screwdriver and this had a user manual

128
00:04:56,400 --> 00:05:01,840
from november 2012 which was seven

129
00:04:59,520 --> 00:05:04,800
months after snare's original talk at

130
00:05:01,840 --> 00:05:07,520
ciscan in april of 2012 and basically

131
00:05:04,800 --> 00:05:10,320
this was the cia's weaponized form of

132
00:05:07,520 --> 00:05:11,919
snare's attack basically used a apple

133
00:05:10,320 --> 00:05:14,720
thunderbolt ethernet adapter with a

134
00:05:11,919 --> 00:05:18,160
custom malicious option rom in order to

135
00:05:14,720 --> 00:05:20,160
infect apple systems at the efi level

136
00:05:18,160 --> 00:05:22,240
and to bypass things like firmware

137
00:05:20,160 --> 00:05:24,400
passwords which are frequently used in

138
00:05:22,240 --> 00:05:26,240
order to restrict the capability to boot

139
00:05:24,400 --> 00:05:28,240
off external media from which other

140
00:05:26,240 --> 00:05:30,240
attacks can be launched so that's quick

141
00:05:28,240 --> 00:05:32,320
overview of some of the option realm

142
00:05:30,240 --> 00:05:34,479
attacks that have been documented in

143
00:05:32,320 --> 00:05:36,080
research and in the wild as it were

144
00:05:34,479 --> 00:05:37,680
although i don't think anyone's actually

145
00:05:36,080 --> 00:05:39,680
caught this in the wild

146
00:05:37,680 --> 00:05:42,800
so now let's go understand what option

147
00:05:39,680 --> 00:05:42,800
roms actually are

