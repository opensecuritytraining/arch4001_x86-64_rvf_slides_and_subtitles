1
00:00:00,080 --> 00:00:03,919
so now we're going to do a brief lab

2
00:00:01,680 --> 00:00:04,880
where i want you to go mess with the pci

3
00:00:03,919 --> 00:00:07,359
bars

4
00:00:04,880 --> 00:00:10,000
so specifically i want you to go look at

5
00:00:07,359 --> 00:00:11,920
the pci devices go to the relevant

6
00:00:10,000 --> 00:00:13,440
offset for a bar

7
00:00:11,920 --> 00:00:16,400
find out whether or not there's

8
00:00:13,440 --> 00:00:18,800
something non-zero there and if there is

9
00:00:16,400 --> 00:00:20,800
write it with all ones all binary ones

10
00:00:18,800 --> 00:00:23,600
all f's in order to

11
00:00:20,800 --> 00:00:24,960
see what the device returns for a size

12
00:00:23,600 --> 00:00:27,039
for that bar

13
00:00:24,960 --> 00:00:29,599
now you can do it for both memory mapped

14
00:00:27,039 --> 00:00:31,679
or port i o bars and it should tell you

15
00:00:29,599 --> 00:00:33,680
the size in each case so i gotta say

16
00:00:31,679 --> 00:00:36,399
you're almost certainly going to break

17
00:00:33,680 --> 00:00:38,559
the os's driver's interactions with that

18
00:00:36,399 --> 00:00:40,079
particular piece of hardware and so this

19
00:00:38,559 --> 00:00:42,079
is going to make the hardware probably

20
00:00:40,079 --> 00:00:44,879
stop working so if you happen to

21
00:00:42,079 --> 00:00:47,280
interact with you know a usb device then

22
00:00:44,879 --> 00:00:49,120
it's going to you know stop working for

23
00:00:47,280 --> 00:00:51,680
usb for a keyboard for instance if you

24
00:00:49,120 --> 00:00:53,280
interact with a nic it's going to stop

25
00:00:51,680 --> 00:00:55,360
accepting packets

26
00:00:53,280 --> 00:00:58,079
so you know miss frizzle says you should

27
00:00:55,360 --> 00:01:00,399
take chances make mistakes and get messy

28
00:00:58,079 --> 00:01:03,120
so you know go ahead and break your neck

29
00:01:00,399 --> 00:01:04,879
but as long as you're connected via usb

30
00:01:03,120 --> 00:01:07,200
or break your usb

31
00:01:04,879 --> 00:01:09,840
as long as you're connected via the nic

32
00:01:07,200 --> 00:01:11,520
so go ahead and go mess around and you

33
00:01:09,840 --> 00:01:14,400
know see this

34
00:01:11,520 --> 00:01:16,000
weird world of bar sizing in action by

35
00:01:14,400 --> 00:01:20,000
writing all ones and seeing what's

36
00:01:16,000 --> 00:01:20,000
returned when you read back the value

