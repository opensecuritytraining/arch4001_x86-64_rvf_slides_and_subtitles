1
00:00:00,240 --> 00:00:03,520
now let's talk about the second smi

2
00:00:02,000 --> 00:00:05,759
source that we're going to highlight and

3
00:00:03,520 --> 00:00:07,839
this is a big one the advanced power

4
00:00:05,759 --> 00:00:10,719
management port io

5
00:00:07,839 --> 00:00:12,639
port b2 interface

6
00:00:10,719 --> 00:00:14,799
so this is a big one because this is

7
00:00:12,639 --> 00:00:17,359
going to be used basically as an

8
00:00:14,799 --> 00:00:19,039
interface to smm as a sort of sys

9
00:00:17,359 --> 00:00:20,320
callish type thing

10
00:00:19,039 --> 00:00:22,240
so we said we're going to talk about it

11
00:00:20,320 --> 00:00:23,840
later and the time is now and the place

12
00:00:22,240 --> 00:00:26,080
is here

13
00:00:23,840 --> 00:00:28,080
checking all of this big list of system

14
00:00:26,080 --> 00:00:30,160
management interrupts we see that a

15
00:00:28,080 --> 00:00:32,160
right to the b2 register is something

16
00:00:30,160 --> 00:00:35,200
that causes an smi

17
00:00:32,160 --> 00:00:38,800
and the additional enables is that apmc

18
00:00:35,200 --> 00:00:43,040
n must be equal to 1 and the status is

19
00:00:38,800 --> 00:00:45,760
reporting in the apm status register

20
00:00:43,040 --> 00:00:46,879
now apm is advanced power management

21
00:00:45,760 --> 00:00:50,000
interface

22
00:00:46,879 --> 00:00:52,399
and all pch systems support this some

23
00:00:50,000 --> 00:00:54,399
earlier systems may not but basically

24
00:00:52,399 --> 00:00:56,960
it's the power management standard that

25
00:00:54,399 --> 00:00:59,840
intel followed before it started using

26
00:00:56,960 --> 00:01:01,920
acpi which we'll talk about a bit later

27
00:00:59,840 --> 00:01:05,760
this is a fixed i o range it's always

28
00:01:01,920 --> 00:01:08,640
ports b2 and b3 so it can't be relocated

29
00:01:05,760 --> 00:01:09,760
so it listed an additional enable of apm

30
00:01:08,640 --> 00:01:12,720
cn

31
00:01:09,760 --> 00:01:15,759
so what is that well that's bit five in

32
00:01:12,720 --> 00:01:17,840
the smi enable register

33
00:01:15,759 --> 00:01:20,560
and that register is found at pm base

34
00:01:17,840 --> 00:01:22,880
plus 30 so if it's an offset from pm

35
00:01:20,560 --> 00:01:25,920
base it's going to be port i o so we saw

36
00:01:22,880 --> 00:01:28,240
that in the previous smi thing uh pm

37
00:01:25,920 --> 00:01:30,799
base plus 60 was

38
00:01:28,240 --> 00:01:32,880
port i o this is port io pm base itself

39
00:01:30,799 --> 00:01:34,000
is just a port i o base for power

40
00:01:32,880 --> 00:01:35,759
management

41
00:01:34,000 --> 00:01:37,680
and this is indeed power management

42
00:01:35,759 --> 00:01:40,640
advanced power management

43
00:01:37,680 --> 00:01:44,240
so if this is set to one then writes to

44
00:01:40,640 --> 00:01:47,680
the hex b2 will cause an smi so this is

45
00:01:44,240 --> 00:01:49,600
set to 1 it says writes to the apm cnt

46
00:01:47,680 --> 00:01:52,479
register cause an smi and if it's set to

47
00:01:49,600 --> 00:01:55,759
0 they don't well what is the apm cnt

48
00:01:52,479 --> 00:01:57,439
that is the actual name for the b2

49
00:01:55,759 --> 00:02:00,000
port so looking through the manual

50
00:01:57,439 --> 00:02:01,439
finding this apm cnt you've got the b2

51
00:02:00,000 --> 00:02:03,680
which is the advanced power management

52
00:02:01,439 --> 00:02:05,680
control port and the b3 is advanced

53
00:02:03,680 --> 00:02:07,520
power management status port and while

54
00:02:05,680 --> 00:02:09,200
it's notionally a status as you'll see

55
00:02:07,520 --> 00:02:10,479
in the next slide it's not really used

56
00:02:09,200 --> 00:02:12,080
that way much

57
00:02:10,479 --> 00:02:14,879
now both of these registers are read

58
00:02:12,080 --> 00:02:17,760
right and how they tend to work is that

59
00:02:14,879 --> 00:02:19,760
a bios or operating system will poke

60
00:02:17,760 --> 00:02:22,319
some arbitrary bytes into each of these

61
00:02:19,760 --> 00:02:24,560
these are each one byte large so they'll

62
00:02:22,319 --> 00:02:27,200
put some arbitrary values in here it'll

63
00:02:24,560 --> 00:02:30,000
cause an smi and then the smi handler

64
00:02:27,200 --> 00:02:31,680
will peak at the values of b2 and b3 and

65
00:02:30,000 --> 00:02:34,959
say okay well i'll have a switch

66
00:02:31,680 --> 00:02:36,879
statement that says if b2 was equal to 0

67
00:02:34,959 --> 00:02:38,879
do this if it was one do this it was

68
00:02:36,879 --> 00:02:40,480
three do this so basically there's just

69
00:02:38,879 --> 00:02:42,080
a big switch statement on the other side

70
00:02:40,480 --> 00:02:44,000
where they treat this as sort of

71
00:02:42,080 --> 00:02:46,560
commands that are given to them by the

72
00:02:44,000 --> 00:02:50,239
operating system or bios so there are

73
00:02:46,560 --> 00:02:52,879
formal definitions of apm cnt port b2 is

74
00:02:50,239 --> 00:02:53,920
used to pass command between os and smi

75
00:02:52,879 --> 00:02:55,280
handler

76
00:02:53,920 --> 00:02:57,760
but the interesting thing here is more

77
00:02:55,280 --> 00:02:59,599
the status one b3 it says used to pass

78
00:02:57,760 --> 00:03:01,680
data between os and smi handler

79
00:02:59,599 --> 00:03:03,360
basically this is a scratch pad register

80
00:03:01,680 --> 00:03:04,400
and is not affected by other registers

81
00:03:03,360 --> 00:03:07,440
or functions

82
00:03:04,400 --> 00:03:10,480
i like to see basically written in intel

83
00:03:07,440 --> 00:03:13,280
manuals i like the casual feel of it so

84
00:03:10,480 --> 00:03:15,200
anyways this register is basically just

85
00:03:13,280 --> 00:03:17,360
a scratchpad register and this one is

86
00:03:15,200 --> 00:03:19,280
the one that actually causes the smi

87
00:03:17,360 --> 00:03:21,519
rights to this don't cause an smi writes

88
00:03:19,280 --> 00:03:24,239
to this one dude remember that it said

89
00:03:21,519 --> 00:03:27,760
in that big list that it said a couple

90
00:03:24,239 --> 00:03:29,680
things back that if smi if smi enable

91
00:03:27,760 --> 00:03:33,120
apmc enable

92
00:03:29,680 --> 00:03:35,200
writes to apm cnt so port b2 cause an

93
00:03:33,120 --> 00:03:37,040
smi so it's only port b2 which is

94
00:03:35,200 --> 00:03:38,239
causing the smi

95
00:03:37,040 --> 00:03:40,080
all right so here's how i want you to

96
00:03:38,239 --> 00:03:42,720
think about it you learned about system

97
00:03:40,080 --> 00:03:44,959
calls in architecture 2001 which were a

98
00:03:42,720 --> 00:03:47,680
way to transition between current

99
00:03:44,959 --> 00:03:49,760
privilege level three so user space then

100
00:03:47,680 --> 00:03:51,519
some sort of system call and boom you're

101
00:03:49,760 --> 00:03:52,959
up in kernel space current privilege

102
00:03:51,519 --> 00:03:54,640
level zero

103
00:03:52,959 --> 00:03:56,640
so now i want you to think about that

104
00:03:54,640 --> 00:03:58,799
someone's in kernel space because they

105
00:03:56,640 --> 00:04:00,959
need that in order to do port i o

106
00:03:58,799 --> 00:04:02,480
assuming the i o privilege level is set

107
00:04:00,959 --> 00:04:04,239
to zero

108
00:04:02,480 --> 00:04:07,599
and so the code is running along in

109
00:04:04,239 --> 00:04:10,640
kernel space at cpl zero and then boeing

110
00:04:07,599 --> 00:04:12,640
they hit an smi perhaps via writing to

111
00:04:10,640 --> 00:04:14,879
port b2 perhaps not

112
00:04:12,640 --> 00:04:17,280
but then that ultimately transitions

113
00:04:14,879 --> 00:04:19,680
them up into smm and now they are dark

114
00:04:17,280 --> 00:04:22,560
sonic instead of supersonic

115
00:04:19,680 --> 00:04:25,440
and so smm for reasons we'll see later

116
00:04:22,560 --> 00:04:27,919
is a dark world in which you can't see

117
00:04:25,440 --> 00:04:28,880
this dark sonic working behind the

118
00:04:27,919 --> 00:04:31,440
scenes

119
00:04:28,880 --> 00:04:33,919
so anyways smiles are used via port b2

120
00:04:31,440 --> 00:04:35,840
in particular as a sort of

121
00:04:33,919 --> 00:04:40,160
system callish type thing where you're

122
00:04:35,840 --> 00:04:40,160
calling into smm rather than a kernel

