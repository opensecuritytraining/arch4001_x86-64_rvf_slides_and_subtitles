1
00:00:00,240 --> 00:00:03,679
all right on to understanding system

2
00:00:02,159 --> 00:00:06,080
management ram

3
00:00:03,679 --> 00:00:09,360
well it's so simple it just goes from sm

4
00:00:06,080 --> 00:00:12,639
base up to sm base plus sram size there

5
00:00:09,360 --> 00:00:14,639
you go that's sram got it good

6
00:00:12,639 --> 00:00:16,640
all right done with this video

7
00:00:14,639 --> 00:00:19,520
now we're actually continuing all right

8
00:00:16,640 --> 00:00:21,199
so smram refers to the entire range or

9
00:00:19,520 --> 00:00:23,760
ranges where the system management

10
00:00:21,199 --> 00:00:25,840
interrupt handler code can be located

11
00:00:23,760 --> 00:00:28,800
now we showed that the start of smram is

12
00:00:25,840 --> 00:00:31,039
given by sm base and that is a private

13
00:00:28,800 --> 00:00:33,040
cpu internal register

14
00:00:31,039 --> 00:00:34,800
that's basically a register that you

15
00:00:33,040 --> 00:00:36,480
don't normally have a way to just

16
00:00:34,800 --> 00:00:38,640
directly read or write

17
00:00:36,480 --> 00:00:40,000
and each core is going to have its own

18
00:00:38,640 --> 00:00:41,840
sm base

19
00:00:40,000 --> 00:00:42,800
now when a system management interrupt

20
00:00:41,840 --> 00:00:45,520
fires

21
00:00:42,800 --> 00:00:47,920
the entry point for code execution is

22
00:00:45,520 --> 00:00:49,680
always going to be found at sm base plus

23
00:00:47,920 --> 00:00:52,000
hex 8000

24
00:00:49,680 --> 00:00:54,239
and additionally when you cause a system

25
00:00:52,000 --> 00:00:56,719
management interrupt it's going to need

26
00:00:54,239 --> 00:00:58,719
to save the state of the cpu so that it

27
00:00:56,719 --> 00:01:02,239
can get back to whatever it was doing

28
00:00:58,719 --> 00:01:03,039
after you leave smm so consequently the

29
00:01:02,239 --> 00:01:06,000
pre

30
00:01:03,039 --> 00:01:08,560
smi register context is saved into sm

31
00:01:06,000 --> 00:01:11,520
ram on entry and it's restored on exit

32
00:01:08,560 --> 00:01:15,040
so in our smram space we start at sm

33
00:01:11,520 --> 00:01:17,680
base and sm base on startup defaults to

34
00:01:15,040 --> 00:01:19,439
hex thirty thousand so it can be

35
00:01:17,680 --> 00:01:21,119
relocated to somewhere else as we've

36
00:01:19,439 --> 00:01:23,040
shown previously

37
00:01:21,119 --> 00:01:24,880
and it typically will be but that's

38
00:01:23,040 --> 00:01:27,840
where it's going to start

39
00:01:24,880 --> 00:01:31,759
so if it starts at hex 30 000 then the

40
00:01:27,840 --> 00:01:33,759
entry point is going to be 30 000 plus 8

41
00:01:31,759 --> 00:01:35,920
000. now practically speaking how this

42
00:01:33,759 --> 00:01:39,040
is actually achieved is that sm base

43
00:01:35,920 --> 00:01:41,360
defaults as a cs you know base address

44
00:01:39,040 --> 00:01:43,759
in the code segment the shadow portion

45
00:01:41,360 --> 00:01:46,880
of the code segment it defaults to

46
00:01:43,759 --> 00:01:50,000
three zero zero zero and then the ip is

47
00:01:46,880 --> 00:01:52,799
set to 8 000. so that's the first use

48
00:01:50,000 --> 00:01:55,600
for the sm ram which is the code for the

49
00:01:52,799 --> 00:01:57,360
smi handler but we said there also is

50
00:01:55,600 --> 00:02:01,280
data that's stored there specifically

51
00:01:57,360 --> 00:02:05,360
the pre-smm save state so that's saved

52
00:02:01,280 --> 00:02:06,640
at smbase plus 8000 plus 7ff so

53
00:02:05,360 --> 00:02:08,479
unfortunately we're going to be using

54
00:02:06,640 --> 00:02:10,399
this convention that intel uses when

55
00:02:08,479 --> 00:02:13,120
they describe thing in their data sheets

56
00:02:10,399 --> 00:02:14,959
instead of just giving you smb base plus

57
00:02:13,120 --> 00:02:17,680
ffff

58
00:02:14,959 --> 00:02:20,480
they say smbs plus 8000 plus 7ff which

59
00:02:17,680 --> 00:02:22,400
is really just smbs plus ffff

60
00:02:20,480 --> 00:02:26,000
so anyways if the processor was

61
00:02:22,400 --> 00:02:28,000
executing in 32-bit mode before the

62
00:02:26,000 --> 00:02:30,080
system management interrupt fired

63
00:02:28,000 --> 00:02:32,480
then the information will range from

64
00:02:30,080 --> 00:02:35,200
there down to here so it's a total of

65
00:02:32,480 --> 00:02:37,440
hex 200 bytes

66
00:02:35,200 --> 00:02:38,959
and so this is the description of what

67
00:02:37,440 --> 00:02:41,680
that looks like in terms of what

68
00:02:38,959 --> 00:02:43,280
registers are saved in what order

69
00:02:41,680 --> 00:02:45,440
but an interesting thing to call

70
00:02:43,280 --> 00:02:49,040
attention to is the fact that the smbase

71
00:02:45,440 --> 00:02:51,920
itself is also saved to this save state

72
00:02:49,040 --> 00:02:54,560
and it says that it's writable

73
00:02:51,920 --> 00:02:55,840
so the sm base is stored on system

74
00:02:54,560 --> 00:02:57,280
management interrupt and it's always

75
00:02:55,840 --> 00:02:59,840
stored to the exact same location

76
00:02:57,280 --> 00:03:04,000
whether it's 32 bits or 64 bits stored

77
00:02:59,840 --> 00:03:06,239
to sm base plus fef8 so previously when

78
00:03:04,000 --> 00:03:08,159
we saw just abstractly about you know

79
00:03:06,239 --> 00:03:10,400
compatible memory spaces and the

80
00:03:08,159 --> 00:03:12,000
relocation of sm ram

81
00:03:10,400 --> 00:03:14,480
basically what's going to happen is

82
00:03:12,000 --> 00:03:17,280
you're going to go into smm the sm base

83
00:03:14,480 --> 00:03:19,519
that defaults to hex 3000 is going to be

84
00:03:17,280 --> 00:03:21,200
saved to that save state area and then

85
00:03:19,519 --> 00:03:23,200
the system management interrupt handler

86
00:03:21,200 --> 00:03:25,440
is going to say hey looks like this is

87
00:03:23,200 --> 00:03:27,120
my you know first ever entry into system

88
00:03:25,440 --> 00:03:29,120
management mode i think i'm going to go

89
00:03:27,120 --> 00:03:31,360
ahead and relocate system management

90
00:03:29,120 --> 00:03:33,200
base to some other location

91
00:03:31,360 --> 00:03:35,440
but because sm base is sort of a weird

92
00:03:33,200 --> 00:03:37,920
register that you don't have a direct

93
00:03:35,440 --> 00:03:40,000
sort of read and write access to the way

94
00:03:37,920 --> 00:03:42,720
that that first smi handler actually

95
00:03:40,000 --> 00:03:46,159
changes it is that it overwrites the

96
00:03:42,720 --> 00:03:48,480
saved copy in the smram save state and

97
00:03:46,159 --> 00:03:50,959
then upon doing the resume it's going to

98
00:03:48,480 --> 00:03:52,720
be you know the change will occur upon

99
00:03:50,959 --> 00:03:54,560
resume

100
00:03:52,720 --> 00:03:56,400
just like we showed in those previous

101
00:03:54,560 --> 00:03:58,720
animated diagrams

102
00:03:56,400 --> 00:04:00,879
so if the processor was executing in

103
00:03:58,720 --> 00:04:03,599
64-bit mode at the time that the smi

104
00:04:00,879 --> 00:04:06,879
fires the save state is going to range

105
00:04:03,599 --> 00:04:09,120
again from smbase plus ffff and now this

106
00:04:06,879 --> 00:04:11,360
time it's going to be hex 400 bytes so

107
00:04:09,120 --> 00:04:15,680
it's going to go down to smbase plus

108
00:04:11,360 --> 00:04:17,199
8000 plus 7c 0. so x 400 bytes stored

109
00:04:15,680 --> 00:04:20,720
there which is what you would expect you

110
00:04:17,199 --> 00:04:22,400
know 64-bit mode has larger registers so

111
00:04:20,720 --> 00:04:24,400
double the space

112
00:04:22,400 --> 00:04:26,479
okay so here's what the data sheets say

113
00:04:24,400 --> 00:04:30,320
about what the save state looks like for

114
00:04:26,479 --> 00:04:33,440
64 bits and you know it's more stuff but

115
00:04:30,320 --> 00:04:36,320
conveniently intel does save the sm base

116
00:04:33,440 --> 00:04:41,040
at the same address so it is either sm

117
00:04:36,320 --> 00:04:43,759
base plus 8000 plus 7e f8 or smbase plus

118
00:04:41,040 --> 00:04:46,479
fe f8 however you want to think of it so

119
00:04:43,759 --> 00:04:48,400
32 or 64 bits the same value is stored

120
00:04:46,479 --> 00:04:50,720
there it says double word so it's a 32

121
00:04:48,400 --> 00:04:53,199
bit value this means that s and base

122
00:04:50,720 --> 00:04:54,080
cannot actually be above the 4 gigabyte

123
00:04:53,199 --> 00:04:57,840
range

124
00:04:54,080 --> 00:05:00,080
and that's just fine with us

125
00:04:57,840 --> 00:05:02,479
now the total size of the smram is going

126
00:05:00,080 --> 00:05:04,720
to depend on which location the bios

127
00:05:02,479 --> 00:05:06,479
vendor has actually placed it in there

128
00:05:04,720 --> 00:05:08,800
are multiple locations that we'll learn

129
00:05:06,479 --> 00:05:10,960
about in the next section and they have

130
00:05:08,800 --> 00:05:12,960
different constraints on their sizes now

131
00:05:10,960 --> 00:05:15,360
because we said that all of the cores

132
00:05:12,960 --> 00:05:17,199
have their own copy of sm base

133
00:05:15,360 --> 00:05:18,800
you shouldn't have all of those cores

134
00:05:17,199 --> 00:05:20,720
pointing at the exact same s and base

135
00:05:18,800 --> 00:05:23,520
that would cause a problem when you

136
00:05:20,720 --> 00:05:25,039
enter into smm all of a sudden

137
00:05:23,520 --> 00:05:26,800
everything would be at the same offset

138
00:05:25,039 --> 00:05:29,039
and all of their save states would start

139
00:05:26,800 --> 00:05:31,280
clobbering each other because obviously

140
00:05:29,039 --> 00:05:33,039
each core is probably at some completely

141
00:05:31,280 --> 00:05:35,039
different address completely different

142
00:05:33,039 --> 00:05:37,199
register state and so they would smash

143
00:05:35,039 --> 00:05:39,520
each other so necessarily a bios is

144
00:05:37,199 --> 00:05:41,759
responsible for setting these up at some

145
00:05:39,520 --> 00:05:44,639
offset from each other for instance here

146
00:05:41,759 --> 00:05:46,800
we just showed it as hex 1000 offset and

147
00:05:44,639 --> 00:05:48,639
so there will be a different entry point

148
00:05:46,800 --> 00:05:50,639
for each core and there will be a

149
00:05:48,639 --> 00:05:52,800
different save state for each core and

150
00:05:50,639 --> 00:05:54,960
practically speaking it may be the case

151
00:05:52,800 --> 00:05:56,720
that the biospender decides to make it

152
00:05:54,960 --> 00:05:58,400
so that you know only a single core

153
00:05:56,720 --> 00:06:00,479
actually runs code and the rest of them

154
00:05:58,400 --> 00:06:02,880
just sort of dead loop and wait for that

155
00:06:00,479 --> 00:06:04,880
first core to end but it kind of is up

156
00:06:02,880 --> 00:06:07,520
to the bios manufacturer how they want

157
00:06:04,880 --> 00:06:07,520
to do it

