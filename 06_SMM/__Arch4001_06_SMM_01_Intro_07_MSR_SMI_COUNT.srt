1
00:00:00,080 --> 00:00:05,520
so i want to make a point about how

2
00:00:02,159 --> 00:00:07,839
smi's and smm can be invoked behind the

3
00:00:05,520 --> 00:00:09,040
scenes and you wouldn't actually see it

4
00:00:07,839 --> 00:00:11,599
so after this video i'm going to have

5
00:00:09,040 --> 00:00:13,759
you do a basic little experiment using

6
00:00:11,599 --> 00:00:15,679
either read write everything or chipset

7
00:00:13,759 --> 00:00:17,600
and i'm going to want you to write to

8
00:00:15,679 --> 00:00:20,000
port b2 just you know write some number

9
00:00:17,600 --> 00:00:22,800
there one whatever and assuming your

10
00:00:20,000 --> 00:00:25,519
system has the relevant enable enabled

11
00:00:22,800 --> 00:00:27,760
it should just transition to smm and

12
00:00:25,519 --> 00:00:29,920
then immediately come back and the thing

13
00:00:27,760 --> 00:00:31,760
is you wouldn't actually notice anything

14
00:00:29,920 --> 00:00:34,559
this is supposed to all happen behind

15
00:00:31,760 --> 00:00:36,480
the scenes quietly quickly and visibly

16
00:00:34,559 --> 00:00:38,480
in a way that ideally doesn't perturb

17
00:00:36,480 --> 00:00:40,399
the running operating system

18
00:00:38,480 --> 00:00:42,480
so then there's the question of you know

19
00:00:40,399 --> 00:00:44,480
how often is this happening i talked

20
00:00:42,480 --> 00:00:46,559
briefly about how things like periodic

21
00:00:44,480 --> 00:00:48,559
smis are useful for malware just to give

22
00:00:46,559 --> 00:00:50,719
them a guarantee that they're actually

23
00:00:48,559 --> 00:00:51,520
going to get their malware invoked

24
00:00:50,719 --> 00:00:53,120
but

25
00:00:51,520 --> 00:00:54,960
you know what if if there was a bunch of

26
00:00:53,120 --> 00:00:56,160
other stuff happening behind the scenes

27
00:00:54,960 --> 00:00:57,840
on the system then they wouldn't need to

28
00:00:56,160 --> 00:00:59,440
use periodic

29
00:00:57,840 --> 00:01:02,480
and so the question of how often these

30
00:00:59,440 --> 00:01:04,720
happen basically is it depends

31
00:01:02,480 --> 00:01:07,840
laptops and things that need to do more

32
00:01:04,720 --> 00:01:09,360
power management might be using smm more

33
00:01:07,840 --> 00:01:11,360
frequently it sort of depends on the

34
00:01:09,360 --> 00:01:13,119
vendor some vendors might use a

35
00:01:11,360 --> 00:01:15,520
completely separate chip that handles

36
00:01:13,119 --> 00:01:17,280
most of the power management things the

37
00:01:15,520 --> 00:01:19,200
embedded controller system management

38
00:01:17,280 --> 00:01:21,119
controller things like that

39
00:01:19,200 --> 00:01:22,640
that's why a desktop system that

40
00:01:21,119 --> 00:01:24,320
typically doesn't have those sort of

41
00:01:22,640 --> 00:01:25,759
chips and doesn't have those sort of

42
00:01:24,320 --> 00:01:26,720
because they don't have those sort of

43
00:01:25,759 --> 00:01:29,439
needs

44
00:01:26,720 --> 00:01:31,439
might not have smi's as frequently but

45
00:01:29,439 --> 00:01:33,439
there is one surefire way to find out

46
00:01:31,439 --> 00:01:35,040
how often system management interrupts

47
00:01:33,439 --> 00:01:38,079
are occurring on your system and that's

48
00:01:35,040 --> 00:01:40,560
by reading the msr smi count

49
00:01:38,079 --> 00:01:42,320
so it was a new msr that was introduced

50
00:01:40,560 --> 00:01:44,640
in first generation core systems and

51
00:01:42,320 --> 00:01:47,119
later and it's basically just going to

52
00:01:44,640 --> 00:01:50,000
give you a running total of how many

53
00:01:47,119 --> 00:01:52,320
smis have fired so consequently if you

54
00:01:50,000 --> 00:01:54,799
read this msr and then you go right to

55
00:01:52,320 --> 00:01:57,040
b2 and then you read it again you should

56
00:01:54,799 --> 00:01:59,360
see that the smis have increased by at

57
00:01:57,040 --> 00:02:01,040
least one but possibly more depending on

58
00:01:59,360 --> 00:02:02,560
whether or not there's other background

59
00:02:01,040 --> 00:02:06,799
activity in your system

60
00:02:02,560 --> 00:02:06,799
so let's go have you do that now

