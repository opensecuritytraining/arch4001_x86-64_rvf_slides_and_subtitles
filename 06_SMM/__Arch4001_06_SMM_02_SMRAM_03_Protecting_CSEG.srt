1
00:00:00,399 --> 00:00:05,359
so we had said that smm can be used for

2
00:00:03,360 --> 00:00:07,839
the typical power management type things

3
00:00:05,359 --> 00:00:10,400
but it also can be used by biospenders

4
00:00:07,839 --> 00:00:12,080
to place some proprietary code in a

5
00:00:10,400 --> 00:00:14,160
place that it generally can't be

6
00:00:12,080 --> 00:00:16,720
tampered with so in order to achieve

7
00:00:14,160 --> 00:00:18,320
that the bios vendor must take advantage

8
00:00:16,720 --> 00:00:20,880
of some of the hardware restriction

9
00:00:18,320 --> 00:00:22,320
mechanisms that intel has built in

10
00:00:20,880 --> 00:00:24,560
so how do they do that how do they

11
00:00:22,320 --> 00:00:25,840
protect compatible smm

12
00:00:24,560 --> 00:00:29,279
well the first thing is they have to

13
00:00:25,840 --> 00:00:31,599
enable the feature using that gsm ram e

14
00:00:29,279 --> 00:00:34,320
flag that we saw just a second ago

15
00:00:31,599 --> 00:00:36,160
and then they have to actually open up

16
00:00:34,320 --> 00:00:38,079
the memory range to say i want to be

17
00:00:36,160 --> 00:00:40,879
writing to the sm ram i want to be

18
00:00:38,079 --> 00:00:42,239
writing to the ram not that video memory

19
00:00:40,879 --> 00:00:44,640
so they have to make it so that the

20
00:00:42,239 --> 00:00:46,320
decoding decodes to ram instead of video

21
00:00:44,640 --> 00:00:47,920
memory and then after they have

22
00:00:46,320 --> 00:00:49,840
successfully written the code in there

23
00:00:47,920 --> 00:00:52,079
then they have to lock it back down so

24
00:00:49,840 --> 00:00:55,120
no one can ever unlock it again

25
00:00:52,079 --> 00:00:59,039
so let's see how that works well back to

26
00:00:55,120 --> 00:01:00,640
sm ram c and here's our gsm ram e

27
00:00:59,039 --> 00:01:02,079
there's going to be two other bits that

28
00:01:00,640 --> 00:01:04,960
we're going to care about there's the d

29
00:01:02,079 --> 00:01:07,119
open bit this d open bit is going to be

30
00:01:04,960 --> 00:01:10,240
what makes it so that writes to that hex

31
00:01:07,119 --> 00:01:11,600
30 000 are writing to ram instead of to

32
00:01:10,240 --> 00:01:14,080
video memory

33
00:01:11,600 --> 00:01:16,640
and then d lock is what's going to be

34
00:01:14,080 --> 00:01:18,400
set later on in order to make it so that

35
00:01:16,640 --> 00:01:20,479
no one is allowed to open it anymore so

36
00:01:18,400 --> 00:01:22,479
you open it up write your code in and

37
00:01:20,479 --> 00:01:23,280
you lock it so no one else can write

38
00:01:22,479 --> 00:01:25,759
there

39
00:01:23,280 --> 00:01:27,280
there is also a d closed but we're going

40
00:01:25,759 --> 00:01:30,000
to pretty much ignore that that really

41
00:01:27,280 --> 00:01:31,920
just has to do with if smram itself

42
00:01:30,000 --> 00:01:34,000
actually wants to write to video memory

43
00:01:31,920 --> 00:01:35,759
so we don't care about that for now so

44
00:01:34,000 --> 00:01:37,520
there's going to be two toggles that the

45
00:01:35,759 --> 00:01:39,680
bios is going to be flipping to do their

46
00:01:37,520 --> 00:01:41,680
job so let's see how that works going

47
00:01:39,680 --> 00:01:44,799
back to this let's start with the bios

48
00:01:41,680 --> 00:01:47,520
doing d open equals one so flip the

49
00:01:44,799 --> 00:01:50,880
toggle up that means that video memory

50
00:01:47,520 --> 00:01:54,000
now translates through to dram instead

51
00:01:50,880 --> 00:01:57,439
of video memory then the bios may copy

52
00:01:54,000 --> 00:01:59,600
its relocating sm ram sm cine system

53
00:01:57,439 --> 00:02:01,280
management interrupt handler etc they

54
00:01:59,600 --> 00:02:02,960
can copy that down there if they hadn't

55
00:02:01,280 --> 00:02:05,280
said the open that wouldn't work they

56
00:02:02,960 --> 00:02:06,399
would have been writing to video

57
00:02:05,280 --> 00:02:08,399
then they can go ahead and put it

58
00:02:06,399 --> 00:02:09,920
wherever else they wanted do the same

59
00:02:08,399 --> 00:02:12,319
thing as before cause a system

60
00:02:09,920 --> 00:02:14,720
management interrupt miss frizzle loses

61
00:02:12,319 --> 00:02:16,400
her face goes into the shadow realm and

62
00:02:14,720 --> 00:02:19,120
then you just have the typical

63
00:02:16,400 --> 00:02:20,239
relocation change your sm base blah blah

64
00:02:19,120 --> 00:02:22,400
blah

65
00:02:20,239 --> 00:02:24,640
and exactly the same as before resume

66
00:02:22,400 --> 00:02:27,360
back out and you've successfully changed

67
00:02:24,640 --> 00:02:29,599
sm base now the bios vendor is going to

68
00:02:27,360 --> 00:02:31,360
set d open to zero

69
00:02:29,599 --> 00:02:33,920
and that's going to go ahead and flip

70
00:02:31,360 --> 00:02:36,080
the switch so that this dram is no

71
00:02:33,920 --> 00:02:39,120
longer accessible now this is going to

72
00:02:36,080 --> 00:02:42,160
translate to video memory not dram next

73
00:02:39,120 --> 00:02:44,080
they're going to set d lock equal to 1.

74
00:02:42,160 --> 00:02:46,800
when you do that that means that you're

75
00:02:44,080 --> 00:02:48,720
no longer allowed to flip d open open

76
00:02:46,800 --> 00:02:51,200
you're not allowed to set this to 1. so

77
00:02:48,720 --> 00:02:52,879
that means no one else would be able to

78
00:02:51,200 --> 00:02:55,680
write 2dram here because it's

79
00:02:52,879 --> 00:02:58,159
continuously going to be video memory so

80
00:02:55,680 --> 00:03:00,800
effectively that control goes away and

81
00:02:58,159 --> 00:03:03,599
now this range is protected

82
00:03:00,800 --> 00:03:06,000
well that's great except for the fact

83
00:03:03,599 --> 00:03:09,360
that i said that they typically remove

84
00:03:06,000 --> 00:03:11,680
some code elsewhere so the actual sm

85
00:03:09,360 --> 00:03:13,840
base is now one two three zero zero zero

86
00:03:11,680 --> 00:03:14,720
and the actual dark sonic is hiding out

87
00:03:13,840 --> 00:03:17,680
there

88
00:03:14,720 --> 00:03:19,440
so cool this d lock and d open and stuff

89
00:03:17,680 --> 00:03:22,000
that helps protect here which is great

90
00:03:19,440 --> 00:03:24,000
if you never move out of that 128

91
00:03:22,000 --> 00:03:26,879
kilobytes of memory but i said most

92
00:03:24,000 --> 00:03:28,799
people do so we got to understand also

93
00:03:26,879 --> 00:03:30,640
how you're going to protect this up here

94
00:03:28,799 --> 00:03:33,040
all right well let's get started on

95
00:03:30,640 --> 00:03:35,200
understanding the attacking smm threat

96
00:03:33,040 --> 00:03:38,239
tree well the attacker's goal is to

97
00:03:35,200 --> 00:03:40,000
write malware into sm ram and well how

98
00:03:38,239 --> 00:03:42,000
can they do that well one way is they

99
00:03:40,000 --> 00:03:42,959
can just go ahead and infect the spy

100
00:03:42,000 --> 00:03:44,720
flash

101
00:03:42,959 --> 00:03:47,120
and that would bring us back to the

102
00:03:44,720 --> 00:03:49,280
previous tree well we know that like if

103
00:03:47,120 --> 00:03:51,680
you infect the bios the bio sets up the

104
00:03:49,280 --> 00:03:53,040
sm ram and therefore you as an attacker

105
00:03:51,680 --> 00:03:55,840
are guaranteed to be able to write

106
00:03:53,040 --> 00:03:57,519
malware into assam rap so cool infect

107
00:03:55,840 --> 00:03:58,480
the bios we have already seen how that

108
00:03:57,519 --> 00:04:00,560
works

109
00:03:58,480 --> 00:04:03,120
but now let's say the attacker is just

110
00:04:00,560 --> 00:04:05,360
running on the cpu and they want to

111
00:04:03,120 --> 00:04:07,439
infect the sm-ram

112
00:04:05,360 --> 00:04:10,319
well the first defense against that is

113
00:04:07,439 --> 00:04:13,599
the c-seg defense this sm ram c d-lock

114
00:04:10,319 --> 00:04:15,840
is one and smrmc d-open is zero that's

115
00:04:13,599 --> 00:04:18,560
going to stop a attacker on the cpu from

116
00:04:15,840 --> 00:04:21,519
just writing into that sm ram range

117
00:04:18,560 --> 00:04:23,759
now the attacker because ram and this is

118
00:04:21,519 --> 00:04:26,160
sm ram it's ram it's just dram behind

119
00:04:23,759 --> 00:04:28,400
the scenes and so if an attacker can

120
00:04:26,160 --> 00:04:30,320
write to that physical address then they

121
00:04:28,400 --> 00:04:33,040
can successfully infect it so one way

122
00:04:30,320 --> 00:04:35,440
they could do that is via dma direct

123
00:04:33,040 --> 00:04:38,080
memory access and they could do that via

124
00:04:35,440 --> 00:04:41,120
some peripheral that has dma access to

125
00:04:38,080 --> 00:04:41,120
the ram

