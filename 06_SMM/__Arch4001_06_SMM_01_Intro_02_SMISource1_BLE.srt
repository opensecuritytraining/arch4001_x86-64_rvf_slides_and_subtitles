1
00:00:00,080 --> 00:00:03,280
so out of all those different smi

2
00:00:01,680 --> 00:00:05,520
possibilities these are the ones that

3
00:00:03,280 --> 00:00:07,440
i'm going to go over quickly so let's

4
00:00:05,520 --> 00:00:09,360
look at this first smi source which

5
00:00:07,440 --> 00:00:11,679
should be familiar to us

6
00:00:09,360 --> 00:00:13,200
it was the use of bios lock enable and

7
00:00:11,679 --> 00:00:15,759
bios write enable

8
00:00:13,200 --> 00:00:17,920
we said that if the cpu sees that bios

9
00:00:15,759 --> 00:00:20,480
control bios write enable is set to zero

10
00:00:17,920 --> 00:00:22,480
then it's going to write it to become

11
00:00:20,480 --> 00:00:25,039
bios right enable equal to one happy

12
00:00:22,480 --> 00:00:26,720
little elves down in the lpc hardware

13
00:00:25,039 --> 00:00:27,680
are going to have conditional logic that

14
00:00:26,720 --> 00:00:29,599
says

15
00:00:27,680 --> 00:00:32,480
if someone is trying to write bias write

16
00:00:29,599 --> 00:00:35,040
enable equal to one and bios control

17
00:00:32,480 --> 00:00:37,520
bios lock enable is set then it'll fire

18
00:00:35,040 --> 00:00:39,840
off a smi system management interrupt

19
00:00:37,520 --> 00:00:41,920
that'll cause the cpu to go into system

20
00:00:39,840 --> 00:00:44,160
measurement mode there will be a system

21
00:00:41,920 --> 00:00:45,280
management interrupt handler in smram

22
00:00:44,160 --> 00:00:47,120
which we'll learn about in the next

23
00:00:45,280 --> 00:00:48,800
section and it's going to get a chance

24
00:00:47,120 --> 00:00:50,879
to decide whether it will allow the

25
00:00:48,800 --> 00:00:52,800
right to proceed or not

26
00:00:50,879 --> 00:00:55,039
and basically that's generally going to

27
00:00:52,800 --> 00:00:56,719
be for reasons of you know the access

28
00:00:55,039 --> 00:00:58,559
control will say okay if i see that

29
00:00:56,719 --> 00:01:00,719
there is for instance a firmware update

30
00:00:58,559 --> 00:01:02,559
occurring then maybe i'll allow it or if

31
00:01:00,719 --> 00:01:06,159
i see for instance that you're writing

32
00:01:02,559 --> 00:01:07,760
to non-volatile ram nvram the variables

33
00:01:06,159 --> 00:01:10,000
that are used very commonly on things

34
00:01:07,760 --> 00:01:12,320
like uefi then maybe it'll allow it to

35
00:01:10,000 --> 00:01:14,560
those particular addresses but not to

36
00:01:12,320 --> 00:01:17,040
other different addresses so this just

37
00:01:14,560 --> 00:01:19,280
gives a more sort of feature-rich

38
00:01:17,040 --> 00:01:20,960
capability to provide access control and

39
00:01:19,280 --> 00:01:23,119
that you can put sort of any arbitrary

40
00:01:20,960 --> 00:01:25,360
logic here but unfortunately it's a

41
00:01:23,119 --> 00:01:27,280
design where ultimately the flexibility

42
00:01:25,360 --> 00:01:30,159
was a little bit too much and it led to

43
00:01:27,280 --> 00:01:33,680
vulnerabilities so in this big list of

44
00:01:30,159 --> 00:01:35,360
smi causes we scroll down and there we

45
00:01:33,680 --> 00:01:37,680
go this is the thing that matters there

46
00:01:35,360 --> 00:01:39,600
it's called the tco smi

47
00:01:37,680 --> 00:01:41,920
and it happens with the change of the

48
00:01:39,600 --> 00:01:43,680
bios write enable so going back to this

49
00:01:41,920 --> 00:01:45,920
big list of smis we have the first

50
00:01:43,680 --> 00:01:46,799
column was the cause of the smi and then

51
00:01:45,920 --> 00:01:48,720
we're going to care about this

52
00:01:46,799 --> 00:01:50,000
additional enables and where it's

53
00:01:48,720 --> 00:01:52,799
reported

54
00:01:50,000 --> 00:01:54,960
so the additional enables for this

55
00:01:52,799 --> 00:01:57,119
is going to be the bios lock enable

56
00:01:54,960 --> 00:02:00,000
equals one so that's fine

57
00:01:57,119 --> 00:02:03,280
and the cause of it is the tco total

58
00:02:00,000 --> 00:02:05,920
cost of ownership smi which is the

59
00:02:03,280 --> 00:02:08,239
changing of bios right enable from zero

60
00:02:05,920 --> 00:02:10,319
to one and then there's going to be some

61
00:02:08,239 --> 00:02:11,760
status bit somewhere that the system

62
00:02:10,319 --> 00:02:13,920
measurement interrupt handler can look

63
00:02:11,760 --> 00:02:16,239
up in order to figure out why it's here

64
00:02:13,920 --> 00:02:19,040
on the 500 series chipsets it looks

65
00:02:16,239 --> 00:02:21,040
basically the same you got the tco smis

66
00:02:19,040 --> 00:02:22,400
you've got the lock enable bit instead

67
00:02:21,040 --> 00:02:24,400
of bias lock enable

68
00:02:22,400 --> 00:02:26,640
and it's set when the write protect

69
00:02:24,400 --> 00:02:28,959
disable bit is set from 0 to 1 which was

70
00:02:26,640 --> 00:02:31,280
basically the bios write enable so just

71
00:02:28,959 --> 00:02:33,280
to remind you about how those registers

72
00:02:31,280 --> 00:02:36,080
look you have the bios control which is

73
00:02:33,280 --> 00:02:38,800
at the lpc device bus 0 device 31

74
00:02:36,080 --> 00:02:40,800
function 0 offset dc there as well as

75
00:02:38,800 --> 00:02:42,640
the bios lock enable and the bios write

76
00:02:40,800 --> 00:02:44,319
enable

77
00:02:42,640 --> 00:02:46,400
now i would just point out at this point

78
00:02:44,319 --> 00:02:48,480
that there was this other field that

79
00:02:46,400 --> 00:02:51,280
says write attempted to the bios will

80
00:02:48,480 --> 00:02:53,519
cause an smi it says additional enables

81
00:02:51,280 --> 00:02:55,120
is bios write enable is equal to one

82
00:02:53,519 --> 00:02:56,959
pretty sure that should be bios write

83
00:02:55,120 --> 00:02:58,560
enable is equal to zero so if it's

84
00:02:56,959 --> 00:03:00,560
non-writable and someone attempts to

85
00:02:58,560 --> 00:03:02,560
write to the bios that will cause a

86
00:03:00,560 --> 00:03:04,720
system management interrupt

87
00:03:02,560 --> 00:03:06,640
and then on the 500 series basically the

88
00:03:04,720 --> 00:03:08,959
same thing except it's lacking the typo

89
00:03:06,640 --> 00:03:10,959
so if that's zero and someone attempts

90
00:03:08,959 --> 00:03:13,760
to write to the bios then that'll cause

91
00:03:10,959 --> 00:03:13,760
the interrupt

