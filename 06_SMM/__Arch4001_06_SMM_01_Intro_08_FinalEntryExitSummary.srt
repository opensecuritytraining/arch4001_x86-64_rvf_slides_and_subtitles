1
00:00:00,34 --> 00:00:00,67
All right

2
00:00:00,67 --> 00:00:03,63
So now let's sum things up with respect to entering

3
00:00:03,63 --> 00:00:04,1
and exiting

4
00:00:04,1 --> 00:00:07,81
S mm So when a system management interrupt fires the

5
00:00:07,81 --> 00:00:10,24
processor is going to need to wait for,

6
00:00:10,24 --> 00:00:10,48
you know,

7
00:00:10,48 --> 00:00:14,37
the current assembly instructions to complete and things like stores

8
00:00:14,37 --> 00:00:16,25
to complete as well stores to memory

9
00:00:16,64 --> 00:00:17,71
So it basically needs to,

10
00:00:17,71 --> 00:00:18,23
you know,

11
00:00:18,24 --> 00:00:20,94
finish up doing what it's doing before it can stop

12
00:00:20,95 --> 00:00:24,56
and go off to sme out to S mm And

13
00:00:24,56 --> 00:00:26,44
so that's why we say that s me interrupts are

14
00:00:26,44 --> 00:00:30,51
handled on an architecturally defined interruptible point in program execution

15
00:00:30,74 --> 00:00:32,78
such as instruction boundaries module

16
00:00:32,78 --> 00:00:32,87
Oh,

17
00:00:32,87 --> 00:00:33,44
the fact that,

18
00:00:33,44 --> 00:00:33,71
you know,

19
00:00:33,71 --> 00:00:36,35
there's all of this pipe lining and everything going on

20
00:00:36,35 --> 00:00:37,16
behind the scenes

21
00:00:37,74 --> 00:00:39,31
Once an sme fires,

22
00:00:39,39 --> 00:00:42,97
the processor is going to automatically save context and state

23
00:00:42,98 --> 00:00:46,57
about what's going on right now into SM room

24
00:00:46,69 --> 00:00:49,03
So that's very similar to the way that we saw

25
00:00:49,03 --> 00:00:51,24
with the interrupt descriptor table,

26
00:00:51,39 --> 00:00:54,36
that when they interrupt assembly instruction happens,

27
00:00:54,63 --> 00:00:56,98
some save state is going to be stored onto the

28
00:00:56,98 --> 00:00:59,18
stack and that's going to be popped back off the

29
00:00:59,18 --> 00:01:01,42
stack by an irate assembly instruction

30
00:01:01,43 --> 00:01:03,14
When you're returning from an interrupt

31
00:01:03,15 --> 00:01:05,51
So that's just a general principle about interrupt

32
00:01:05,52 --> 00:01:06,28
If you're interrupted,

33
00:01:06,28 --> 00:01:09,05
you need to save state to go somewhere else and

34
00:01:09,05 --> 00:01:10,8
then come back and restore the state

35
00:01:11,34 --> 00:01:13,96
So in this case the go somewhere else is to

36
00:01:13,97 --> 00:01:16,32
go to SM room where it's going to run code

37
00:01:16,36 --> 00:01:18,95
but it's also going to save the state into SM

38
00:01:18,95 --> 00:01:19,24
room

39
00:01:19,46 --> 00:01:21,02
And then also and importantly,

40
00:01:21,02 --> 00:01:23,11
if you have a multi core processor,

41
00:01:23,44 --> 00:01:26,06
none of the none of the cores are going to

42
00:01:26,06 --> 00:01:29,86
start executing system management interrupt handler code until all of

43
00:01:29,86 --> 00:01:33,06
the cores have entered into system management mode

44
00:01:34,24 --> 00:01:36,75
Then we said exiting out of S mm is via

45
00:01:36,75 --> 00:01:40,55
the resume assembly instruction and this is the only way

46
00:01:40,55 --> 00:01:43,81
to normally exit S mm other than just turning off

47
00:01:43,81 --> 00:01:44,37
the computer

48
00:01:44,37 --> 00:01:45,86
So restarting or shutting down,

49
00:01:46,44 --> 00:01:49,6
resume is going to be responsible for reading back out

50
00:01:49,6 --> 00:01:51,78
the contents of sm RAM,

51
00:01:51,83 --> 00:01:54,16
the contents of safe state that have been stored in

52
00:01:54,16 --> 00:01:58,02
SmR um and restoring them to the relevant registers as

53
00:01:58,02 --> 00:01:58,69
part of that

54
00:01:58,69 --> 00:02:02,8
If anything about the state that is being replaced into

55
00:02:02,8 --> 00:02:04,29
registers is invalid,

56
00:02:04,45 --> 00:02:07,53
the system will just straight up shut down and then

57
00:02:07,53 --> 00:02:11,08
finally resume can only be executed from within the system

58
00:02:11,08 --> 00:02:11,76
management mode

59
00:02:11,89 --> 00:02:13,82
If you try to run this from anywhere else,

60
00:02:13,83 --> 00:02:16,77
it's just going to cause an invalid opcode exception

61
00:02:17,34 --> 00:02:20,5
And so specifically if you see a resume and assembly

62
00:02:20,5 --> 00:02:21,02
instruction,

63
00:02:21,02 --> 00:02:23,04
which is up code zero F A

64
00:02:23,11 --> 00:02:26,08
If you see that while you're disassembling some code somewhere

65
00:02:26,1 --> 00:02:29,25
if that code is valid and correctly disassembled and you're

66
00:02:29,25 --> 00:02:32,06
not just erroneously disassembling some data,

67
00:02:32,33 --> 00:02:35,03
then you can be pretty confident that you have actually

68
00:02:35,03 --> 00:02:37,91
found some system management interrupt handler code

69
00:02:38,14 --> 00:02:38,59
Okay,

70
00:02:38,59 --> 00:02:41,34
that's all I want to talk about for the basics

71
00:02:41,34 --> 00:02:42,52
of system management mode,

72
00:02:42,52 --> 00:02:46,59
entering exiting system management interrupts various different forms,

73
00:02:46,59 --> 00:02:48,39
many other ones that we haven't talked about in this

74
00:02:48,39 --> 00:02:48,92
class

75
00:02:49,22 --> 00:02:51,51
Now we need to move on to system management RAM

76
00:02:51,51 --> 00:02:55,15
where the actual system management interrupt code runs from

