1
00:00:00,240 --> 00:00:06,240
okay our next type of smi implementation

2
00:00:03,120 --> 00:00:08,240
bug is called a confused deputy attack

3
00:00:06,240 --> 00:00:10,639
and the mitigations are going to be all

4
00:00:08,240 --> 00:00:12,320
these sort of comma mitigations plus one

5
00:00:10,639 --> 00:00:13,920
extra one that has to do with the

6
00:00:12,320 --> 00:00:15,200
particulars of this attack which we'll

7
00:00:13,920 --> 00:00:17,359
talk about now

8
00:00:15,200 --> 00:00:19,760
first if you haven't heard of it before

9
00:00:17,359 --> 00:00:22,480
the confused deputy attack is a general

10
00:00:19,760 --> 00:00:24,320
class of logic vulnerabilities

11
00:00:22,480 --> 00:00:26,320
and it basically has to do with when

12
00:00:24,320 --> 00:00:28,560
there's interactions between a

13
00:00:26,320 --> 00:00:31,119
privileged entity which is the deputy

14
00:00:28,560 --> 00:00:33,040
and the unprivileged entity which is the

15
00:00:31,119 --> 00:00:34,800
notional inmate here

16
00:00:33,040 --> 00:00:37,920
so you know someone's jailed someone's

17
00:00:34,800 --> 00:00:40,160
de-privileged someone's on a time out

18
00:00:37,920 --> 00:00:41,920
and so they ask the deputy to do

19
00:00:40,160 --> 00:00:44,160
something and the deputy may think this

20
00:00:41,920 --> 00:00:46,160
is an innocuous action that you know

21
00:00:44,160 --> 00:00:48,239
doesn't give the inmate any particular

22
00:00:46,160 --> 00:00:49,840
advantage but if it does give them an

23
00:00:48,239 --> 00:00:51,520
advantage and if it does achieve

24
00:00:49,840 --> 00:00:53,920
something they couldn't achieve on their

25
00:00:51,520 --> 00:00:56,399
own from behind bars then this is a

26
00:00:53,920 --> 00:00:58,480
confused deputy because they have just

27
00:00:56,399 --> 00:01:00,160
done something that the attacker the

28
00:00:58,480 --> 00:01:02,160
inmate couldn't do on their own so

29
00:01:00,160 --> 00:01:04,159
confused deputy attacks were initially

30
00:01:02,160 --> 00:01:06,560
considered in the context of operating

31
00:01:04,159 --> 00:01:08,720
systems because of course you know you

32
00:01:06,560 --> 00:01:10,400
have the situation of unprivileged user

33
00:01:08,720 --> 00:01:11,280
space code interacting with a privileged

34
00:01:10,400 --> 00:01:12,799
kernel

35
00:01:11,280 --> 00:01:15,040
or you could have you know a

36
00:01:12,799 --> 00:01:16,880
unprivileged non-root process

37
00:01:15,040 --> 00:01:19,360
interacting with the root process

38
00:01:16,880 --> 00:01:21,759
but of course when it comes to privilege

39
00:01:19,360 --> 00:01:24,720
user spaces to kernel as kernel is to

40
00:01:21,759 --> 00:01:26,560
smm smm has more privilege and power

41
00:01:24,720 --> 00:01:29,280
more capability to access all physical

42
00:01:26,560 --> 00:01:32,000
memory everywhere without inspection

43
00:01:29,280 --> 00:01:34,159
so in the context of smi vulnerabilities

44
00:01:32,000 --> 00:01:37,119
what does this look like so before we

45
00:01:34,159 --> 00:01:39,680
talked about how port b2 can be used as

46
00:01:37,119 --> 00:01:41,759
a sort of system call interface and b3

47
00:01:39,680 --> 00:01:43,840
can hold an extra byte of data

48
00:01:41,759 --> 00:01:46,320
but frequently that you know one byte of

49
00:01:43,840 --> 00:01:48,799
data in b3 and one byte of data in b2 is

50
00:01:46,320 --> 00:01:51,680
not enough to provide a useful system

51
00:01:48,799 --> 00:01:53,360
call interface and so what vendors tend

52
00:01:51,680 --> 00:01:55,600
to do is they will use the general

53
00:01:53,360 --> 00:01:58,640
purpose registers as the additional

54
00:01:55,600 --> 00:02:00,719
parameters for that notional system call

55
00:01:58,640 --> 00:02:02,799
so they can basically you know write to

56
00:02:00,719 --> 00:02:04,799
port b2 but before they do that they

57
00:02:02,799 --> 00:02:06,880
fill in some data in some general

58
00:02:04,799 --> 00:02:08,879
purpose registers then when the smi

59
00:02:06,880 --> 00:02:10,560
handler is invoked it expects that

60
00:02:08,879 --> 00:02:12,400
according to you know

61
00:02:10,560 --> 00:02:14,640
whatever protocol the you know vendor

62
00:02:12,400 --> 00:02:16,560
has decided to create it expects that

63
00:02:14,640 --> 00:02:19,680
there you know might be for

64
00:02:16,560 --> 00:02:22,560
port b2 right of you know seven they

65
00:02:19,680 --> 00:02:25,360
expect a pointer in rbx and a pointer in

66
00:02:22,560 --> 00:02:28,000
rcx for port b2

67
00:02:25,360 --> 00:02:30,720
right of 8 they maybe only expect a

68
00:02:28,000 --> 00:02:32,720
pointer in rax so they get to decide

69
00:02:30,720 --> 00:02:34,480
what the protocol is how the system

70
00:02:32,720 --> 00:02:36,640
calls work and so forth

71
00:02:34,480 --> 00:02:38,080
but if you imagine that they're passing

72
00:02:36,640 --> 00:02:39,840
around pointers well the pointers are

73
00:02:38,080 --> 00:02:42,560
going to point to some sort of memory

74
00:02:39,840 --> 00:02:44,720
and the person outside of smm probably

75
00:02:42,560 --> 00:02:46,560
is not going to know where anything is

76
00:02:44,720 --> 00:02:47,519
in smram because they can't see it so

77
00:02:46,560 --> 00:02:49,120
they're not going to be pointing in

78
00:02:47,519 --> 00:02:50,879
there so they're probably going to be

79
00:02:49,120 --> 00:02:52,720
pointing at some sort of

80
00:02:50,879 --> 00:02:55,519
data structure out in operating system

81
00:02:52,720 --> 00:02:57,760
memory and then the smram or sorry the

82
00:02:55,519 --> 00:02:59,599
smi handler may read from that data or

83
00:02:57,760 --> 00:03:02,400
it may write to that data

84
00:02:59,599 --> 00:03:04,239
if it's the case that the smi handler

85
00:03:02,400 --> 00:03:05,599
looks at some pointer and then says oh

86
00:03:04,239 --> 00:03:08,159
yeah i'm going to write to wherever you

87
00:03:05,599 --> 00:03:10,720
tell me to write well that's a bad time

88
00:03:08,159 --> 00:03:13,360
because if the attacker points it inside

89
00:03:10,720 --> 00:03:15,360
of smram and if the smi handler just

90
00:03:13,360 --> 00:03:17,120
naively writes wherever the attacker

91
00:03:15,360 --> 00:03:19,360
controlled pointer says

92
00:03:17,120 --> 00:03:22,319
then they could be clobbering themselves

93
00:03:19,360 --> 00:03:24,400
in smram writing you know a

94
00:03:22,319 --> 00:03:26,000
you know single byte to change some

95
00:03:24,400 --> 00:03:28,480
configuration data a single byte to

96
00:03:26,000 --> 00:03:30,080
change sm base in the save state area

97
00:03:28,480 --> 00:03:32,720
things like that

98
00:03:30,080 --> 00:03:34,400
so that's not a good time this has been

99
00:03:32,720 --> 00:03:36,400
mitigated a little bit in the open

100
00:03:34,400 --> 00:03:38,879
source code by the introduction of a

101
00:03:36,400 --> 00:03:41,200
call to smm isbuffer outside of smm

102
00:03:38,879 --> 00:03:43,360
valid and that checks to make sure that

103
00:03:41,200 --> 00:03:45,360
you know wherever it's trying to write

104
00:03:43,360 --> 00:03:47,840
to should be straight up outside of srm

105
00:03:45,360 --> 00:03:50,159
so it's not clobbering itself in sram

106
00:03:47,840 --> 00:03:53,200
but that's really just kind of a partial

107
00:03:50,159 --> 00:03:54,400
mitigation right because there are other

108
00:03:53,200 --> 00:03:56,080
things other privilege entities

109
00:03:54,400 --> 00:03:57,680
especially in you know modern windows

110
00:03:56,080 --> 00:03:58,720
systems with virtualization based

111
00:03:57,680 --> 00:04:01,840
security

112
00:03:58,720 --> 00:04:03,680
if the smi handler just says oh it's not

113
00:04:01,840 --> 00:04:06,159
an smm so it's not my problem and it

114
00:04:03,680 --> 00:04:08,400
just writes wherever an attacker says

115
00:04:06,159 --> 00:04:09,760
then the attacker who's in a jailed

116
00:04:08,400 --> 00:04:12,000
operating system inside of

117
00:04:09,760 --> 00:04:14,439
virtualization based security could go

118
00:04:12,000 --> 00:04:16,880
ahead and write outside of that you know

119
00:04:14,439 --> 00:04:19,280
virtualization jail and clobber the

120
00:04:16,880 --> 00:04:22,560
hypervisor clobber the more privileged

121
00:04:19,280 --> 00:04:25,520
virtualization system virtual machines

122
00:04:22,560 --> 00:04:27,600
so yeah like this is a general ongoing

123
00:04:25,520 --> 00:04:30,000
problem essentially

124
00:04:27,600 --> 00:04:31,600
just the the general idea of smi

125
00:04:30,000 --> 00:04:33,840
handlers not properly treating

126
00:04:31,600 --> 00:04:35,919
attacker-controlled input safely

127
00:04:33,840 --> 00:04:38,320
and for this one there's not you know a

128
00:04:35,919 --> 00:04:41,280
really good solution for not clobbering

129
00:04:38,320 --> 00:04:43,680
anything sensitive outside in os land

130
00:04:41,280 --> 00:04:45,520
other than the idea that

131
00:04:43,680 --> 00:04:47,840
the system call interface between

132
00:04:45,520 --> 00:04:50,320
operating systems and smm needs to be

133
00:04:47,840 --> 00:04:53,040
more restrictive needs to have you know

134
00:04:50,320 --> 00:04:55,120
fixed expectations of there has to be

135
00:04:53,040 --> 00:04:57,680
exactly this physical address is always

136
00:04:55,120 --> 00:04:59,199
only used for input and output the smi

137
00:04:57,680 --> 00:05:02,160
handler can make sure that you know

138
00:04:59,199 --> 00:05:06,240
nothing else is there that kind of thing

139
00:05:02,160 --> 00:05:08,000
so that was the confused deputy attacks

140
00:05:06,240 --> 00:05:10,720
all of the same defensive things apply

141
00:05:08,000 --> 00:05:12,880
auditing the code privilege reduction

142
00:05:10,720 --> 00:05:15,759
right you could see how if an stm or

143
00:05:12,880 --> 00:05:18,160
page tables restricted the ram that smi

144
00:05:15,759 --> 00:05:20,720
handler could write to then for instance

145
00:05:18,160 --> 00:05:22,000
you know it wouldn't be able to clobber

146
00:05:20,720 --> 00:05:23,600
the hypervisor memory right the

147
00:05:22,000 --> 00:05:25,520
hypervisor memory would just be straight

148
00:05:23,600 --> 00:05:26,960
up outside of its range of memory it

149
00:05:25,520 --> 00:05:27,840
could write to

150
00:05:26,960 --> 00:05:29,840
and then

151
00:05:27,840 --> 00:05:32,639
things like ppm if the attacker used

152
00:05:29,840 --> 00:05:34,479
confused deputy to write into smram and

153
00:05:32,639 --> 00:05:36,720
then get code execution there while the

154
00:05:34,479 --> 00:05:39,280
ppam should hopefully detect that and

155
00:05:36,720 --> 00:05:41,199
then again checking for overlap with you

156
00:05:39,280 --> 00:05:43,520
know whether pointers point inside of

157
00:05:41,199 --> 00:05:45,199
smrm it would be clobbering itself or

158
00:05:43,520 --> 00:05:47,360
whether they point to you know operating

159
00:05:45,199 --> 00:05:50,080
system regions that the smi handler

160
00:05:47,360 --> 00:05:52,240
somehow knows shouldn't be written to

161
00:05:50,080 --> 00:05:53,919
or better yet just create a reserved

162
00:05:52,240 --> 00:05:56,479
region so it doesn't have to worry about

163
00:05:53,919 --> 00:05:56,479
that

