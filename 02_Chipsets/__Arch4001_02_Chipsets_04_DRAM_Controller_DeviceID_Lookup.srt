1
00:00:00,719 --> 00:00:04,160
all right now we need to do the same

2
00:00:02,159 --> 00:00:06,640
thing to find your memory controller

3
00:00:04,160 --> 00:00:08,639
device id or dram controller device id

4
00:00:06,640 --> 00:00:10,400
or host bridge device id you know many

5
00:00:08,639 --> 00:00:12,400
different names for the same thing

6
00:00:10,400 --> 00:00:15,519
and that's going to be again go to the

7
00:00:12,400 --> 00:00:17,680
pci device look at 16 bits at a time and

8
00:00:15,519 --> 00:00:20,960
here you're going to do bus 0 device 0

9
00:00:17,680 --> 00:00:23,840
function 0 and offset 2. so this is

10
00:00:20,960 --> 00:00:25,680
where you're going to find the

11
00:00:23,840 --> 00:00:27,199
member dram controller host bridge

12
00:00:25,680 --> 00:00:28,800
whatever you want to call it

13
00:00:27,199 --> 00:00:31,039
i call it dram controller throughout

14
00:00:28,800 --> 00:00:33,600
most of the class and again you can have

15
00:00:31,039 --> 00:00:36,719
an option to do that in linux using ls

16
00:00:33,600 --> 00:00:38,640
pci saying bus 0 device 0 function 0

17
00:00:36,719 --> 00:00:39,760
then this offset 2 is what you're going

18
00:00:38,640 --> 00:00:42,480
to care about

19
00:00:39,760 --> 00:00:44,800
in mac os terminal using i o reg looking

20
00:00:42,480 --> 00:00:46,879
for the mchc

21
00:00:44,800 --> 00:00:49,920
and the offset two here is what you care

22
00:00:46,879 --> 00:00:53,199
about and chipset bus zero device serial

23
00:00:49,920 --> 00:00:54,640
function zero offset two two bytes and

24
00:00:53,199 --> 00:00:56,960
that'll give you what you care about but

25
00:00:54,640 --> 00:00:59,600
again look at the website not these

26
00:00:56,960 --> 00:01:01,039
because these will get out of date

27
00:00:59,600 --> 00:01:03,680
same thing as before in that you would

28
00:01:01,039 --> 00:01:05,600
historically look up the device id in

29
00:01:03,680 --> 00:01:06,640
the pci device ids list and it would

30
00:01:05,600 --> 00:01:08,799
tell you oh you've got a fourth

31
00:01:06,640 --> 00:01:10,560
generation core processor dram

32
00:01:08,799 --> 00:01:12,080
controller and then you'd say oh well i

33
00:01:10,560 --> 00:01:15,119
know i must have a fourth generation

34
00:01:12,080 --> 00:01:17,280
core as well processor and same thing as

35
00:01:15,119 --> 00:01:20,159
before you know the mac being a special

36
00:01:17,280 --> 00:01:22,240
snowflake its particular device id is

37
00:01:20,159 --> 00:01:25,119
not actually listed on this but it is

38
00:01:22,240 --> 00:01:27,600
actually found in the below datasheet

39
00:01:25,119 --> 00:01:29,439
so once again go to the website you know

40
00:01:27,600 --> 00:01:31,759
it'll have the instructions on how to

41
00:01:29,439 --> 00:01:33,600
pull out your dram controller device id

42
00:01:31,759 --> 00:01:35,280
and then it'll have a pointer to the

43
00:01:33,600 --> 00:01:36,960
right data sheets should go into the

44
00:01:35,280 --> 00:01:39,360
data sheets and make sure you can find

45
00:01:36,960 --> 00:01:40,799
that device id or its range inside of

46
00:01:39,360 --> 00:01:42,240
them because you're going to need to use

47
00:01:40,799 --> 00:01:44,880
those data sheets throughout the rest of

48
00:01:42,240 --> 00:01:44,880
the class

