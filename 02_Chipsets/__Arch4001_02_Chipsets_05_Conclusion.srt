1
00:00:00,160 --> 00:00:04,480
so in summary this whole section is just

2
00:00:02,320 --> 00:00:06,319
about us finding the right data sheets

3
00:00:04,480 --> 00:00:07,839
for identifying what kind of hardware

4
00:00:06,319 --> 00:00:09,200
you're dealing with because once we

5
00:00:07,839 --> 00:00:11,120
understand what hardware you're dealing

6
00:00:09,200 --> 00:00:12,880
with we understand where to look for

7
00:00:11,120 --> 00:00:15,040
access controls which is where we're

8
00:00:12,880 --> 00:00:17,680
ultimately trying to get so the dram

9
00:00:15,040 --> 00:00:19,439
controller and the lpc device are the

10
00:00:17,680 --> 00:00:22,080
two bits of information we need to

11
00:00:19,439 --> 00:00:24,480
uniquely identify the relevant hardware

12
00:00:22,080 --> 00:00:26,400
for checking the access controls so with

13
00:00:24,480 --> 00:00:28,560
that we're done with the chip set

14
00:00:26,400 --> 00:00:31,439
section and we are well on our way

15
00:00:28,560 --> 00:00:35,079
towards pcie ultimately on our way to

16
00:00:31,439 --> 00:00:35,079
flash right protection

