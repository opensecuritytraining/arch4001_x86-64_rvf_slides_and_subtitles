1
00:00:00,160 --> 00:00:05,440
back in the reset vector section we saw

2
00:00:02,560 --> 00:00:08,080
how an x86 processor's reset vector is

3
00:00:05,440 --> 00:00:11,440
the physical address seven f's followed

4
00:00:08,080 --> 00:00:13,200
by zero so four gigabytes minus 16 bytes

5
00:00:11,440 --> 00:00:15,200
that's the location where the processor

6
00:00:13,200 --> 00:00:17,600
fetches the first assembly instruction

7
00:00:15,200 --> 00:00:18,480
to run from the bios from the spy flash

8
00:00:17,600 --> 00:00:21,279
chip

9
00:00:18,480 --> 00:00:23,840
now the fact that this address can exist

10
00:00:21,279 --> 00:00:26,800
in you know physical memory but actually

11
00:00:23,840 --> 00:00:28,480
be translated down to the bio spy flash

12
00:00:26,800 --> 00:00:30,640
chip means there must be some sort of

13
00:00:28,480 --> 00:00:32,399
hardware decoding going on so that when

14
00:00:30,640 --> 00:00:34,960
the cpu tries to fetch the assembly

15
00:00:32,399 --> 00:00:37,760
instruction it reroutes and redirects

16
00:00:34,960 --> 00:00:39,920
the thing down to the spy flash chip

17
00:00:37,760 --> 00:00:42,640
so what if i told you that we could

18
00:00:39,920 --> 00:00:44,879
actually see and slightly control this

19
00:00:42,640 --> 00:00:46,160
sort of physical memory to spy flash

20
00:00:44,879 --> 00:00:48,320
chip mapping

21
00:00:46,160 --> 00:00:50,719
well we can and we're going to fiddle

22
00:00:48,320 --> 00:00:52,480
with some registers now in order to see

23
00:00:50,719 --> 00:00:54,399
a little bit about how we can control

24
00:00:52,480 --> 00:00:55,280
that and what we can't control about

25
00:00:54,399 --> 00:00:57,440
that

26
00:00:55,280 --> 00:00:59,760
now originally i had this material in

27
00:00:57,440 --> 00:01:02,480
the reset vector section but because of

28
00:00:59,760 --> 00:01:04,479
the vagaries of how intel has changed

29
00:01:02,480 --> 00:01:06,000
the location of where some of these

30
00:01:04,479 --> 00:01:07,119
magic bits are that i'm just going to

31
00:01:06,000 --> 00:01:09,920
point you at

32
00:01:07,119 --> 00:01:12,560
we had to wait until you understood

33
00:01:09,920 --> 00:01:14,000
which particular chipset and cpu you had

34
00:01:12,560 --> 00:01:15,520
so there's going to be different people

35
00:01:14,000 --> 00:01:16,640
with different generation of chipsets

36
00:01:15,520 --> 00:01:18,960
we'll have to look at different

37
00:01:16,640 --> 00:01:20,799
locations so i needed to make sure you

38
00:01:18,960 --> 00:01:22,720
understood that notion first before i

39
00:01:20,799 --> 00:01:24,400
give you different some different

40
00:01:22,720 --> 00:01:25,680
instructions for different people now

41
00:01:24,400 --> 00:01:27,520
we'll come back to this later in the

42
00:01:25,680 --> 00:01:29,520
class and it'll make even more sense

43
00:01:27,520 --> 00:01:31,840
then but for now i just kind of want to

44
00:01:29,520 --> 00:01:33,759
show you uh you know poking some magic

45
00:01:31,840 --> 00:01:35,040
register bits and having some sort of

46
00:01:33,759 --> 00:01:36,400
behavior

47
00:01:35,040 --> 00:01:39,280
so first i have to give you a little bit

48
00:01:36,400 --> 00:01:41,439
of history about why this is where it is

49
00:01:39,280 --> 00:01:43,439
on some particular chipsets so if you

50
00:01:41,439 --> 00:01:46,320
look at older intel documentation you

51
00:01:43,439 --> 00:01:48,399
would see references to the firmware hub

52
00:01:46,320 --> 00:01:50,079
and the firmware hub was the previous

53
00:01:48,399 --> 00:01:51,840
location where the bios was stored it

54
00:01:50,079 --> 00:01:54,720
was a particular non-volatile storage

55
00:01:51,840 --> 00:01:55,600
chip and it existed on the low pin count

56
00:01:54,720 --> 00:01:56,320
bus

57
00:01:55,600 --> 00:01:58,960
so

58
00:01:56,320 --> 00:02:01,040
lpc is just a low pin count meaning a

59
00:01:58,960 --> 00:02:03,920
small number of pins and a relatively

60
00:02:01,040 --> 00:02:05,439
slow bus used for accessing hardware so

61
00:02:03,920 --> 00:02:06,960
back in the data sheets for these older

62
00:02:05,439 --> 00:02:09,759
systems there was this thing called

63
00:02:06,960 --> 00:02:12,879
firmware hub decode enable and it talked

64
00:02:09,759 --> 00:02:14,959
about the low pin count bus device

65
00:02:12,879 --> 00:02:17,680
internal to the hardware so if we

66
00:02:14,959 --> 00:02:19,360
instead look at a more modern data sheet

67
00:02:17,680 --> 00:02:21,360
we will see that the exact same

68
00:02:19,360 --> 00:02:23,680
information exists but now it's called

69
00:02:21,360 --> 00:02:25,840
biosd code enable rather than firmware

70
00:02:23,680 --> 00:02:28,080
hub it exists in the same location it's

71
00:02:25,840 --> 00:02:29,920
on the lpc device that we talked about

72
00:02:28,080 --> 00:02:32,400
when we're talking about chipsets but we

73
00:02:29,920 --> 00:02:34,239
don't yet fully understand how pci works

74
00:02:32,400 --> 00:02:35,920
and what sort of address and coding is

75
00:02:34,239 --> 00:02:37,599
going on here that's why this is going

76
00:02:35,920 --> 00:02:39,360
to make more sense to you later on after

77
00:02:37,599 --> 00:02:41,280
we cover pci

78
00:02:39,360 --> 00:02:43,599
but the key thing that i want to point

79
00:02:41,280 --> 00:02:46,160
out is that despite the fact that the

80
00:02:43,599 --> 00:02:48,319
bios is now on a spy flash chip this

81
00:02:46,160 --> 00:02:51,200
decoding information is still just the

82
00:02:48,319 --> 00:02:52,879
same location in the lpc hardware device

83
00:02:51,200 --> 00:02:54,959
for legacy reasons

84
00:02:52,879 --> 00:02:57,440
so what exactly is in this information

85
00:02:54,959 --> 00:03:00,720
that's interesting to us well there's a

86
00:02:57,440 --> 00:03:03,440
bit 15 that has to do with the decoding

87
00:03:00,720 --> 00:03:06,080
of a 512 kilobyte

88
00:03:03,440 --> 00:03:08,800
bios range and specifically that range

89
00:03:06,080 --> 00:03:11,680
is three f's eight and four zeros all

90
00:03:08,800 --> 00:03:13,920
the way up to all f's so the end of the

91
00:03:11,680 --> 00:03:16,159
four gigabyte range so basically what

92
00:03:13,920 --> 00:03:19,040
it's saying is when you have this set to

93
00:03:16,159 --> 00:03:21,120
one then it will decode physical

94
00:03:19,040 --> 00:03:24,720
addresses like this

95
00:03:21,120 --> 00:03:28,239
into the end of the spy flash chip

96
00:03:24,720 --> 00:03:30,640
similarly this next bit 14 down here if

97
00:03:28,239 --> 00:03:33,040
this is set to 1 then it will decode 3

98
00:03:30,640 --> 00:03:36,000
fs and five zeros all the way up to

99
00:03:33,040 --> 00:03:38,159
three f's seven and four f's

100
00:03:36,000 --> 00:03:41,280
uh those sort of physical address ranges

101
00:03:38,159 --> 00:03:43,360
will be decoded into the next 512 block

102
00:03:41,280 --> 00:03:44,879
of the bios flash chip

103
00:03:43,360 --> 00:03:46,720
now the important thing to recognize

104
00:03:44,879 --> 00:03:49,200
here is that actually on this bit it

105
00:03:46,720 --> 00:03:51,200
says it's read only and therefore you

106
00:03:49,200 --> 00:03:52,720
can't actually flip it to zero you know

107
00:03:51,200 --> 00:03:54,319
despite the fact that it shows zero and

108
00:03:52,720 --> 00:03:57,360
says it can be disabled you can't

109
00:03:54,319 --> 00:04:00,480
actually flip it to zero but this bit 14

110
00:03:57,360 --> 00:04:02,319
you can it says it's read write so if we

111
00:04:00,480 --> 00:04:04,959
go in and we poke this bit and we set it

112
00:04:02,319 --> 00:04:06,720
to zero we would expect that addresses

113
00:04:04,959 --> 00:04:08,799
in this range which previously the

114
00:04:06,720 --> 00:04:11,040
hardware was automatically decoding

115
00:04:08,799 --> 00:04:13,519
attempts to access those physical memory

116
00:04:11,040 --> 00:04:16,079
ranges it would reroute those down to

117
00:04:13,519 --> 00:04:17,919
the spy flash chip now if we disable

118
00:04:16,079 --> 00:04:19,759
this by setting it to zero they'll just

119
00:04:17,919 --> 00:04:21,040
decode to nothing at all

120
00:04:19,759 --> 00:04:22,880
so that's what we're gonna do in this

121
00:04:21,040 --> 00:04:24,560
next lab we're just gonna you know go in

122
00:04:22,880 --> 00:04:26,560
find the right locations you know you'll

123
00:04:24,560 --> 00:04:28,479
understand it more in later on once you

124
00:04:26,560 --> 00:04:30,080
understand pci we're just going to go

125
00:04:28,479 --> 00:04:31,600
poke those bits and see that all of a

126
00:04:30,080 --> 00:04:32,639
sudden these physical address ranges

127
00:04:31,600 --> 00:04:34,560
disappear

128
00:04:32,639 --> 00:04:35,840
now this is the the part that i wanted

129
00:04:34,560 --> 00:04:37,759
to say that you know we had to

130
00:04:35,840 --> 00:04:38,880
understand which chipset you're using

131
00:04:37,759 --> 00:04:40,479
because

132
00:04:38,880 --> 00:04:43,520
intel has actually moved where these

133
00:04:40,479 --> 00:04:44,560
decode bits are so for people who are

134
00:04:43,520 --> 00:04:47,680
running

135
00:04:44,560 --> 00:04:49,199
9 series pchs and earlier you're going

136
00:04:47,680 --> 00:04:51,600
to be looking at you know one particular

137
00:04:49,199 --> 00:04:54,000
thing the lpc device for people running

138
00:04:51,600 --> 00:04:55,759
100 series chipsets and later you're

139
00:04:54,000 --> 00:04:58,320
going to be looking at a different thing

140
00:04:55,759 --> 00:05:00,320
the spy device so intel added a

141
00:04:58,320 --> 00:05:02,400
dedicated spy device which takes over

142
00:05:00,320 --> 00:05:04,320
some of the stuff that was in the lpc

143
00:05:02,400 --> 00:05:05,680
device for legacy reasons

144
00:05:04,320 --> 00:05:07,280
and puts it in the more appropriate

145
00:05:05,680 --> 00:05:09,600
location because you're actually using

146
00:05:07,280 --> 00:05:12,639
spy flash chips so this is just a

147
00:05:09,600 --> 00:05:14,400
different device 31 function 5 but i'll

148
00:05:12,639 --> 00:05:16,400
give you the exact commands so that

149
00:05:14,400 --> 00:05:17,840
depending on which chipset you have you

150
00:05:16,400 --> 00:05:19,600
should run a different set of commands

151
00:05:17,840 --> 00:05:22,720
and look at a different location in

152
00:05:19,600 --> 00:05:24,560
order to disable and toggle this bit 14

153
00:05:22,720 --> 00:05:26,400
in order to stop the decoding of that

154
00:05:24,560 --> 00:05:28,560
range so let's go ahead and start the

155
00:05:26,400 --> 00:05:28,560
lab

