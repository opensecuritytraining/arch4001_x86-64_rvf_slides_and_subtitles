1
00:00:00,240 --> 00:00:04,560
now there's another form of SMI

2
00:00:02,080 --> 00:00:08,080
suppression that involves some registers

3
00:00:04,560 --> 00:00:11,120
called TCO for total cost of ownership

4
00:00:08,080 --> 00:00:13,280
uh registers and so typically TCO stuff

5
00:00:11,120 --> 00:00:14,639
was used for a bunch of miscellaneous

6
00:00:13,280 --> 00:00:17,440
functionality

7
00:00:14,639 --> 00:00:20,160
around things like uh detection of

8
00:00:17,440 --> 00:00:22,640
intrusion into a chassis of a you know

9
00:00:20,160 --> 00:00:24,560
desktop system things like that cause a

10
00:00:22,640 --> 00:00:26,320
system management interrupt so that SMI

11
00:00:24,560 --> 00:00:29,119
code could say oh I see someone broke in

12
00:00:26,320 --> 00:00:32,079
you know or wipe the secrets whatever

13
00:00:29,119 --> 00:00:34,239
and so basically there's a way to set

14
00:00:32,079 --> 00:00:36,239
and enable here to zero and similar to

15
00:00:34,239 --> 00:00:38,480
setting this enable to zero that will

16
00:00:36,239 --> 00:00:41,040
cause suppression of

17
00:00:38,480 --> 00:00:43,040
TCO SMIs but it turns out that the

18
00:00:41,040 --> 00:00:45,039
hardware that's used for TCO stuff is

19
00:00:43,040 --> 00:00:48,480
also used for this

20
00:00:45,039 --> 00:00:50,160
BIOS lock enable so there is also a TCO

21
00:00:48,480 --> 00:00:52,160
lock that goes along with that similar

22
00:00:50,160 --> 00:00:53,600
to this SMI lock and that is how this

23
00:00:52,160 --> 00:00:55,440
can be prevented from being written to

24
00:00:53,600 --> 00:00:57,360
xero and then while we're here we'll

25
00:00:55,440 --> 00:01:00,800
just say that you could also defend this

26
00:00:57,360 --> 00:01:02,719
with intel bioscarred or smm bwp as with

27
00:01:00,800 --> 00:01:04,720
this other suppression attack

28
00:01:02,719 --> 00:01:07,280
so this was just mentioned offhandedly

29
00:01:04,720 --> 00:01:09,600
by some intel researchers when they were

30
00:01:07,280 --> 00:01:11,280
doing a survey paper at defcon to talk

31
00:01:09,600 --> 00:01:13,280
about the different types of attacks

32
00:01:11,280 --> 00:01:15,119
that were known at the time so they

33
00:01:13,280 --> 00:01:17,360
referenced this you know charizard

34
00:01:15,119 --> 00:01:19,600
attack from our work and then they said

35
00:01:17,360 --> 00:01:23,360
by the way there's another variant which

36
00:01:19,600 --> 00:01:26,080
is if you disable TCO SMI sources

37
00:01:23,360 --> 00:01:29,119
specifically by clearing SMI enable TCO

38
00:01:26,080 --> 00:01:31,280
enable then it won't actually lock the

39
00:01:29,119 --> 00:01:33,360
BIOS lock enables the BIOS lock enable

40
00:01:31,280 --> 00:01:35,600
will not effectively work so I was kind

41
00:01:33,360 --> 00:01:38,240
of curious about this before the class

42
00:01:35,600 --> 00:01:40,479
and so I pinged Yuriy Bulygin on twitter

43
00:01:38,240 --> 00:01:41,920
to ask him to really confirm that this

44
00:01:40,479 --> 00:01:44,079
was true and he said yes this is

45
00:01:41,920 --> 00:01:46,720
definitely true so I went off and tried

46
00:01:44,079 --> 00:01:48,079
it on one of these dell optiplex 7010s

47
00:01:46,720 --> 00:01:50,320
that we've been referencing through the

48
00:01:48,079 --> 00:01:52,560
class and it turns out it had exactly

49
00:01:50,320 --> 00:01:54,880
the configuration that I needed because

50
00:01:52,560 --> 00:01:57,040
it was defended against this because it

51
00:01:54,880 --> 00:01:58,960
had the SMI lock set so you couldn't use

52
00:01:57,040 --> 00:02:00,560
this attack but it was not defended

53
00:01:58,960 --> 00:02:02,479
against this because it didn't have the

54
00:02:00,560 --> 00:02:04,880
TCO lock set

55
00:02:02,479 --> 00:02:08,080
so these are the relevant registers so

56
00:02:04,880 --> 00:02:09,840
SMI enable as before TCO enable is just

57
00:02:08,080 --> 00:02:12,720
a thing that if set to 0 means there's

58
00:02:09,840 --> 00:02:14,879
no TCO SMIs and it just turns out that

59
00:02:12,720 --> 00:02:16,160
those you know despite not really saying

60
00:02:14,879 --> 00:02:18,640
it anywhere

61
00:02:16,160 --> 00:02:22,560
Yuriy basically confirmed that the little

62
00:02:18,640 --> 00:02:26,879
hardware block inside of the pch that

63
00:02:22,560 --> 00:02:28,720
is used for tcio TCO events also is the

64
00:02:26,879 --> 00:02:30,800
same hardware block that's used for SMIs

65
00:02:28,720 --> 00:02:32,720
related to BIOS write protection

66
00:02:30,800 --> 00:02:35,360
so TCO enable an attacker would want to

67
00:02:32,720 --> 00:02:37,840
set that to zero and then the SMIs would

68
00:02:35,360 --> 00:02:39,599
not fire and that can be prevented by

69
00:02:37,840 --> 00:02:43,800
setting TCO lock

70
00:02:39,599 --> 00:02:43,800
over in some other register

