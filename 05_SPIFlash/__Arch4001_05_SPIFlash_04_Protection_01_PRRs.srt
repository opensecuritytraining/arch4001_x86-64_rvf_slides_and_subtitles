1
00:00:00,320 --> 00:00:04,160
now at last this is where we really were

2
00:00:02,800 --> 00:00:06,879
trying to get throughout this whole

3
00:00:04,160 --> 00:00:08,880
class this is the meat of the flash

4
00:00:06,879 --> 00:00:11,040
right protection section

5
00:00:08,880 --> 00:00:13,920
so how to stop someone from infecting

6
00:00:11,040 --> 00:00:16,080
your bios it's simple just do all the

7
00:00:13,920 --> 00:00:18,000
things in green oh and actually it's not

8
00:00:16,080 --> 00:00:19,119
up to you to do it your bios vendor has

9
00:00:18,000 --> 00:00:21,039
to do it

10
00:00:19,119 --> 00:00:23,119
now we're going to start introducing

11
00:00:21,039 --> 00:00:24,800
threat trees into the class to help

12
00:00:23,119 --> 00:00:26,960
organize our thinking about what the

13
00:00:24,800 --> 00:00:28,960
moves and counter moves are that's why i

14
00:00:26,960 --> 00:00:31,039
like to refer to threat trees as the

15
00:00:28,960 --> 00:00:33,200
tree of the knowledge of good and evil

16
00:00:31,039 --> 00:00:35,520
because the root node is what the

17
00:00:33,200 --> 00:00:37,680
attacker wants to do the green nodes are

18
00:00:35,520 --> 00:00:39,520
what a defender can do to counter that

19
00:00:37,680 --> 00:00:41,360
and the red nodes are what the attacker

20
00:00:39,520 --> 00:00:42,559
does to counter their countermeasure the

21
00:00:41,360 --> 00:00:43,920
green nodes are then the counter

22
00:00:42,559 --> 00:00:46,480
measures for the countermeasures for the

23
00:00:43,920 --> 00:00:48,960
countermeasures and so on

24
00:00:46,480 --> 00:00:50,559
good bad good bad the tree of the

25
00:00:48,960 --> 00:00:54,000
knowledge of good and evil

26
00:00:50,559 --> 00:00:56,160
so it's green versus red who is stronger

27
00:00:54,000 --> 00:00:59,600
well i know who i think stronger

28
00:00:56,160 --> 00:01:02,000
greenhouse stop red hulk now

29
00:00:59,600 --> 00:01:04,000
but let's see exactly how there's two

30
00:01:02,000 --> 00:01:06,080
main flash protection mechanisms which

31
00:01:04,000 --> 00:01:07,360
intel outlines in this convenient little

32
00:01:06,080 --> 00:01:09,280
table here

33
00:01:07,360 --> 00:01:11,200
first we're going to talk about the bios

34
00:01:09,280 --> 00:01:14,320
range right protection which blocks

35
00:01:11,200 --> 00:01:16,000
rights and is range specific and has a

36
00:01:14,320 --> 00:01:18,159
reset override meaning that when you

37
00:01:16,000 --> 00:01:20,320
restart the system the protection will

38
00:01:18,159 --> 00:01:22,880
go away so let's see how a bios maker

39
00:01:20,320 --> 00:01:26,159
can protect the bios intel outlines two

40
00:01:22,880 --> 00:01:28,240
main forms a bios range right protection

41
00:01:26,159 --> 00:01:29,600
and a general right protection one of

42
00:01:28,240 --> 00:01:30,960
which is range specific and the other

43
00:01:29,600 --> 00:01:34,079
which is not

44
00:01:30,960 --> 00:01:36,640
so this particular one is exhibited via

45
00:01:34,079 --> 00:01:38,640
protected range registers now i consider

46
00:01:36,640 --> 00:01:40,400
these the most important defensive

47
00:01:38,640 --> 00:01:41,920
mechanism because i think it's one of

48
00:01:40,400 --> 00:01:44,240
the strongest for reasons that you'll

49
00:01:41,920 --> 00:01:46,720
see as we explore the threat tree

50
00:01:44,240 --> 00:01:48,960
there are five protected range registers

51
00:01:46,720 --> 00:01:51,600
which can be set by the bios maker to

52
00:01:48,960 --> 00:01:53,759
protect ranges within the flash from

53
00:01:51,600 --> 00:01:56,079
reads or rights now the protected range

54
00:01:53,759 --> 00:01:59,119
registers are enforced when someone is

55
00:01:56,079 --> 00:02:01,360
trying to use those spy flash memory

56
00:01:59,119 --> 00:02:03,920
mapped i o registers to read or write to

57
00:02:01,360 --> 00:02:05,680
the bios as we saw before it doesn't

58
00:02:03,920 --> 00:02:07,280
apply to the direct memory access but

59
00:02:05,680 --> 00:02:10,720
that's fine because there's no write

60
00:02:07,280 --> 00:02:12,640
capability via the memory access anyways

61
00:02:10,720 --> 00:02:14,319
these protected range registers apply

62
00:02:12,640 --> 00:02:15,680
regardless of whether the system is

63
00:02:14,319 --> 00:02:18,000
running in descriptor mode or

64
00:02:15,680 --> 00:02:19,760
non-descriptor mode and also once

65
00:02:18,000 --> 00:02:21,760
they're set and locked even the

66
00:02:19,760 --> 00:02:24,080
ostensibly most privileged code on the

67
00:02:21,760 --> 00:02:26,400
system system management mode is not

68
00:02:24,080 --> 00:02:27,920
able to actually bypass them so that's

69
00:02:26,400 --> 00:02:29,920
why i say that they're the most

70
00:02:27,920 --> 00:02:31,840
important mechanism

71
00:02:29,920 --> 00:02:33,599
now if we go look in the data sheets for

72
00:02:31,840 --> 00:02:36,640
the protected range registers we'll find

73
00:02:33,599 --> 00:02:39,120
things like this pr0 this one is at spy

74
00:02:36,640 --> 00:02:41,120
bar plus 74 and this is the zeroth one

75
00:02:39,120 --> 00:02:43,280
and we said that there's five total so

76
00:02:41,120 --> 00:02:45,760
spy bar plus 74 back on some older

77
00:02:43,280 --> 00:02:47,760
things like the 5 series chipset

78
00:02:45,760 --> 00:02:52,319
and when we look at it we see that it

79
00:02:47,760 --> 00:02:54,239
has 13 bits 0 through 12 so 13 bits for

80
00:02:52,319 --> 00:02:57,760
a protected range base

81
00:02:54,239 --> 00:03:00,319
1 bit for read protection 13 bits for

82
00:02:57,760 --> 00:03:02,239
protected range limit and 1 bit for

83
00:03:00,319 --> 00:03:04,000
write protection so the write and the

84
00:03:02,239 --> 00:03:06,480
read are pretty self self-explanatory if

85
00:03:04,000 --> 00:03:08,720
this bit is set then the range applies

86
00:03:06,480 --> 00:03:10,400
to protection against writes and if this

87
00:03:08,720 --> 00:03:12,159
bit is set to then the range applies to

88
00:03:10,400 --> 00:03:14,319
protection against reads

89
00:03:12,159 --> 00:03:16,800
but what exactly how do we specify this

90
00:03:14,319 --> 00:03:19,040
protected range base and limit well

91
00:03:16,800 --> 00:03:21,760
these 13 bits are used as the most

92
00:03:19,040 --> 00:03:22,720
significant 13 bits of a flash linear

93
00:03:21,760 --> 00:03:25,519
address

94
00:03:22,720 --> 00:03:27,200
where the protection should start so

95
00:03:25,519 --> 00:03:29,519
again we talked about before you know

96
00:03:27,200 --> 00:03:31,360
nominally flash is using 24 bit

97
00:03:29,519 --> 00:03:32,720
addressing and so it should only have 16

98
00:03:31,360 --> 00:03:34,080
megabyte things

99
00:03:32,720 --> 00:03:36,560
but you could have two things

100
00:03:34,080 --> 00:03:38,239
concatenated so that would add up to be

101
00:03:36,560 --> 00:03:40,560
25 bits

102
00:03:38,239 --> 00:03:42,720
so these upper 13 bits are saying

103
00:03:40,560 --> 00:03:45,280
someplace in the flash linear address

104
00:03:42,720 --> 00:03:47,599
range right here let's start to the

105
00:03:45,280 --> 00:03:49,120
right protection the rest of the bits

106
00:03:47,599 --> 00:03:51,200
the least significant 12 bits are

107
00:03:49,120 --> 00:03:54,560
assumed to be zero so you have to start

108
00:03:51,200 --> 00:03:56,319
the protection on some you know x 1000

109
00:03:54,560 --> 00:03:59,200
aligned address

110
00:03:56,319 --> 00:04:01,439
for the protected range limit this is

111
00:03:59,200 --> 00:04:04,080
going to be the upper 13 bits again of a

112
00:04:01,439 --> 00:04:05,920
25 bit address and the least significant

113
00:04:04,080 --> 00:04:07,840
12 bits are assumed to be one so

114
00:04:05,920 --> 00:04:10,640
basically you take that and you say this

115
00:04:07,840 --> 00:04:12,560
is the upper range and then set all ones

116
00:04:10,640 --> 00:04:14,799
so let's see that right here

117
00:04:12,560 --> 00:04:16,799
so if we had for instance read out of a

118
00:04:14,799 --> 00:04:20,320
protected range register the literal

119
00:04:16,799 --> 00:04:21,919
value 0 3 ff02a2

120
00:04:20,320 --> 00:04:23,759
we would interpret that like this

121
00:04:21,919 --> 00:04:26,320
according to that register and we would

122
00:04:23,759 --> 00:04:28,479
take the least significant 13 bits and

123
00:04:26,320 --> 00:04:30,400
those would be essentially shifted by 12

124
00:04:28,479 --> 00:04:34,479
to the left and that would give us

125
00:04:30,400 --> 00:04:36,240
something like 0 2 a 2 0 0 0. on the

126
00:04:34,479 --> 00:04:38,720
other hand if we wanted to understand

127
00:04:36,240 --> 00:04:41,199
the limit of it we would take these 13

128
00:04:38,720 --> 00:04:43,199
bits and we would tack on 12 bits at the

129
00:04:41,199 --> 00:04:46,080
least significant bits

130
00:04:43,199 --> 00:04:47,919
and those bits should be ones and so

131
00:04:46,080 --> 00:04:50,720
this would give us you know basically

132
00:04:47,919 --> 00:04:55,680
taking these 12 these 13 bits right here

133
00:04:50,720 --> 00:04:57,199
zero three ff and so zero three ff ff so

134
00:04:55,680 --> 00:04:59,199
least significant bits should be treated

135
00:04:57,199 --> 00:05:01,680
as all ones

136
00:04:59,199 --> 00:05:04,000
now flipping the page to the 100 series

137
00:05:01,680 --> 00:05:06,479
chipset datasheets it looks a little bit

138
00:05:04,000 --> 00:05:09,840
different we don't have something in the

139
00:05:06,479 --> 00:05:12,560
spy bar memory mapped i o range instead

140
00:05:09,840 --> 00:05:15,840
the fl the protected range registers are

141
00:05:12,560 --> 00:05:16,960
found in device 31 function five so that

142
00:05:15,840 --> 00:05:20,160
was the

143
00:05:16,960 --> 00:05:23,680
spy device that we saw had a pointer in

144
00:05:20,160 --> 00:05:25,919
its bar zero to a you know spy bar area

145
00:05:23,680 --> 00:05:28,880
where the where the flash read and write

146
00:05:25,919 --> 00:05:31,759
registers were so in that same range

147
00:05:28,880 --> 00:05:33,680
just at the device 31 function

148
00:05:31,759 --> 00:05:36,000
5 and offset

149
00:05:33,680 --> 00:05:37,440
84 then you start seeing the protected

150
00:05:36,000 --> 00:05:38,880
range registers and again there's

151
00:05:37,440 --> 00:05:40,479
multiple of them

152
00:05:38,880 --> 00:05:43,039
everything looks basically the same in

153
00:05:40,479 --> 00:05:45,199
terms of there being a base a limit a

154
00:05:43,039 --> 00:05:46,880
write protect and a read protect but as

155
00:05:45,199 --> 00:05:48,320
we sort of saw with the flash linear

156
00:05:46,880 --> 00:05:50,320
addresses before when we were talking

157
00:05:48,320 --> 00:05:52,400
about flash read and writes these newer

158
00:05:50,320 --> 00:05:56,000
systems do actually support a larger

159
00:05:52,400 --> 00:05:56,000
flash linear address range

