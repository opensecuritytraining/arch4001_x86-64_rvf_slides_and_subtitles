1
00:00:00,399 --> 00:00:04,080
so this section contains the first

2
00:00:02,080 --> 00:00:06,080
little hints of flash write protection

3
00:00:04,080 --> 00:00:08,240
in some of the optional material i

4
00:00:06,080 --> 00:00:10,400
didn't make it mandatory because this

5
00:00:08,240 --> 00:00:12,320
information is usually set by tools

6
00:00:10,400 --> 00:00:14,480
provided by intel so it's rarely

7
00:00:12,320 --> 00:00:16,160
misconfigured but for those of you who

8
00:00:14,480 --> 00:00:17,920
want to know all the possible ways that

9
00:00:16,160 --> 00:00:19,439
something could be misconfigured you

10
00:00:17,920 --> 00:00:20,800
should watch the optional material for

11
00:00:19,439 --> 00:00:23,119
this section

12
00:00:20,800 --> 00:00:25,039
but this first bit is required and so

13
00:00:23,119 --> 00:00:26,640
what i want to talk about is the notion

14
00:00:25,039 --> 00:00:28,720
of when you're first trying to

15
00:00:26,640 --> 00:00:31,039
understand a system you should generally

16
00:00:28,720 --> 00:00:32,640
understand how the flash is laid out

17
00:00:31,039 --> 00:00:35,040
certainly this is the first thing that i

18
00:00:32,640 --> 00:00:37,280
do when i do an assessment of a rom or a

19
00:00:35,040 --> 00:00:39,200
firmware if it's a white box evaluation

20
00:00:37,280 --> 00:00:41,680
i ask the vendor to give me a full

21
00:00:39,200 --> 00:00:43,760
detailed map of all the different areas

22
00:00:41,680 --> 00:00:45,440
on the spy flash chip how they're laid

23
00:00:43,760 --> 00:00:47,520
out what's attacker controlled what's

24
00:00:45,440 --> 00:00:49,120
not generally this has to do with

25
00:00:47,520 --> 00:00:51,440
whether or not portions are covered by

26
00:00:49,120 --> 00:00:53,680
digital signatures so generally i want

27
00:00:51,440 --> 00:00:55,680
to understand how the flash layout works

28
00:00:53,680 --> 00:00:58,320
if it's a black box evaluation then this

29
00:00:55,680 --> 00:01:00,640
is always productive to get a sense of

30
00:00:58,320 --> 00:01:02,559
how the data is structured on the flash

31
00:01:00,640 --> 00:01:05,360
now i'm just going to very briefly go

32
00:01:02,559 --> 00:01:08,080
over how the spy flash on x86 systems is

33
00:01:05,360 --> 00:01:11,119
reused between the cpu for the bios the

34
00:01:08,080 --> 00:01:13,600
management engine gigabit ethernet etc

35
00:01:11,119 --> 00:01:16,240
right so zooming in on the spy flash

36
00:01:13,600 --> 00:01:18,479
chip at the very beginning of the chip

37
00:01:16,240 --> 00:01:21,040
is that flash descriptor that is what

38
00:01:18,479 --> 00:01:22,880
gives descriptor mode its name

39
00:01:21,040 --> 00:01:24,560
and the flash descriptor has a data

40
00:01:22,880 --> 00:01:27,280
structure unto itself it's four

41
00:01:24,560 --> 00:01:29,520
kilobytes it looks like this and we'll

42
00:01:27,280 --> 00:01:32,400
cover this in much much more detail in

43
00:01:29,520 --> 00:01:34,960
the optional material after this

44
00:01:32,400 --> 00:01:36,799
so the flash descriptor is all good but

45
00:01:34,960 --> 00:01:39,439
what's interesting about it is that it

46
00:01:36,799 --> 00:01:41,920
lays out the definition of how the other

47
00:01:39,439 --> 00:01:43,520
information is stored on the flash chip

48
00:01:41,920 --> 00:01:45,439
so it tells you about what sort of

49
00:01:43,520 --> 00:01:47,680
regions there are for plate things like

50
00:01:45,439 --> 00:01:49,840
the bios region management engine intel

51
00:01:47,680 --> 00:01:51,840
gigabit ethernet and so forth and in

52
00:01:49,840 --> 00:01:53,920
particular intel adds things over time

53
00:01:51,840 --> 00:01:55,520
so there didn't used to be a platform

54
00:01:53,920 --> 00:01:57,520
data region and there didn't used to be

55
00:01:55,520 --> 00:01:59,439
an embedded controller region but these

56
00:01:57,520 --> 00:02:01,280
were added and specified in newer

57
00:01:59,439 --> 00:02:02,719
hardware and there's still room to add

58
00:02:01,280 --> 00:02:04,640
even more

59
00:02:02,719 --> 00:02:06,320
these different regions can all be moved

60
00:02:04,640 --> 00:02:09,039
around except for the fact that the

61
00:02:06,320 --> 00:02:10,560
flash descriptor needs to be at region 0

62
00:02:09,039 --> 00:02:11,840
at the beginning of the chip because

63
00:02:10,560 --> 00:02:13,840
that's where the hardware expects to

64
00:02:11,840 --> 00:02:15,440
read it from and the bios region needs

65
00:02:13,840 --> 00:02:17,360
to be at the end of the flash chip

66
00:02:15,440 --> 00:02:19,680
because that's what's going to be mapped

67
00:02:17,360 --> 00:02:22,239
up to the high memory range of 4

68
00:02:19,680 --> 00:02:24,480
gigabytes and the reset vector at the

69
00:02:22,239 --> 00:02:26,160
end here minus hex 10 is going to be

70
00:02:24,480 --> 00:02:27,680
mapped to the reset vector in memory at

71
00:02:26,160 --> 00:02:29,280
ffff0

72
00:02:27,680 --> 00:02:31,440
but then the question is what's in the

73
00:02:29,280 --> 00:02:34,000
different regions well in the case of

74
00:02:31,440 --> 00:02:36,319
intel systems there's a data structure

75
00:02:34,000 --> 00:02:38,480
called the firmware interface table a

76
00:02:36,319 --> 00:02:41,040
pointer to that data structure exists at

77
00:02:38,480 --> 00:02:43,120
four gigabyte minus hex 40 and then it

78
00:02:41,040 --> 00:02:44,800
has something like this and that has

79
00:02:43,120 --> 00:02:46,560
further pointers into further

80
00:02:44,800 --> 00:02:48,160
information like i said one of the

81
00:02:46,560 --> 00:02:50,319
interesting questions always about

82
00:02:48,160 --> 00:02:52,319
firmware layout is what's covered by

83
00:02:50,319 --> 00:02:54,160
digital signatures what's not how does

84
00:02:52,319 --> 00:02:55,840
you know the secure boot chain things

85
00:02:54,160 --> 00:02:58,000
work things like that

86
00:02:55,840 --> 00:03:00,319
and you know only this authenticated

87
00:02:58,000 --> 00:03:01,840
code module uh that's right here that's

88
00:03:00,319 --> 00:03:03,680
the thing that's covered by digital

89
00:03:01,840 --> 00:03:06,159
signature and that's what kicks off the

90
00:03:03,680 --> 00:03:07,920
secure boot chain on a maximally trusted

91
00:03:06,159 --> 00:03:09,680
intel boot card boot

92
00:03:07,920 --> 00:03:10,640
so that could exist but it might not

93
00:03:09,680 --> 00:03:12,640
exist

94
00:03:10,640 --> 00:03:14,720
generally speaking the bios region on

95
00:03:12,640 --> 00:03:17,120
modern uefi systems is going to have

96
00:03:14,720 --> 00:03:19,599
some sort of file system and

97
00:03:17,120 --> 00:03:22,159
specifically uefi lays out a particular

98
00:03:19,599 --> 00:03:24,400
data structure of firmware volumes

99
00:03:22,159 --> 00:03:26,720
firmware files and things like that

100
00:03:24,400 --> 00:03:28,560
alternatively if this was a non-uefi

101
00:03:26,720 --> 00:03:30,000
system for instance a core boot system

102
00:03:28,560 --> 00:03:32,560
like you learn about in architecture

103
00:03:30,000 --> 00:03:34,799
4031 it could have a completely

104
00:03:32,560 --> 00:03:36,400
different file system organization it's

105
00:03:34,799 --> 00:03:38,319
really up to the firmware maker how

106
00:03:36,400 --> 00:03:40,159
exactly they want to lay things out

107
00:03:38,319 --> 00:03:42,480
modulo the fact that they always have to

108
00:03:40,159 --> 00:03:44,480
have the reset vector at the end of the

109
00:03:42,480 --> 00:03:46,879
flash minus x10

110
00:03:44,480 --> 00:03:48,720
so given this picture from the core vote

111
00:03:46,879 --> 00:03:51,120
documentation this is the start of the

112
00:03:48,720 --> 00:03:52,640
rom and that's the reset vector then we

113
00:03:51,120 --> 00:03:54,879
actually need to flip this upside down

114
00:03:52,640 --> 00:03:57,840
like this then we might ask ourselves

115
00:03:54,879 --> 00:04:00,239
what's inside the csme region and the

116
00:03:57,840 --> 00:04:02,080
positive technologies folks did a talk

117
00:04:00,239 --> 00:04:04,000
just on reverse engineering the data

118
00:04:02,080 --> 00:04:06,000
structure format of the management

119
00:04:04,000 --> 00:04:07,760
engine file system because they have

120
00:04:06,000 --> 00:04:08,799
their own firmware file system onto

121
00:04:07,760 --> 00:04:11,360
themselves

122
00:04:08,799 --> 00:04:12,959
a proprietary and undocumented thing

123
00:04:11,360 --> 00:04:14,879
and then also there's the intel gigabit

124
00:04:12,959 --> 00:04:17,199
ethernet region and you can find some

125
00:04:14,879 --> 00:04:19,759
documentation of what exactly is stored

126
00:04:17,199 --> 00:04:22,000
here at the cited documentation below

127
00:04:19,759 --> 00:04:24,160
and also at the data sheets for intel

128
00:04:22,000 --> 00:04:25,440
gigabit ethernet controllers

129
00:04:24,160 --> 00:04:27,120
i've never really looked at this

130
00:04:25,440 --> 00:04:28,720
particularly deeply that's an

131
00:04:27,120 --> 00:04:31,440
interesting research opportunity if

132
00:04:28,720 --> 00:04:33,360
anyone wants to look into it but to a

133
00:04:31,440 --> 00:04:35,360
quick skim it doesn't look like it's

134
00:04:33,360 --> 00:04:37,199
containing you know primary and core

135
00:04:35,360 --> 00:04:38,960
firmware for the ethernet itself it

136
00:04:37,199 --> 00:04:40,960
seems to mostly contain a bunch of

137
00:04:38,960 --> 00:04:43,120
configuration type information for

138
00:04:40,960 --> 00:04:45,759
instance you can see that the first six

139
00:04:43,120 --> 00:04:47,120
bytes are bytes for the mac address of

140
00:04:45,759 --> 00:04:48,800
the ethernet

141
00:04:47,120 --> 00:04:50,720
and then after that you see things that

142
00:04:48,800 --> 00:04:53,040
you would expect from pci devices like

143
00:04:50,720 --> 00:04:54,479
vendor id device id and so forth so

144
00:04:53,040 --> 00:04:55,919
that's not to say there's not other

145
00:04:54,479 --> 00:04:57,600
interesting things here i just haven't

146
00:04:55,919 --> 00:04:58,880
looked into it

147
00:04:57,600 --> 00:05:00,560
and then there's things like the

148
00:04:58,880 --> 00:05:02,080
platform data region and the embedded

149
00:05:00,560 --> 00:05:04,400
controller region what do those look

150
00:05:02,080 --> 00:05:06,560
like well as of right now there's no

151
00:05:04,400 --> 00:05:08,000
public documentation now i'm sure the

152
00:05:06,560 --> 00:05:10,080
platform data region probably has

153
00:05:08,000 --> 00:05:11,680
proprietary intel documentation but i

154
00:05:10,080 --> 00:05:13,600
wasn't looking at any of that for the

155
00:05:11,680 --> 00:05:15,199
purposes of this class and it's been a

156
00:05:13,600 --> 00:05:17,120
number of years since i've bothered to

157
00:05:15,199 --> 00:05:19,759
look at any of the intel ndaid

158
00:05:17,120 --> 00:05:21,199
documentation so this is actually a

159
00:05:19,759 --> 00:05:22,880
research opportunity in the same way

160
00:05:21,199 --> 00:05:24,720
that just reverse engineering the intel

161
00:05:22,880 --> 00:05:27,039
management engine was a useful

162
00:05:24,720 --> 00:05:28,880
presentation someone could go out and

163
00:05:27,039 --> 00:05:30,639
dig into how these things work and fully

164
00:05:28,880 --> 00:05:33,280
independently construct them without

165
00:05:30,639 --> 00:05:36,720
need for intel proprietary documentation

166
00:05:33,280 --> 00:05:36,720
and that would help other researchers

