1
00:00:00,160 --> 00:00:05,040
okay so up in the flash map it told us

2
00:00:02,639 --> 00:00:08,800
that at hex 30 is going to be the next

3
00:00:05,040 --> 00:00:10,880
thing and that was the component section

4
00:00:08,800 --> 00:00:13,360
so you would then proceed into your

5
00:00:10,880 --> 00:00:14,880
manual you would find the fcba well

6
00:00:13,360 --> 00:00:17,920
that's just basically saying the you

7
00:00:14,880 --> 00:00:20,160
know component base address plus zero so

8
00:00:17,920 --> 00:00:22,800
fl component how do you interpret it

9
00:00:20,160 --> 00:00:25,039
well there's this old thing so let's

10
00:00:22,800 --> 00:00:27,519
take a look there so the value at the

11
00:00:25,039 --> 00:00:29,439
least significant byte is one c so that

12
00:00:27,519 --> 00:00:31,679
is one and c

13
00:00:29,439 --> 00:00:34,800
and then the least significant three

14
00:00:31,679 --> 00:00:37,040
bits is going to be one zero zero and so

15
00:00:34,800 --> 00:00:39,680
one zero zero is telling you component

16
00:00:37,040 --> 00:00:42,559
one is eight megabytes large so one

17
00:00:39,680 --> 00:00:45,280
flash chip out of the two flash chips is

18
00:00:42,559 --> 00:00:46,719
eight megabytes large then the next bits

19
00:00:45,280 --> 00:00:49,200
are telling you that the second

20
00:00:46,719 --> 00:00:50,640
component is four megabytes large and

21
00:00:49,200 --> 00:00:52,640
that's exactly what you would expect

22
00:00:50,640 --> 00:00:54,480
because you are currently holding a 12

23
00:00:52,640 --> 00:00:56,480
megabyte flash chip and you know it has

24
00:00:54,480 --> 00:00:58,559
two components

25
00:00:56,480 --> 00:01:00,719
next in the component section at flash

26
00:00:58,559 --> 00:01:02,960
component-based address plus four is the

27
00:01:00,719 --> 00:01:05,360
invalid instructions register you can

28
00:01:02,960 --> 00:01:07,200
see this is set to all zeros so there's

29
00:01:05,360 --> 00:01:08,960
no invalid instructions that

30
00:01:07,200 --> 00:01:10,960
specifically it wants to prevent from

31
00:01:08,960 --> 00:01:13,360
being sent there so that's all good and

32
00:01:10,960 --> 00:01:15,200
well one thing you might imagine this

33
00:01:13,360 --> 00:01:16,880
being used for is for instance you know

34
00:01:15,200 --> 00:01:18,880
there's potentially dangerous things

35
00:01:16,880 --> 00:01:21,840
that might accidentally get sent like a

36
00:01:18,880 --> 00:01:23,600
full chip erase flash up code and so the

37
00:01:21,840 --> 00:01:25,439
point of that particular section was to

38
00:01:23,600 --> 00:01:27,520
say like if someone comes along and uses

39
00:01:25,439 --> 00:01:29,119
software sequencing to try to you know

40
00:01:27,520 --> 00:01:31,119
pass through a full chip arrays i want

41
00:01:29,119 --> 00:01:32,720
you to actually block that in hardware

42
00:01:31,119 --> 00:01:35,280
now you can't block that one because

43
00:01:32,720 --> 00:01:37,840
this is actually the required opcodes

44
00:01:35,280 --> 00:01:39,520
section from the intel datasheets but if

45
00:01:37,840 --> 00:01:42,240
you look at some datasheet for a

46
00:01:39,520 --> 00:01:44,320
specific spy flash chip you can see that

47
00:01:42,240 --> 00:01:46,560
they might support multiple erase

48
00:01:44,320 --> 00:01:49,439
commands so you may be required to

49
00:01:46,560 --> 00:01:51,840
support c7 in order to use a particular

50
00:01:49,439 --> 00:01:54,399
chip on intel but it might also support

51
00:01:51,840 --> 00:01:56,079
hexa60 and so then you might want to say

52
00:01:54,399 --> 00:01:57,920
well i only want to let c7 through and i

53
00:01:56,079 --> 00:01:59,920
want to block 60 because it might have

54
00:01:57,920 --> 00:02:01,520
completely different behavior so anyways

55
00:01:59,920 --> 00:02:03,520
that's just an example it's not actually

56
00:02:01,520 --> 00:02:05,360
used on this particular system

57
00:02:03,520 --> 00:02:07,920
next there's the flash partition

58
00:02:05,360 --> 00:02:09,200
boundary register and you can see this

59
00:02:07,920 --> 00:02:12,160
has what looks like a little

60
00:02:09,200 --> 00:02:13,599
endian008000

61
00:02:12,160 --> 00:02:15,520
then there is the flash partition

62
00:02:13,599 --> 00:02:16,800
boundary register we showed a picture

63
00:02:15,520 --> 00:02:19,200
before saying you know you could

64
00:02:16,800 --> 00:02:20,640
partition a single flash chip more often

65
00:02:19,200 --> 00:02:22,400
than not it's going to be used for

66
00:02:20,640 --> 00:02:24,319
partitioning between two flash ships and

67
00:02:22,400 --> 00:02:26,400
we do indeed have two flash chips the

68
00:02:24,319 --> 00:02:28,800
first first flash chip is eight

69
00:02:26,400 --> 00:02:32,800
megabytes and this particular value is

70
00:02:28,800 --> 00:02:34,160
interpreted as bits 24 through 12. so

71
00:02:32,800 --> 00:02:36,160
basically this is just saying the

72
00:02:34,160 --> 00:02:40,640
boundary is an eight megabyte boundary

73
00:02:36,160 --> 00:02:40,640
for flash partition zero versus one

