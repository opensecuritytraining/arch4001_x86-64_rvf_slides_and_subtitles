1
00:00:00,240 --> 00:00:04,720
all right so that was fun right everyone

2
00:00:02,240 --> 00:00:06,960
enjoys parsing through things by hand as

3
00:00:04,720 --> 00:00:08,400
much as i do and so if you had any

4
00:00:06,960 --> 00:00:10,080
questions about how that works if you

5
00:00:08,400 --> 00:00:11,759
weren't able to get some of the answers

6
00:00:10,080 --> 00:00:13,200
then this section is for you if you got

7
00:00:11,759 --> 00:00:15,679
everything then you don't really need to

8
00:00:13,200 --> 00:00:18,000
watch this so i want you to keep it

9
00:00:15,679 --> 00:00:21,199
gritty because gritty is the patron

10
00:00:18,000 --> 00:00:23,840
saint of nitty gritty analysis gritty is

11
00:00:21,199 --> 00:00:26,800
a sports mascot for a hockey team who

12
00:00:23,840 --> 00:00:29,359
will haunt your dreams

13
00:00:26,800 --> 00:00:31,760
so anyways the particular thing that i

14
00:00:29,359 --> 00:00:34,880
used for this analysis was uefi tool

15
00:00:31,760 --> 00:00:36,399
specifically its hex view image and i'm

16
00:00:34,880 --> 00:00:38,800
going to use this here because i want

17
00:00:36,399 --> 00:00:40,399
you to understand that uefi tool is

18
00:00:38,800 --> 00:00:41,840
basically just parsing the flash

19
00:00:40,399 --> 00:00:43,760
descriptor in order to get some

20
00:00:41,840 --> 00:00:45,600
information that it displays over here

21
00:00:43,760 --> 00:00:48,000
and so we're going to see that as we go

22
00:00:45,600 --> 00:00:50,640
along so if you click on the intel image

23
00:00:48,000 --> 00:00:53,120
portion in uefi tool you will see this

24
00:00:50,640 --> 00:00:55,520
information and if you instead click on

25
00:00:53,120 --> 00:00:57,360
the descriptor region portion you will

26
00:00:55,520 --> 00:00:59,600
see all of this information so where

27
00:00:57,360 --> 00:01:02,160
does it get it from let's find out so

28
00:00:59,600 --> 00:01:05,439
offset hex 10 i said you should treat

29
00:01:02,160 --> 00:01:07,920
fdbar in the ich datasheet as if it is

30
00:01:05,439 --> 00:01:12,000
hex10 because it's not really an ich10

31
00:01:07,920 --> 00:01:14,159
it's a pch7 series so fdbar plus zero so

32
00:01:12,000 --> 00:01:16,400
10 plus zero here's the magic signature

33
00:01:14,159 --> 00:01:18,320
it should be exactly that that is what

34
00:01:16,400 --> 00:01:20,640
we expect

35
00:01:18,320 --> 00:01:22,400
and there we go that's it at the base of

36
00:01:20,640 --> 00:01:24,240
this thing i'll be popping this in just

37
00:01:22,400 --> 00:01:27,040
to remind you where we are as we get

38
00:01:24,240 --> 00:01:29,280
lost in the hex

39
00:01:27,040 --> 00:01:33,280
then after that the next thing is the

40
00:01:29,280 --> 00:01:36,880
flash map so flash map zero fd bar plus

41
00:01:33,280 --> 00:01:39,680
four has these descriptions so how do we

42
00:01:36,880 --> 00:01:42,560
parse that well the zero with byte is

43
00:01:39,680 --> 00:01:44,479
this flash component base address and

44
00:01:42,560 --> 00:01:47,119
this is actually telling us where we can

45
00:01:44,479 --> 00:01:49,040
find the components section so

46
00:01:47,119 --> 00:01:53,040
specifically it says this identifies

47
00:01:49,040 --> 00:01:55,920
bits 11 through 4 and that the bits 3

48
00:01:53,040 --> 00:01:58,000
through zero are zero so the value is

49
00:01:55,920 --> 00:01:59,680
three but that's treated like bits

50
00:01:58,000 --> 00:02:01,040
eleven through four and at the least

51
00:01:59,680 --> 00:02:02,880
significant four bits are zero so

52
00:02:01,040 --> 00:02:05,600
essentially it's just shifted left by

53
00:02:02,880 --> 00:02:07,759
four bits which means it's saying thirty

54
00:02:05,600 --> 00:02:09,440
thirty is the base address of the next

55
00:02:07,759 --> 00:02:11,840
section of the flash descriptor the

56
00:02:09,440 --> 00:02:13,840
component section all right basically so

57
00:02:11,840 --> 00:02:16,319
if you had flash map zero field in the

58
00:02:13,840 --> 00:02:18,640
descriptor map it is pointing to the

59
00:02:16,319 --> 00:02:20,640
component section so remember the flash

60
00:02:18,640 --> 00:02:23,280
descriptor map points to each of these

61
00:02:20,640 --> 00:02:25,200
sections and the region section points

62
00:02:23,280 --> 00:02:27,280
to all of the stuff outside of this the

63
00:02:25,200 --> 00:02:29,040
different regions so there we go found

64
00:02:27,280 --> 00:02:30,720
the components section

65
00:02:29,040 --> 00:02:32,480
and then this tells us how many

66
00:02:30,720 --> 00:02:33,920
components there are this tells us the

67
00:02:32,480 --> 00:02:35,840
number of flash chips but

68
00:02:33,920 --> 00:02:37,200
counter-intuitively you see one and

69
00:02:35,840 --> 00:02:40,160
you'd think it would mean one flash

70
00:02:37,200 --> 00:02:42,400
chips but no one means two so one means

71
00:02:40,160 --> 00:02:44,080
two components two flash chips and

72
00:02:42,400 --> 00:02:45,760
that's what it's telling you up here

73
00:02:44,080 --> 00:02:47,519
it's telling you there's two flash chips

74
00:02:45,760 --> 00:02:49,599
because it read this section and it

75
00:02:47,519 --> 00:02:52,800
interpreted it accordingly

76
00:02:49,599 --> 00:02:55,440
next byte 4 this is the flash region

77
00:02:52,800 --> 00:02:57,519
base address well the region is again

78
00:02:55,440 --> 00:02:59,599
one of those next chunks of the flash

79
00:02:57,519 --> 00:03:01,840
descriptor and this once again does the

80
00:02:59,599 --> 00:03:04,159
i'm bits 11 through 4 and the least

81
00:03:01,840 --> 00:03:06,640
significant bits are zero so again shift

82
00:03:04,159 --> 00:03:08,959
it left by four and that tells you that

83
00:03:06,640 --> 00:03:11,440
40 is where you will see the region

84
00:03:08,959 --> 00:03:14,560
portion of the flash descriptor starting

85
00:03:11,440 --> 00:03:16,080
so like so that is the pointer to the

86
00:03:14,560 --> 00:03:18,159
region section

87
00:03:16,080 --> 00:03:20,879
all right and then the next byte is

88
00:03:18,159 --> 00:03:23,200
three and this is the number of regions

89
00:03:20,879 --> 00:03:26,560
but this is the zero indexed number of

90
00:03:23,200 --> 00:03:29,040
regions so three actually means four

91
00:03:26,560 --> 00:03:31,120
there are regions zero one two and three

92
00:03:29,040 --> 00:03:33,840
then you would move on to the next flash

93
00:03:31,120 --> 00:03:36,080
map one which is fdbar plus eight and it

94
00:03:33,840 --> 00:03:37,519
has very similar interpretation

95
00:03:36,080 --> 00:03:40,799
boom this tells you where to find the

96
00:03:37,519 --> 00:03:43,680
master portion it's again offset to the

97
00:03:40,799 --> 00:03:44,959
shift left by four and so that's 60.

98
00:03:43,680 --> 00:03:47,120
that's where the master portion of the

99
00:03:44,959 --> 00:03:48,319
flash descriptor is boom pointer like

100
00:03:47,120 --> 00:03:50,640
that

101
00:03:48,319 --> 00:03:52,560
what's the next byte number of masters

102
00:03:50,640 --> 00:03:55,040
this is a zeros based number so two

103
00:03:52,560 --> 00:03:56,640
means three three masters that would be

104
00:03:55,040 --> 00:03:59,599
the cpu master

105
00:03:56,640 --> 00:04:02,319
the management engine spy bus master and

106
00:03:59,599 --> 00:04:04,640
the intel integrated gigabit ethernet

107
00:04:02,319 --> 00:04:07,120
spy bus master

108
00:04:04,640 --> 00:04:08,799
all right then we've got the flash ic8

109
00:04:07,120 --> 00:04:10,720
strap base address now this is not an

110
00:04:08,799 --> 00:04:13,040
ich so this is going to be the soft

111
00:04:10,720 --> 00:04:17,040
straps for the pch

112
00:04:13,040 --> 00:04:19,680
so boom that is offset 100

113
00:04:17,040 --> 00:04:21,840
looks like so boom point at the pch

114
00:04:19,680 --> 00:04:25,199
instead of the ich stuff

115
00:04:21,840 --> 00:04:29,199
next byte the ich strap length well pch

116
00:04:25,199 --> 00:04:32,240
strap length that is 12 but that is a

117
00:04:29,199 --> 00:04:35,759
hex zeros based thing so

118
00:04:32,240 --> 00:04:39,680
16 plus two is 18 so there's going to be

119
00:04:35,759 --> 00:04:41,120
18 data structures in this offset x100

120
00:04:39,680 --> 00:04:43,840
and specifically each of those data

121
00:04:41,120 --> 00:04:44,720
structures is a d word so 4 kilobytes

122
00:04:43,840 --> 00:04:48,000
each

123
00:04:44,720 --> 00:04:48,000
sorry 4 bytes each

