1
00:00:00,080 --> 00:00:03,840
this was the overall picture of how this

2
00:00:02,000 --> 00:00:05,839
worked but we still were abstracting

3
00:00:03,840 --> 00:00:07,919
some things by just saying you know you

4
00:00:05,839 --> 00:00:10,480
set an address to 1000 and you set to

5
00:00:07,919 --> 00:00:14,240
read 4 bytes and you send it to here and

6
00:00:10,480 --> 00:00:16,480
somehow it just works so let's enhance

7
00:00:14,240 --> 00:00:19,359
and understand that this spy bar memory

8
00:00:16,480 --> 00:00:22,080
mapped i o registers better

9
00:00:19,359 --> 00:00:24,560
so in the data sheets you will find this

10
00:00:22,080 --> 00:00:26,080
description spy bar plus offset and

11
00:00:24,560 --> 00:00:27,199
these are the lists of the various

12
00:00:26,080 --> 00:00:28,960
registers

13
00:00:27,199 --> 00:00:31,679
there are registers both for hardware

14
00:00:28,960 --> 00:00:33,520
sequencing and software sequencing so

15
00:00:31,679 --> 00:00:35,280
those are the two forms of access i was

16
00:00:33,520 --> 00:00:36,960
talking about hardware sequencing you

17
00:00:35,280 --> 00:00:38,800
just let the hardware figure out what it

18
00:00:36,960 --> 00:00:41,520
means to read and write and software

19
00:00:38,800 --> 00:00:43,840
sequencing you give the exact spy flash

20
00:00:41,520 --> 00:00:46,000
op codes for the particular chip or part

21
00:00:43,840 --> 00:00:48,960
that you're dealing with in order to go

22
00:00:46,000 --> 00:00:50,320
beneath the abstraction

23
00:00:48,960 --> 00:00:52,559
at this point i would point out that

24
00:00:50,320 --> 00:00:54,719
there is probably a

25
00:00:52,559 --> 00:00:56,399
typo here in that the

26
00:00:54,719 --> 00:01:00,640
hsfs

27
00:00:56,399 --> 00:01:03,039
ts and h s f c t l etcetera the you know

28
00:01:00,640 --> 00:01:05,680
they're saying status here and control

29
00:01:03,039 --> 00:01:07,280
there but uh more often than not in the

30
00:01:05,680 --> 00:01:11,520
data sheets it's just

31
00:01:07,280 --> 00:01:13,360
referred to as h s f s etc so whatever

32
00:01:11,520 --> 00:01:15,280
it's just in this five series data sheet

33
00:01:13,360 --> 00:01:18,159
so later on i'll probably refer to

34
00:01:15,280 --> 00:01:20,240
things as hsfs.blah

35
00:01:18,159 --> 00:01:21,200
and that's this is just to not confuse

36
00:01:20,240 --> 00:01:24,960
you

37
00:01:21,200 --> 00:01:26,080
so that was for the ich 9 and 10 pch 5

38
00:01:24,960 --> 00:01:28,560
through nine

39
00:01:26,080 --> 00:01:30,960
and unfortunately the names and

40
00:01:28,560 --> 00:01:33,759
locations of registers slightly shifts

41
00:01:30,960 --> 00:01:36,400
around between the nine series pch and

42
00:01:33,759 --> 00:01:38,400
the 100 series pch so i'm going to

43
00:01:36,400 --> 00:01:40,880
basically be flipping back and forth

44
00:01:38,400 --> 00:01:42,320
between these data sheets just so that

45
00:01:40,880 --> 00:01:43,920
depending on what you have hopefully

46
00:01:42,320 --> 00:01:45,439
it'll make it easier for you to find

47
00:01:43,920 --> 00:01:47,119
what you need to find

48
00:01:45,439 --> 00:01:49,520
so whereas the previous had you know

49
00:01:47,119 --> 00:01:51,759
hardware sequencing when there were two

50
00:01:49,520 --> 00:01:53,759
two byte registers here there's a single

51
00:01:51,759 --> 00:01:55,759
four byte register but for all intents

52
00:01:53,759 --> 00:01:58,880
purposes it's just those two 2 byte

53
00:01:55,759 --> 00:02:00,880
registers back to back

54
00:01:58,880 --> 00:02:01,840
now what is the sequence that you do to

55
00:02:00,880 --> 00:02:04,079
read

56
00:02:01,840 --> 00:02:06,719
you enter an address to read you enter a

57
00:02:04,079 --> 00:02:08,720
size to read you set the flash cycle

58
00:02:06,719 --> 00:02:10,720
type to read so it says like what kind

59
00:02:08,720 --> 00:02:11,920
of action do you want to take

60
00:02:10,720 --> 00:02:14,000
and then you check whether or not

61
00:02:11,920 --> 00:02:15,680
anyone's using the flash right now so

62
00:02:14,000 --> 00:02:18,080
you want to make sure you have you know

63
00:02:15,680 --> 00:02:20,239
exclusive access and if no one's using

64
00:02:18,080 --> 00:02:21,680
it then you say go and start to this

65
00:02:20,239 --> 00:02:23,680
read cycle

66
00:02:21,680 --> 00:02:25,280
then the chip is going to you know take

67
00:02:23,680 --> 00:02:27,840
a while we talked about how you know it

68
00:02:25,280 --> 00:02:29,280
might be running at 20 to 60 megahertz

69
00:02:27,840 --> 00:02:31,519
so it's going to take a while to get the

70
00:02:29,280 --> 00:02:33,680
data back to you but when it does that

71
00:02:31,519 --> 00:02:37,680
data will be stored in the f data

72
00:02:33,680 --> 00:02:39,840
registers 0 through n so up to 64 bytes

73
00:02:37,680 --> 00:02:41,760
32 bits at a time

74
00:02:39,840 --> 00:02:43,120
in the flash data registers so that's a

75
00:02:41,760 --> 00:02:45,200
read sequence

76
00:02:43,120 --> 00:02:47,440
and the right sequence is almost the

77
00:02:45,200 --> 00:02:49,680
same so you enter an address to write

78
00:02:47,440 --> 00:02:51,200
you enter a size to write and then you

79
00:02:49,680 --> 00:02:53,040
take the data that you want to write and

80
00:02:51,200 --> 00:02:55,440
you put it into those flash data

81
00:02:53,040 --> 00:02:58,000
registers so these data registers are

82
00:02:55,440 --> 00:03:01,040
used both for reads to read into there

83
00:02:58,000 --> 00:03:04,000
and for writes to write out from there

84
00:03:01,040 --> 00:03:06,239
then you set cycle type to write and you

85
00:03:04,000 --> 00:03:08,640
make sure that bios write enable is one

86
00:03:06,239 --> 00:03:10,560
so we'll talk about that a bit later

87
00:03:08,640 --> 00:03:13,519
and as long as no one else is using the

88
00:03:10,560 --> 00:03:15,920
chip then you can say go and again you

89
00:03:13,519 --> 00:03:18,080
just wait for the thing to take a while

90
00:03:15,920 --> 00:03:20,000
to do this write operation when it's

91
00:03:18,080 --> 00:03:21,760
finally done there will be a status bit

92
00:03:20,000 --> 00:03:24,159
that says it's done and so you just keep

93
00:03:21,760 --> 00:03:26,239
checking that to know when it's done

94
00:03:24,159 --> 00:03:28,720
but that's the high level view let's dig

95
00:03:26,239 --> 00:03:31,440
down deeper what exactly does it mean to

96
00:03:28,720 --> 00:03:33,599
enter an address to read

97
00:03:31,440 --> 00:03:37,120
so if we were to look at the 5 series

98
00:03:33,599 --> 00:03:39,200
chipset there is the f adder field at

99
00:03:37,120 --> 00:03:41,440
spy bar plus 8

100
00:03:39,200 --> 00:03:43,440
that is the flash address

101
00:03:41,440 --> 00:03:48,000
and it looks like this there is going to

102
00:03:43,440 --> 00:03:51,120
be 25 bits 0 through 24 25 bits that

103
00:03:48,000 --> 00:03:53,439
specify a flash linear address and so i

104
00:03:51,120 --> 00:03:56,640
said before that you know spy as a

105
00:03:53,439 --> 00:03:58,400
protocol only supports 24 bit addressing

106
00:03:56,640 --> 00:04:00,799
so normally you should only have access

107
00:03:58,400 --> 00:04:03,920
to 16 megabytes of space

108
00:04:00,799 --> 00:04:06,799
but i also said that the intel hardware

109
00:04:03,920 --> 00:04:08,319
allows for concatenating two spy flash

110
00:04:06,799 --> 00:04:10,319
chips back to back

111
00:04:08,319 --> 00:04:12,640
so in this class when we refer to a

112
00:04:10,319 --> 00:04:14,959
flash linear address you can kind of

113
00:04:12,640 --> 00:04:17,040
think of it as if it's a concatenation

114
00:04:14,959 --> 00:04:19,440
of however many spy flash chips the

115
00:04:17,040 --> 00:04:21,440
device supports so even though you know

116
00:04:19,440 --> 00:04:24,320
the hardware protocol may only support

117
00:04:21,440 --> 00:04:26,160
24 bits the intel hardware recognizes oh

118
00:04:24,320 --> 00:04:28,080
this address is too high i'm going to

119
00:04:26,160 --> 00:04:30,800
select the upper chip instead of lower

120
00:04:28,080 --> 00:04:32,400
chip and it'll just make it all work out

121
00:04:30,800 --> 00:04:34,800
so that's what it looked like for the 5

122
00:04:32,400 --> 00:04:37,280
series chipset now let's flip the page

123
00:04:34,800 --> 00:04:40,160
to a 100 series chipset

124
00:04:37,280 --> 00:04:42,400
here it is again at offset 8. it is the

125
00:04:40,160 --> 00:04:43,680
flash address and here it's called bios

126
00:04:42,400 --> 00:04:45,520
f adder

127
00:04:43,680 --> 00:04:47,199
and basically it'll look exactly the

128
00:04:45,520 --> 00:04:49,199
same as the other one you've got some

129
00:04:47,199 --> 00:04:51,680
number of bits that specify a flash

130
00:04:49,199 --> 00:04:54,880
linear address although interestingly

131
00:04:51,680 --> 00:04:56,960
this time it appears to be 27 bits so

132
00:04:54,880 --> 00:04:59,120
this again tells us that intel is doing

133
00:04:56,960 --> 00:05:01,360
something a little non-standard here and

134
00:04:59,120 --> 00:05:04,320
somehow allowing you to either access

135
00:05:01,360 --> 00:05:06,639
larger spy flash chips or and or the

136
00:05:04,320 --> 00:05:08,400
concatenation of you know morse by flash

137
00:05:06,639 --> 00:05:10,320
chips

138
00:05:08,400 --> 00:05:13,360
okay that was how we enter an address to

139
00:05:10,320 --> 00:05:15,600
read now we need to enter a size to read

140
00:05:13,360 --> 00:05:18,720
so how do we do that

141
00:05:15,600 --> 00:05:20,880
well on the 5 series chipset at offset 6

142
00:05:18,720 --> 00:05:23,759
is this 2 byte register the hardware

143
00:05:20,880 --> 00:05:27,199
sequencing flash control register

144
00:05:23,759 --> 00:05:30,320
and inside of that at bits 8 to 13

145
00:05:27,199 --> 00:05:31,680
there is the flash data byte count

146
00:05:30,320 --> 00:05:33,520
and this has a little bit of an

147
00:05:31,680 --> 00:05:36,240
interesting encoding here

148
00:05:33,520 --> 00:05:37,039
so basically there's you know six bits

149
00:05:36,240 --> 00:05:40,320
here

150
00:05:37,039 --> 00:05:42,160
and 0 would say i want to you know if

151
00:05:40,320 --> 00:05:44,479
you set all bits to 0 it would say i

152
00:05:42,160 --> 00:05:46,960
want to read a single byte

153
00:05:44,479 --> 00:05:49,360
and if you set all bits to 1 that would

154
00:05:46,960 --> 00:05:50,880
say i want to read 64 bytes and instead

155
00:05:49,360 --> 00:05:53,520
of just saying how many bytes you want

156
00:05:50,880 --> 00:05:55,280
to read you specify it using some number

157
00:05:53,520 --> 00:05:57,280
of bits

158
00:05:55,280 --> 00:05:58,800
all right now same thing on the 100

159
00:05:57,280 --> 00:06:02,000
series chips set

160
00:05:58,800 --> 00:06:03,759
offset 4 is this 4 byte register and

161
00:06:02,000 --> 00:06:07,199
it's basically the exact same thing

162
00:06:03,759 --> 00:06:10,639
we've got the flash data byte count at

163
00:06:07,199 --> 00:06:13,280
offset 24 it's again six bits and it has

164
00:06:10,639 --> 00:06:15,680
the same encoding so zero represents

165
00:06:13,280 --> 00:06:18,240
that you want one byte and three f or

166
00:06:15,680 --> 00:06:22,080
all ones indicates that you want 64

167
00:06:18,240 --> 00:06:24,319
bytes to be read in or written out

168
00:06:22,080 --> 00:06:26,400
so that's how we set a size to read now

169
00:06:24,319 --> 00:06:29,600
let's figure out how you set the flash

170
00:06:26,400 --> 00:06:29,600
cycle type to read

171
00:06:29,759 --> 00:06:33,919
once again back to the hardware

172
00:06:31,919 --> 00:06:35,680
sequencing flash control register at

173
00:06:33,919 --> 00:06:38,240
offset six

174
00:06:35,680 --> 00:06:40,400
there is at bits one and two

175
00:06:38,240 --> 00:06:41,520
two bits and if they're set to zero that

176
00:06:40,400 --> 00:06:43,039
says you're going to do a read if

177
00:06:41,520 --> 00:06:45,600
they're set to one zero that says you're

178
00:06:43,039 --> 00:06:46,560
gonna do a write and one one means an

179
00:06:45,600 --> 00:06:49,199
erase

180
00:06:46,560 --> 00:06:51,360
erase on spy flash chips specifically

181
00:06:49,199 --> 00:06:52,960
nor spy flash chips which is what these

182
00:06:51,360 --> 00:06:56,080
are and they're implemented with nor

183
00:06:52,960 --> 00:06:58,639
gates of transistors spy erases will

184
00:06:56,080 --> 00:07:00,319
actually set all bits equal to one so

185
00:06:58,639 --> 00:07:03,360
it's an interesting property of the way

186
00:07:00,319 --> 00:07:05,840
that spy nor flash chips work that you

187
00:07:03,360 --> 00:07:07,360
can never actually directly set a zero

188
00:07:05,840 --> 00:07:09,759
to a one when you're writing to this

189
00:07:07,360 --> 00:07:11,919
non-volatile memory you can set a one to

190
00:07:09,759 --> 00:07:13,919
a zero when you're writing but you can't

191
00:07:11,919 --> 00:07:16,880
set a zero to one the only way to set a

192
00:07:13,919 --> 00:07:19,360
zero to one is to do an erase and so

193
00:07:16,880 --> 00:07:21,840
generally there's some block granularity

194
00:07:19,360 --> 00:07:24,080
you know for instance four kilobytes 64

195
00:07:21,840 --> 00:07:26,319
kilobytes and basically software that

196
00:07:24,080 --> 00:07:27,840
wants to you know truly set an arbitrary

197
00:07:26,319 --> 00:07:30,000
value into

198
00:07:27,840 --> 00:07:31,680
uh the spy flash and not just flip ones

199
00:07:30,000 --> 00:07:33,120
to zeros if you want to set a truly

200
00:07:31,680 --> 00:07:34,560
arbitrary value you have to read the

201
00:07:33,120 --> 00:07:37,199
value that's there

202
00:07:34,560 --> 00:07:39,599
erase everything so that it all becomes

203
00:07:37,199 --> 00:07:42,160
ones and then write back the value that

204
00:07:39,599 --> 00:07:43,919
you want so that any ones get flipped to

205
00:07:42,160 --> 00:07:45,039
zeros and you end up with the correct

206
00:07:43,919 --> 00:07:46,160
final value

207
00:07:45,039 --> 00:07:49,199
so anyways

208
00:07:46,160 --> 00:07:53,680
flash cycle read is zero zero in this

209
00:07:49,199 --> 00:07:56,160
spy bar plus six hsfc register

210
00:07:53,680 --> 00:07:58,479
now looking at the 100 series chipset

211
00:07:56,160 --> 00:08:00,720
offset four again this combo two of

212
00:07:58,479 --> 00:08:02,720
register is all combined and it is the

213
00:08:00,720 --> 00:08:05,199
same sort of thing f cycle but now we

214
00:08:02,720 --> 00:08:07,520
see it's four bits instead of two bits

215
00:08:05,199 --> 00:08:10,080
so that means that intel extended it and

216
00:08:07,520 --> 00:08:12,800
added extra support for things so read

217
00:08:10,080 --> 00:08:16,240
is still zero right is still two

218
00:08:12,800 --> 00:08:18,560
and a race is now a four kilobyte block

219
00:08:16,240 --> 00:08:21,280
erase but it also supports things like

220
00:08:18,560 --> 00:08:23,120
64 kilobyte block arrays and other stuff

221
00:08:21,280 --> 00:08:24,800
that we don't actually care about

222
00:08:23,120 --> 00:08:26,319
all right now i'm going to skip over

223
00:08:24,800 --> 00:08:28,000
briefly this you know check that no

224
00:08:26,319 --> 00:08:29,520
one's already using it and i'm just

225
00:08:28,000 --> 00:08:31,120
going to go right to go so we'll come

226
00:08:29,520 --> 00:08:33,039
back to that when we hit that register

227
00:08:31,120 --> 00:08:34,479
for other reasons so normally you would

228
00:08:33,039 --> 00:08:36,320
check if anyone's using it before you

229
00:08:34,479 --> 00:08:38,800
say go but for now let's just assume

230
00:08:36,320 --> 00:08:41,120
you're doing it wrong and you just say

231
00:08:38,800 --> 00:08:43,599
go so how do you say go how do you say

232
00:08:41,120 --> 00:08:45,760
execute my read transaction for reading

233
00:08:43,599 --> 00:08:47,920
these this number of bytes from that

234
00:08:45,760 --> 00:08:49,600
particular flash linear address

235
00:08:47,920 --> 00:08:52,160
well to do that you use the hardware

236
00:08:49,600 --> 00:08:53,440
sequencing flash control register offset

237
00:08:52,160 --> 00:08:55,279
six

238
00:08:53,440 --> 00:08:57,760
and we've already seen this for the

239
00:08:55,279 --> 00:09:01,360
flash data byte count the flash cycle

240
00:08:57,760 --> 00:09:03,920
type and now at bit zero is flash cycle

241
00:09:01,360 --> 00:09:06,399
go so once everything's all set up you

242
00:09:03,920 --> 00:09:08,240
set flash cycle go to one and that says

243
00:09:06,399 --> 00:09:09,839
go ahead and do whatever kind of cycle

244
00:09:08,240 --> 00:09:11,839
was programmed here whatever number of

245
00:09:09,839 --> 00:09:13,680
bytes was programmed there and whatever

246
00:09:11,839 --> 00:09:15,519
address was programmed in the flash

247
00:09:13,680 --> 00:09:18,000
address register

248
00:09:15,519 --> 00:09:20,560
flipping to the 100 series chipset same

249
00:09:18,000 --> 00:09:22,720
thing hardware sequencing flash status

250
00:09:20,560 --> 00:09:26,080
and control and there's just an f go bit

251
00:09:22,720 --> 00:09:29,200
which is at bit 16.

252
00:09:26,080 --> 00:09:32,000
okay then the next step is to wait until

253
00:09:29,200 --> 00:09:34,640
the status says it's done so we said

254
00:09:32,000 --> 00:09:36,399
before spy is slow so you just have to

255
00:09:34,640 --> 00:09:37,760
kind of pull and find out whether it's

256
00:09:36,399 --> 00:09:39,920
done

257
00:09:37,760 --> 00:09:42,000
so where do you pull

258
00:09:39,920 --> 00:09:44,240
now we are going to look at the hardware

259
00:09:42,000 --> 00:09:45,360
sequencing flash status register we've

260
00:09:44,240 --> 00:09:47,279
been looking at the control

261
00:09:45,360 --> 00:09:49,279
predominantly in the flash address now

262
00:09:47,279 --> 00:09:51,279
let's talk about the status

263
00:09:49,279 --> 00:09:53,519
so the status register is what tells you

264
00:09:51,279 --> 00:09:56,480
when it's done so the hardware will

265
00:09:53,519 --> 00:09:58,720
automatically set this bit to 1 to

266
00:09:56,480 --> 00:10:01,360
indicate it's done doing whatever type

267
00:09:58,720 --> 00:10:03,040
of flash cycle you just told it to do

268
00:10:01,360 --> 00:10:04,560
and once it's done you might want to

269
00:10:03,040 --> 00:10:06,160
check whether or not there was an error

270
00:10:04,560 --> 00:10:07,200
because it might say it's done but there

271
00:10:06,160 --> 00:10:09,440
was an error and then you're not going

272
00:10:07,200 --> 00:10:11,760
to be reading valid data

273
00:10:09,440 --> 00:10:13,600
and while we're here this is actually

274
00:10:11,760 --> 00:10:15,920
where you would check whether or not the

275
00:10:13,600 --> 00:10:18,160
hardware is currently in use makes sense

276
00:10:15,920 --> 00:10:20,880
flash status register check whether

277
00:10:18,160 --> 00:10:23,839
there is a spy cycle in progress so

278
00:10:20,880 --> 00:10:25,279
basically the hardware sets this when

279
00:10:23,839 --> 00:10:28,079
someone says go and then it's

280
00:10:25,279 --> 00:10:29,920
continuously set until it says the

281
00:10:28,079 --> 00:10:31,839
hardware is done so basically the

282
00:10:29,920 --> 00:10:33,279
hardware is saying you know nope don't

283
00:10:31,839 --> 00:10:35,519
access this i'm currently doing

284
00:10:33,279 --> 00:10:38,320
something and then when it's done it

285
00:10:35,519 --> 00:10:39,680
unsets this and it sets the flash done

286
00:10:38,320 --> 00:10:41,839
bit

287
00:10:39,680 --> 00:10:44,640
and once more over to the 100 series

288
00:10:41,839 --> 00:10:47,680
chipsets and it's the same sort of thing

289
00:10:44,640 --> 00:10:50,000
with f done at bit 0

290
00:10:47,680 --> 00:10:52,880
same interpretation as before

291
00:10:50,000 --> 00:10:54,079
f error at bit 1 same interpretation as

292
00:10:52,880 --> 00:10:56,880
before

293
00:10:54,079 --> 00:10:59,600
and the cycle in progress bit at bit 5

294
00:10:56,880 --> 00:11:01,279
same interpretation as before

295
00:10:59,600 --> 00:11:02,800
now last but not least if you're

296
00:11:01,279 --> 00:11:05,120
actually reading data in you've got to

297
00:11:02,800 --> 00:11:07,360
find out where the data went and so to

298
00:11:05,120 --> 00:11:09,920
do that you look at the flash data

299
00:11:07,360 --> 00:11:09,920
registers

300
00:11:10,000 --> 00:11:14,640
so the 5 series data sheet shows it like

301
00:11:11,920 --> 00:11:17,440
this that at offset spy bar plus 10

302
00:11:14,640 --> 00:11:20,000
there's flash data 0 and at offsets

303
00:11:17,440 --> 00:11:23,839
spy bar plus 14 through

304
00:11:20,000 --> 00:11:26,640
4f are the flash data and registers

305
00:11:23,839 --> 00:11:29,279
so flash data 0 this is literally just

306
00:11:26,640 --> 00:11:31,360
32 bits of data that was read in from

307
00:11:29,279 --> 00:11:33,279
the spy flash chip so nothing

308
00:11:31,360 --> 00:11:35,440
particularly interesting there

309
00:11:33,279 --> 00:11:37,760
and then there's just all the rest of

310
00:11:35,440 --> 00:11:39,920
these extra registers that have the

311
00:11:37,760 --> 00:11:42,160
exact same function they're just data

312
00:11:39,920 --> 00:11:44,959
holding registers so if you said you

313
00:11:42,160 --> 00:11:46,640
know i want 64 bytes of data then all of

314
00:11:44,959 --> 00:11:48,480
these would be filled in if you said i

315
00:11:46,640 --> 00:11:50,079
want four bytes of data then actually

316
00:11:48,480 --> 00:11:54,160
none of these would be filled in but the

317
00:11:50,079 --> 00:11:56,240
spy bar plus 10 flash data 0 would be

318
00:11:54,160 --> 00:12:00,240
filled in right so however much data you

319
00:11:56,240 --> 00:12:02,880
ask for gets put into these registers

320
00:12:00,240 --> 00:12:05,519
and in the 100 series chipset it shows

321
00:12:02,880 --> 00:12:09,360
that all up front at the sort of

322
00:12:05,519 --> 00:12:10,880
spy bar overall register summary view

323
00:12:09,360 --> 00:12:12,959
and there's nothing particularly

324
00:12:10,880 --> 00:12:15,760
interesting about that once more it is

325
00:12:12,959 --> 00:12:18,000
just the exact same interpretation of

326
00:12:15,760 --> 00:12:19,279
32 bits of data that you read in from

327
00:12:18,000 --> 00:12:21,839
the device

328
00:12:19,279 --> 00:12:23,920
so that is digging deeper into how

329
00:12:21,839 --> 00:12:26,399
exactly you interface with this memory

330
00:12:23,920 --> 00:12:28,720
mapped io in order to cause flash cycles

331
00:12:26,399 --> 00:12:30,399
to for instance read in this case but

332
00:12:28,720 --> 00:12:33,040
you can see how you could do a write

333
00:12:30,399 --> 00:12:35,839
very similarly by just setting the flash

334
00:12:33,040 --> 00:12:37,920
site cycle type to right and writing

335
00:12:35,839 --> 00:12:41,680
data into the flash data registers

336
00:12:37,920 --> 00:12:41,680
before you start the cycle

