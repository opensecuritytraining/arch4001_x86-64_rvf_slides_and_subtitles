1
00:00:00,160 --> 00:00:03,679
all right now if you were using uefi

2
00:00:02,080 --> 00:00:05,440
tool you would click on the descriptor

3
00:00:03,679 --> 00:00:08,000
region so we can start understanding

4
00:00:05,440 --> 00:00:10,960
this stuff instead so next up back in

5
00:00:08,000 --> 00:00:13,200
the map it told us that offset hex 40 is

6
00:00:10,960 --> 00:00:15,280
going to be the region portion of the

7
00:00:13,200 --> 00:00:16,880
flash descriptor so how do we interpret

8
00:00:15,280 --> 00:00:18,320
it well we interpret it like this flash

9
00:00:16,880 --> 00:00:21,119
region zero

10
00:00:18,320 --> 00:00:23,359
like so zero and this is basically just

11
00:00:21,119 --> 00:00:26,880
a placeholder telling you that the flash

12
00:00:23,359 --> 00:00:28,960
descriptor region starts at offset zero

13
00:00:26,880 --> 00:00:30,560
and although it has a limit of zero it's

14
00:00:28,960 --> 00:00:33,120
that's just because it's always hard

15
00:00:30,560 --> 00:00:34,800
coded to end at fff so it's always a

16
00:00:33,120 --> 00:00:37,440
four kilobyte region

17
00:00:34,800 --> 00:00:38,960
next region is region one which is the

18
00:00:37,440 --> 00:00:41,680
bios region

19
00:00:38,960 --> 00:00:43,840
and so if we interpret bits zero through

20
00:00:41,680 --> 00:00:46,000
twelve so we got eight bits from here

21
00:00:43,840 --> 00:00:47,920
and then the next four bits little

22
00:00:46,000 --> 00:00:51,520
endian flipped around what you're gonna

23
00:00:47,920 --> 00:00:53,920
have is hex six zero zero

24
00:00:51,520 --> 00:00:57,280
but this is once again one of those bits

25
00:00:53,920 --> 00:01:01,199
24 to 12 and so it actually corresponds

26
00:00:57,280 --> 00:01:04,080
to six zero zero zero zero zero

27
00:01:01,199 --> 00:01:05,840
so the start of the bios region is at

28
00:01:04,080 --> 00:01:08,400
six megabytes

29
00:01:05,840 --> 00:01:12,400
and the limit of the region

30
00:01:08,400 --> 00:01:15,280
taken from here these 12 bits so 8 bits

31
00:01:12,400 --> 00:01:17,680
from there 4 bits from there so it's bff

32
00:01:15,280 --> 00:01:20,000
and then for region limits you always

33
00:01:17,680 --> 00:01:21,360
fill in the last significant 12 bits

34
00:01:20,000 --> 00:01:24,840
with f's

35
00:01:21,360 --> 00:01:28,320
so the region limit is

36
00:01:24,840 --> 00:01:32,000
bff which is right up to the region

37
00:01:28,320 --> 00:01:34,560
range of c000 which is 12 megabytes so

38
00:01:32,000 --> 00:01:38,079
basically this thing goes from 6

39
00:01:34,560 --> 00:01:39,840
megabytes up to 12 megabytes so that is

40
00:01:38,079 --> 00:01:41,439
the end of the flash chip so the bios

41
00:01:39,840 --> 00:01:42,960
region as you would expect is at the end

42
00:01:41,439 --> 00:01:45,040
of the flash chip so that the reset

43
00:01:42,960 --> 00:01:46,720
vector properly gets mapped to the top

44
00:01:45,040 --> 00:01:47,680
of the four gigabyte physical address

45
00:01:46,720 --> 00:01:50,320
range

46
00:01:47,680 --> 00:01:53,040
all right then the next flash region is

47
00:01:50,320 --> 00:01:55,360
the management engine region take the 12

48
00:01:53,040 --> 00:01:58,000
bits from here interpret it pass it

49
00:01:55,360 --> 00:02:00,840
through as the bits 24 to 12 and what

50
00:01:58,000 --> 00:02:02,719
you get is a management engine region of

51
00:02:00,840 --> 00:02:03,759
5000

52
00:02:02,719 --> 00:02:06,240
okay

53
00:02:03,759 --> 00:02:09,599
and what is the limit of that region

54
00:02:06,240 --> 00:02:12,319
taking these 12 bits we got 5 ff fill in

55
00:02:09,599 --> 00:02:15,040
the least significant 12 bits with fff

56
00:02:12,319 --> 00:02:17,440
and that means that the bios region goes

57
00:02:15,040 --> 00:02:20,239
from hex 5000

58
00:02:17,440 --> 00:02:22,400
right up to the base of the bios region

59
00:02:20,239 --> 00:02:25,520
so the boss region starts at six

60
00:02:22,400 --> 00:02:28,640
megabytes and the management engine ends

61
00:02:25,520 --> 00:02:30,800
at six megabytes minus one byte so there

62
00:02:28,640 --> 00:02:33,040
you go those two things are adjacent and

63
00:02:30,800 --> 00:02:35,840
taking a big chunk of the flash chip

64
00:02:33,040 --> 00:02:38,319
there then the next region is the

65
00:02:35,840 --> 00:02:40,160
gigabit ethernet region again grabbing

66
00:02:38,319 --> 00:02:43,840
12 bits for the base

67
00:02:40,160 --> 00:02:46,319
that gives us a base address of hex 1000

68
00:02:43,840 --> 00:02:49,760
and then we get the bits for the limit

69
00:02:46,319 --> 00:02:52,080
and the limit is 4ff and that is right

70
00:02:49,760 --> 00:02:54,400
up to the management engine region right

71
00:02:52,080 --> 00:02:56,640
so in total we have the flash descriptor

72
00:02:54,400 --> 00:02:58,879
starting at offset zero and then going

73
00:02:56,640 --> 00:02:59,920
for always a hard-coded hex 1000 worth

74
00:02:58,879 --> 00:03:02,159
of bytes

75
00:02:59,920 --> 00:03:05,040
then at the next available spot is the

76
00:03:02,159 --> 00:03:07,920
gigabit ethernet starting at hex 1000

77
00:03:05,040 --> 00:03:09,599
ending at hex4fff

78
00:03:07,920 --> 00:03:11,680
so it goes right up to the management

79
00:03:09,599 --> 00:03:15,120
engine management engine goes from hex

80
00:03:11,680 --> 00:03:16,640
5000 all the way up to 5fff which right

81
00:03:15,120 --> 00:03:19,040
up to the bias region and then the bios

82
00:03:16,640 --> 00:03:21,200
region goes from six megabytes all the

83
00:03:19,040 --> 00:03:24,319
way to the end of the chip at

84
00:03:21,200 --> 00:03:26,080
12 megabytes minus one byte

85
00:03:24,319 --> 00:03:27,840
now there are other regions to consider

86
00:03:26,080 --> 00:03:31,519
here specifically this hardware does

87
00:03:27,840 --> 00:03:33,519
support the platform data region but we

88
00:03:31,519 --> 00:03:36,319
can see that this is filled in with what

89
00:03:33,519 --> 00:03:38,239
look like the magic invalid values to

90
00:03:36,319 --> 00:03:39,760
basically say this is not being used

91
00:03:38,239 --> 00:03:41,440
that's good because there's no more

92
00:03:39,760 --> 00:03:43,840
space on the flash chip all the space

93
00:03:41,440 --> 00:03:45,440
was taken up by all of these regions so

94
00:03:43,840 --> 00:03:46,799
it says if the platform data region is

95
00:03:45,440 --> 00:03:48,720
not used the region base must be

96
00:03:46,799 --> 00:03:51,360
programmed to one ff

97
00:03:48,720 --> 00:03:53,599
f and indeed it is and the limit to

98
00:03:51,360 --> 00:03:55,200
zeros which indeed it is so

99
00:03:53,599 --> 00:03:58,720
platform data region not used on the

100
00:03:55,200 --> 00:03:58,720
optiplex 7010

