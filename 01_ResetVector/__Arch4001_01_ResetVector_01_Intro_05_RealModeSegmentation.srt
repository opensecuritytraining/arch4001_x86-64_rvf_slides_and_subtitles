1
00:00:00,640 --> 00:00:05,359
finally let's talk about how

2
00:00:01,920 --> 00:00:08,320
segmentation works in 16-bit real mode

3
00:00:05,359 --> 00:00:10,240
so even though it is a 16-bit execution

4
00:00:08,320 --> 00:00:12,960
mode segmentation allows for the

5
00:00:10,240 --> 00:00:14,880
addressing of up to 20 bits at a time so

6
00:00:12,960 --> 00:00:17,039
20 bits is a one megabyte chunk of

7
00:00:14,880 --> 00:00:19,119
memory 16 bits would only be a 65

8
00:00:17,039 --> 00:00:21,680
kilobyte chunk of memory now when the

9
00:00:19,119 --> 00:00:24,160
processor is executing in 16 bit mode

10
00:00:21,680 --> 00:00:26,720
the default address size and operand

11
00:00:24,160 --> 00:00:29,359
size is going to be 16 bits

12
00:00:26,720 --> 00:00:31,679
in architecture 2001 we talked about how

13
00:00:29,359 --> 00:00:34,079
even when you were in long mode 64-bit

14
00:00:31,679 --> 00:00:36,480
mode the default operand size and

15
00:00:34,079 --> 00:00:38,719
address size was actually 32-bits and

16
00:00:36,480 --> 00:00:41,840
you had to use instruction prefixes in

17
00:00:38,719 --> 00:00:44,800
order to increase that size to 64-bits

18
00:00:41,840 --> 00:00:47,200
so here it defaults to 16 bits but again

19
00:00:44,800 --> 00:00:49,760
you can use instruction prefixes in

20
00:00:47,200 --> 00:00:52,559
order to increase it and make 16-bit

21
00:00:49,760 --> 00:00:54,160
code access a 32-bit register on you

22
00:00:52,559 --> 00:00:56,160
know modern systems that actually have

23
00:00:54,160 --> 00:00:58,079
32-bit registers not on the original

24
00:00:56,160 --> 00:01:00,399
systems and you would actually see that

25
00:00:58,079 --> 00:01:03,120
if you walked through the reset vector

26
00:01:00,399 --> 00:01:05,760
code for a ways in simics so here's how

27
00:01:03,120 --> 00:01:08,880
segmentation works in 16-bit mode you've

28
00:01:05,760 --> 00:01:11,520
got a 16-bit segment selector and that

29
00:01:08,880 --> 00:01:13,520
is going to be shifted four bits to the

30
00:01:11,520 --> 00:01:15,280
left so the segment selector would be

31
00:01:13,520 --> 00:01:17,680
stored in one of your segment registers

32
00:01:15,280 --> 00:01:19,280
csds ss etc

33
00:01:17,680 --> 00:01:21,600
so you take the segment selector shift

34
00:01:19,280 --> 00:01:24,159
it four bits to the left and then add it

35
00:01:21,600 --> 00:01:26,400
to the 16 bit effective address so

36
00:01:24,159 --> 00:01:27,520
instruction pointer stack pointer

37
00:01:26,400 --> 00:01:29,680
whatever

38
00:01:27,520 --> 00:01:32,240
that 16 bit shifted four bits to the

39
00:01:29,680 --> 00:01:34,079
left gives you basically a upper bits of

40
00:01:32,240 --> 00:01:36,560
a 20 bit address which when added to

41
00:01:34,079 --> 00:01:38,640
this yields a 20 bit address now even

42
00:01:36,560 --> 00:01:41,439
though you can access up to one

43
00:01:38,640 --> 00:01:43,439
megabytes with a 20-bit address in

44
00:01:41,439 --> 00:01:45,840
practice because you've got the 16-bit

45
00:01:43,439 --> 00:01:48,320
offset within the given segment you're

46
00:01:45,840 --> 00:01:50,720
only really accessing 64 kilobytes at a

47
00:01:48,320 --> 00:01:52,560
time and then the segment selector has

48
00:01:50,720 --> 00:01:55,600
to be you know shifted around in order

49
00:01:52,560 --> 00:01:57,360
to access 64 kilobytes at a time within

50
00:01:55,600 --> 00:01:59,280
you know a given you know one megabyte

51
00:01:57,360 --> 00:02:01,200
region so you can see this is much

52
00:01:59,280 --> 00:02:03,920
simpler than the way segmentation worked

53
00:02:01,200 --> 00:02:06,000
on 32 and 64-bit systems it's pretty

54
00:02:03,920 --> 00:02:08,479
much just adding together two values to

55
00:02:06,000 --> 00:02:10,160
get a different linear address but

56
00:02:08,479 --> 00:02:12,640
because it behaves that way where you

57
00:02:10,160 --> 00:02:14,640
just add together your shift and add

58
00:02:12,640 --> 00:02:17,120
it leads to an interesting property that

59
00:02:14,640 --> 00:02:18,560
you can have duplication of linear

60
00:02:17,120 --> 00:02:21,200
addresses for different segment

61
00:02:18,560 --> 00:02:23,840
selectors and effective addresses

62
00:02:21,200 --> 00:02:26,879
so for instance if you had you know a

63
00:02:23,840 --> 00:02:29,920
segment selector of 302e and you had a

64
00:02:26,879 --> 00:02:31,800
linear address of 1057 you add those

65
00:02:29,920 --> 00:02:33,599
together you get

66
00:02:31,800 --> 00:02:35,840
31337

67
00:02:33,599 --> 00:02:38,000
but there's different ways that you can

68
00:02:35,840 --> 00:02:39,840
yield three one three three seven

69
00:02:38,000 --> 00:02:42,560
so you could have you know that which we

70
00:02:39,840 --> 00:02:44,000
just showed or two two three three foot

71
00:02:42,560 --> 00:02:45,360
and add it together three one three

72
00:02:44,000 --> 00:02:47,360
three seven or

73
00:02:45,360 --> 00:02:49,680
two fate last

74
00:02:47,360 --> 00:02:51,920
and get three one three three seven

75
00:02:49,680 --> 00:02:53,599
so basically this is a bit of a quirk

76
00:02:51,920 --> 00:02:55,360
and it means that memory management

77
00:02:53,599 --> 00:02:57,519
within this sort of regime was a little

78
00:02:55,360 --> 00:02:59,760
bit confusing because you never

79
00:02:57,519 --> 00:03:01,840
necessarily knew without you know going

80
00:02:59,760 --> 00:03:03,040
and doing the actual math what kind of

81
00:03:01,840 --> 00:03:04,800
linear address you were going to deal

82
00:03:03,040 --> 00:03:06,800
with and so that was ultimately the

83
00:03:04,800 --> 00:03:08,480
responsibility of bios's and operating

84
00:03:06,800 --> 00:03:11,519
systems these being old operating

85
00:03:08,480 --> 00:03:13,920
systems like dos to manage the segments

86
00:03:11,519 --> 00:03:16,080
in a way that made sense to them so

87
00:03:13,920 --> 00:03:19,680
that's all we're going to talk about for

88
00:03:16,080 --> 00:03:21,760
reset vector and real mode so as we said

89
00:03:19,680 --> 00:03:23,760
we're on our way down to find out more

90
00:03:21,760 --> 00:03:25,360
about flash right protection and that

91
00:03:23,760 --> 00:03:27,760
was just the beginning of what we need

92
00:03:25,360 --> 00:03:29,760
to know to understand how the bios get

93
00:03:27,760 --> 00:03:31,760
started and why it doesn't necessarily

94
00:03:29,760 --> 00:03:36,159
stay in real mode and it moves on to

95
00:03:31,760 --> 00:03:36,159
things like x86 protected mode

