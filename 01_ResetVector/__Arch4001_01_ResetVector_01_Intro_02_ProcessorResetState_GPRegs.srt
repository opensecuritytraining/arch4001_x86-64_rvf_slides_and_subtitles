1
00:00:00,240 --> 00:00:04,799
so what else does simx have to teach us

2
00:00:02,240 --> 00:00:07,680
about the initial state after reset

3
00:00:04,799 --> 00:00:10,880
well it tells us that crs4 is mostly all

4
00:00:07,680 --> 00:00:13,440
zeros cr3 is zero and it says paging is

5
00:00:10,880 --> 00:00:17,039
disabled which is what we would expect

6
00:00:13,440 --> 00:00:18,720
and cr0 is mostly all zeros as well

7
00:00:17,039 --> 00:00:21,359
so let's dig into each of those a little

8
00:00:18,720 --> 00:00:22,240
bit more so cr2 three and four are all

9
00:00:21,359 --> 00:00:25,359
zero

10
00:00:22,240 --> 00:00:27,119
cr0 has some particular fixed value that

11
00:00:25,359 --> 00:00:29,519
it's likely been set to for quite a

12
00:00:27,119 --> 00:00:32,000
while furthermore here are the bits that

13
00:00:29,519 --> 00:00:34,480
we have seen in architecture 2001 and

14
00:00:32,000 --> 00:00:36,160
the pe in particular we said was going

15
00:00:34,480 --> 00:00:39,280
to be the one that we would see in 2001

16
00:00:36,160 --> 00:00:43,040
and 4001. so the initial state here is 0

17
00:00:39,280 --> 00:00:45,520
0 0 and that 0 right there is the

18
00:00:43,040 --> 00:00:47,280
protection enable bit so when protected

19
00:00:45,520 --> 00:00:49,039
mode is not enabled that means we must

20
00:00:47,280 --> 00:00:51,680
be in real mode

21
00:00:49,039 --> 00:00:52,640
and so what is real how do you define

22
00:00:51,680 --> 00:00:54,559
rio

23
00:00:52,640 --> 00:00:58,079
if you're talking about what you can set

24
00:00:54,559 --> 00:01:00,160
for instance cr0.pe equals zero then

25
00:00:58,079 --> 00:01:02,559
real is merely electrical signals

26
00:01:00,160 --> 00:01:04,799
interpreted by your cpu

27
00:01:02,559 --> 00:01:06,720
morpheus

28
00:01:04,799 --> 00:01:10,640
and the rest of the values are filled in

29
00:01:06,720 --> 00:01:13,600
here and we can see up at bit 31 paging

30
00:01:10,640 --> 00:01:15,040
is also disabled as we would expect

31
00:01:13,600 --> 00:01:17,360
but what about some of these other

32
00:01:15,040 --> 00:01:19,840
things that are actually set to one so

33
00:01:17,360 --> 00:01:22,400
here we have the et bit i'm not going to

34
00:01:19,840 --> 00:01:24,720
do the et phone home it's the extension

35
00:01:22,400 --> 00:01:26,240
type bit that indicated originally

36
00:01:24,720 --> 00:01:28,960
whether or not the particular hardware

37
00:01:26,240 --> 00:01:31,040
supported some math coprocessor

38
00:01:28,960 --> 00:01:33,200
instructions and now everything supports

39
00:01:31,040 --> 00:01:35,200
it so it's always set to one

40
00:01:33,200 --> 00:01:38,000
then furthermore there's the cache

41
00:01:35,200 --> 00:01:40,640
disable and not write through bits so

42
00:01:38,000 --> 00:01:42,640
these both have to do with caching and

43
00:01:40,640 --> 00:01:44,799
basically it has to do with your initial

44
00:01:42,640 --> 00:01:47,040
state you don't want the cache to be

45
00:01:44,799 --> 00:01:50,320
used you want to be cleaned up and you

46
00:01:47,040 --> 00:01:52,320
know executing directly from the flash

47
00:01:50,320 --> 00:01:54,000
now i want to go look up in the manual

48
00:01:52,320 --> 00:01:56,079
you know what exactly does it mean when

49
00:01:54,000 --> 00:01:59,200
cash disable is one and non-write

50
00:01:56,079 --> 00:02:01,520
through is one and as i was reading it i

51
00:01:59,200 --> 00:02:03,680
read this little bit here saying that

52
00:02:01,520 --> 00:02:05,360
read hits access the cash and i was like

53
00:02:03,680 --> 00:02:07,600
well that's counter-intuitive that's

54
00:02:05,360 --> 00:02:09,200
weird like i didn't expect that you know

55
00:02:07,600 --> 00:02:12,239
the reset state should be reading from

56
00:02:09,200 --> 00:02:13,360
the cache that seems like an attack

57
00:02:12,239 --> 00:02:16,400
whoa

58
00:02:13,360 --> 00:02:18,560
can i execute stuff from the cache after

59
00:02:16,400 --> 00:02:19,920
reset to have attack control data

60
00:02:18,560 --> 00:02:23,440
executed

61
00:02:19,920 --> 00:02:25,520
and the answer is no so sad keanu and i

62
00:02:23,440 --> 00:02:27,680
feel like every time i come across this

63
00:02:25,520 --> 00:02:30,560
little bit you know however many years

64
00:02:27,680 --> 00:02:32,239
ago six seven eight years ago whenever i

65
00:02:30,560 --> 00:02:33,920
first came across this

66
00:02:32,239 --> 00:02:35,519
i you know said to john butterworth hey

67
00:02:33,920 --> 00:02:37,840
like check that out you know that seems

68
00:02:35,519 --> 00:02:39,280
like that could potentially be an attack

69
00:02:37,840 --> 00:02:40,800
and then you know you read through the

70
00:02:39,280 --> 00:02:43,440
manuals a little bit more and you find

71
00:02:40,800 --> 00:02:45,440
things like this initialization overview

72
00:02:43,440 --> 00:02:46,959
upon reset it also invalidates the

73
00:02:45,440 --> 00:02:49,360
internal caches

74
00:02:46,959 --> 00:02:51,280
or because all internal cache lines are

75
00:02:49,360 --> 00:02:53,760
invalid following reset initialization

76
00:02:51,280 --> 00:02:56,560
it is not necessary to invalidate cache

77
00:02:53,760 --> 00:03:00,720
before enabling caching so

78
00:02:56,560 --> 00:03:02,319
not woe it's a no sad keyanu

79
00:03:00,720 --> 00:03:04,560
so the key thing though is that you

80
00:03:02,319 --> 00:03:06,800
shouldn't behave that virtual machines

81
00:03:04,560 --> 00:03:08,400
behave like real physical ones because

82
00:03:06,800 --> 00:03:10,400
it's actually been shown that there are

83
00:03:08,400 --> 00:03:12,000
those sort of attacks in the past where

84
00:03:10,400 --> 00:03:14,239
someone was able to write into the

85
00:03:12,000 --> 00:03:16,080
virtual bios and then it would actually

86
00:03:14,239 --> 00:03:17,840
persist across reset because the

87
00:03:16,080 --> 00:03:20,159
virtualization system didn't actually

88
00:03:17,840 --> 00:03:22,159
clear it so never take it for granted

89
00:03:20,159 --> 00:03:24,159
that something that doesn't work on a

90
00:03:22,159 --> 00:03:26,959
physical machine won't work on a virtual

91
00:03:24,159 --> 00:03:26,959
machine

