1
00:00:00,240 --> 00:00:04,080
tell me

2
00:00:01,599 --> 00:00:06,240
have you ever heard the tragedy of darth

3
00:00:04,080 --> 00:00:07,680
plagueis the wise

4
00:00:06,240 --> 00:00:10,800
i thought not

5
00:00:07,680 --> 00:00:13,040
it's not a story a jedi would tell you

6
00:00:10,800 --> 00:00:16,000
so we're going to learn about darth

7
00:00:13,040 --> 00:00:19,039
plagueis the wise before he was betrayed

8
00:00:16,000 --> 00:00:21,119
by his apprentice darth sidious

9
00:00:19,039 --> 00:00:23,680
when darth plagueis fought with darth

10
00:00:21,119 --> 00:00:26,720
vades and ultimately bested him

11
00:00:23,680 --> 00:00:29,279
putting him into a coma a deep sleep a

12
00:00:26,720 --> 00:00:31,920
deep death-like sleep and ultimately

13
00:00:29,279 --> 00:00:33,920
using that sleep in order to understand

14
00:00:31,920 --> 00:00:36,399
the workings of midi-chlorians and the

15
00:00:33,920 --> 00:00:37,600
power over life and death

16
00:00:36,399 --> 00:00:40,960
so

17
00:00:37,600 --> 00:00:43,200
let's talk about how sleep wake works on

18
00:00:40,960 --> 00:00:45,920
uefi systems

19
00:00:43,200 --> 00:00:47,920
so uefi has a series of phases which you

20
00:00:45,920 --> 00:00:50,079
can learn more about in future uefi

21
00:00:47,920 --> 00:00:52,160
classes but the basic point is it starts

22
00:00:50,079 --> 00:00:54,480
with the security phase moves to the

23
00:00:52,160 --> 00:00:56,480
pre-efi phase and then the driver

24
00:00:54,480 --> 00:00:58,160
execution environment

25
00:00:56,480 --> 00:01:00,079
now as you well know it takes quite a

26
00:00:58,160 --> 00:01:02,879
while to boot up the system and the

27
00:01:00,079 --> 00:01:05,040
whole point of things like s3 and s4 is

28
00:01:02,879 --> 00:01:07,680
that they're supposed to be faster than

29
00:01:05,040 --> 00:01:10,159
having to do a full normal boot so you

30
00:01:07,680 --> 00:01:13,360
want s3 to come up faster and so how

31
00:01:10,159 --> 00:01:15,920
that's achieved is via the boot script

32
00:01:13,360 --> 00:01:18,560
so basically if there's some sequence of

33
00:01:15,920 --> 00:01:20,720
memory configuration register sets and

34
00:01:18,560 --> 00:01:22,880
locks and things like that that would be

35
00:01:20,720 --> 00:01:25,600
achieved on the normal boot path the

36
00:01:22,880 --> 00:01:28,320
boot script is a place where the dixie

37
00:01:25,600 --> 00:01:31,040
phase and the pei phase can basically

38
00:01:28,320 --> 00:01:32,640
store some sequence of memory reads

39
00:01:31,040 --> 00:01:34,960
memory rights

40
00:01:32,640 --> 00:01:37,680
register reads register rights and

41
00:01:34,960 --> 00:01:39,600
ultimately arbitrary code execution so

42
00:01:37,680 --> 00:01:41,759
it can store some set of things that

43
00:01:39,600 --> 00:01:45,040
should be done to basically skip and

44
00:01:41,759 --> 00:01:47,840
shortcut this entire pei and dixie phase

45
00:01:45,040 --> 00:01:49,920
on the wake from sleep

46
00:01:47,840 --> 00:01:51,439
so basically you boot along you save off

47
00:01:49,920 --> 00:01:54,000
all your state you go into the operating

48
00:01:51,439 --> 00:01:56,640
system if it goes down to s3 sleep then

49
00:01:54,000 --> 00:01:58,479
it boots again in the sec phase and then

50
00:01:56,640 --> 00:02:00,560
pei says oh hey it looks like i'm

51
00:01:58,479 --> 00:02:02,320
actually waking up from s3 according to

52
00:02:00,560 --> 00:02:04,799
this you know sleep type that i see

53
00:02:02,320 --> 00:02:06,399
saved in this register so i'm going to

54
00:02:04,799 --> 00:02:08,720
just not do all the stuff i would

55
00:02:06,399 --> 00:02:10,399
normally do in pei instead i'm going to

56
00:02:08,720 --> 00:02:12,400
hand off to something that's just going

57
00:02:10,399 --> 00:02:14,480
to invoke each of the elements of the

58
00:02:12,400 --> 00:02:16,560
script to set all the registers read all

59
00:02:14,480 --> 00:02:18,319
the registers write all the registers

60
00:02:16,560 --> 00:02:20,080
write to memory whatever i need to do

61
00:02:18,319 --> 00:02:23,520
according to that script that was saved

62
00:02:20,080 --> 00:02:25,760
at boot time in order to boot faster

63
00:02:23,520 --> 00:02:28,720
so it's a good idea in principle

64
00:02:25,760 --> 00:02:30,400
although from this document from 2004

65
00:02:28,720 --> 00:02:34,000
you can see that the bootscript table is

66
00:02:30,400 --> 00:02:36,560
in acpi nvs so that is just a special

67
00:02:34,000 --> 00:02:38,640
reserved region reserved between the

68
00:02:36,560 --> 00:02:41,120
firmware when it's handing off acpi

69
00:02:38,640 --> 00:02:43,440
information to an operating system

70
00:02:41,120 --> 00:02:45,120
but it's not really protected in any way

71
00:02:43,440 --> 00:02:47,040
if an attacker lives in the operating

72
00:02:45,120 --> 00:02:49,360
system they can just read and write this

73
00:02:47,040 --> 00:02:51,440
content to their heart's desire

74
00:02:49,360 --> 00:02:53,120
so if the previous attack was just go to

75
00:02:51,440 --> 00:02:55,200
sleep and wake up and see if stuff is

76
00:02:53,120 --> 00:02:57,120
unlocked well the defense for that

77
00:02:55,200 --> 00:02:58,879
should be to ensure that it's locked and

78
00:02:57,120 --> 00:03:00,959
how that's typically done on uefi

79
00:02:58,879 --> 00:03:02,879
systems is via this resume

80
00:03:00,959 --> 00:03:05,040
reconfiguration script

81
00:03:02,879 --> 00:03:06,720
so if we look a little bit closer about

82
00:03:05,040 --> 00:03:09,120
what kind of stuff can exist in that

83
00:03:06,720 --> 00:03:11,360
script we can see it says things like i

84
00:03:09,120 --> 00:03:14,239
o write i o read memory write memory

85
00:03:11,360 --> 00:03:17,680
read pci config right pci config read

86
00:03:14,239 --> 00:03:20,239
smbus stall and a very interesting thing

87
00:03:17,680 --> 00:03:22,239
called a dispatch opcode

88
00:03:20,239 --> 00:03:24,080
well all of these make sense as well as

89
00:03:22,239 --> 00:03:25,519
shortcut way to just you know read and

90
00:03:24,080 --> 00:03:27,840
write registers and remembering things

91
00:03:25,519 --> 00:03:30,239
like that but the dispatch app code is

92
00:03:27,840 --> 00:03:34,080
basically just a jump to an arbitrary

93
00:03:30,239 --> 00:03:37,200
code location that seems dangerous so if

94
00:03:34,080 --> 00:03:40,000
you have a wake script that's living in

95
00:03:37,200 --> 00:03:42,799
acpi non-volatile storage memory which

96
00:03:40,000 --> 00:03:44,799
is just ram that happens to be reserved

97
00:03:42,799 --> 00:03:47,680
per agreement between the firmware and

98
00:03:44,799 --> 00:03:49,760
operating system well a dark sith lord

99
00:03:47,680 --> 00:03:52,319
doesn't have to abide by the agreements

100
00:03:49,760 --> 00:03:54,080
of no firmware and operating system so

101
00:03:52,319 --> 00:03:54,879
let's see what that attack would look

102
00:03:54,080 --> 00:03:57,439
like

103
00:03:54,879 --> 00:03:59,360
here we have darth vades booting along

104
00:03:57,439 --> 00:04:02,000
happily and healthily

105
00:03:59,360 --> 00:04:03,840
pi phase dixie phase you know and let's

106
00:04:02,000 --> 00:04:05,439
say that they you know finish up all of

107
00:04:03,840 --> 00:04:07,840
their locks by the dixie phase and

108
00:04:05,439 --> 00:04:10,720
they're writing the information into the

109
00:04:07,840 --> 00:04:13,200
s3 resume script in order to make sure

110
00:04:10,720 --> 00:04:15,599
that that thing will also lock things up

111
00:04:13,200 --> 00:04:17,519
as it's coming back up from sleep

112
00:04:15,599 --> 00:04:19,440
and then it continues on and it boots to

113
00:04:17,519 --> 00:04:21,359
the operating system

114
00:04:19,440 --> 00:04:24,720
enter darth plagueis

115
00:04:21,359 --> 00:04:27,040
who writes to the acpi non-volatile

116
00:04:24,720 --> 00:04:30,560
storage memory and writes in for

117
00:04:27,040 --> 00:04:32,560
instance a new dispatch op code that

118
00:04:30,560 --> 00:04:34,720
will just jump to some arbitrary con

119
00:04:32,560 --> 00:04:37,600
arbitrary location that darth plagueis

120
00:04:34,720 --> 00:04:39,919
chooses now we said s3

121
00:04:37,600 --> 00:04:41,919
the ram is going to be in self-refresh

122
00:04:39,919 --> 00:04:44,240
mode so ram doesn't go away between

123
00:04:41,919 --> 00:04:45,919
going down into s3 and coming back up

124
00:04:44,240 --> 00:04:47,919
nor an s4

125
00:04:45,919 --> 00:04:50,160
but basically so the attacker can have

126
00:04:47,919 --> 00:04:52,320
some code somewhere they can have this

127
00:04:50,160 --> 00:04:54,000
little dispatch op code and it's going

128
00:04:52,320 --> 00:04:57,040
to jump into the code when the system

129
00:04:54,000 --> 00:04:59,040
starts waking from sleep so darth

130
00:04:57,040 --> 00:05:03,199
plagueis says sleep

131
00:04:59,040 --> 00:05:05,680
puts darth vanamous into the coma

132
00:05:03,199 --> 00:05:06,880
and now when darth vanamous ultimately

133
00:05:05,680 --> 00:05:08,160
wakes up

134
00:05:06,880 --> 00:05:10,639
from sleep

135
00:05:08,160 --> 00:05:12,320
it's running in an environment where the

136
00:05:10,639 --> 00:05:13,440
locks have all come unlocked that's how

137
00:05:12,320 --> 00:05:15,680
things are supposed to work those

138
00:05:13,440 --> 00:05:18,639
platform resets and other signals

139
00:05:15,680 --> 00:05:20,479
indicate that upon reset of which you

140
00:05:18,639 --> 00:05:22,840
know s3 is considered one of those

141
00:05:20,479 --> 00:05:25,199
resets all of the locks are going to go

142
00:05:22,840 --> 00:05:27,199
away so it's running through this

143
00:05:25,199 --> 00:05:28,720
unlocked environment and of course you

144
00:05:27,199 --> 00:05:30,720
know it's potentially vulnerable during

145
00:05:28,720 --> 00:05:32,639
this code execution phase before it gets

146
00:05:30,720 --> 00:05:34,400
up to the lock so if there's any you

147
00:05:32,639 --> 00:05:36,160
know vulnerabilities typical bios

148
00:05:34,400 --> 00:05:37,759
exploitable vulnerabilities any typical

149
00:05:36,160 --> 00:05:39,919
you know dma vulnerabilities anything

150
00:05:37,759 --> 00:05:41,840
like that that would be a problem

151
00:05:39,919 --> 00:05:45,840
but eventually it will come and it will

152
00:05:41,840 --> 00:05:48,240
try to utilize this bootscript executor

153
00:05:45,840 --> 00:05:50,479
and what it will do is it will invoke

154
00:05:48,240 --> 00:05:52,639
the malicious dispatch app code that

155
00:05:50,479 --> 00:05:54,479
darth plagueis had put in place and

156
00:05:52,639 --> 00:05:55,280
darth plagueis will have control once

157
00:05:54,479 --> 00:05:58,080
more

158
00:05:55,280 --> 00:06:00,560
in the context of a booting system where

159
00:05:58,080 --> 00:06:02,080
the locks have never had a chance to yet

160
00:06:00,560 --> 00:06:04,240
become locked

161
00:06:02,080 --> 00:06:06,800
and that is why we basically say that

162
00:06:04,240 --> 00:06:09,120
this darth vanamous vulnerability

163
00:06:06,800 --> 00:06:10,880
is a situation where it can bypass those

164
00:06:09,120 --> 00:06:14,400
things like protected range resistors it

165
00:06:10,880 --> 00:06:16,240
can bypass the d-lock bit and smm the

166
00:06:14,400 --> 00:06:18,479
t-seg for smm

167
00:06:16,240 --> 00:06:20,560
so it's actually a very very powerful

168
00:06:18,479 --> 00:06:23,520
attack

169
00:06:20,560 --> 00:06:26,000
so the defense is to set the locks via

170
00:06:23,520 --> 00:06:28,560
the s3 resume script the attack is to

171
00:06:26,000 --> 00:06:31,120
rewrite the s3 resume script if it's not

172
00:06:28,560 --> 00:06:32,800
properly protected well recognizing this

173
00:06:31,120 --> 00:06:34,560
you know some intel folks released a

174
00:06:32,800 --> 00:06:36,479
white paper saying you know uh here's

175
00:06:34,560 --> 00:06:39,199
the way that you actually should

176
00:06:36,479 --> 00:06:40,960
implement s3 resume and let's just

177
00:06:39,199 --> 00:06:43,440
augment this picture from our really

178
00:06:40,960 --> 00:06:45,120
really old specification decade ago and

179
00:06:43,440 --> 00:06:48,720
yeah you should actually save it into a

180
00:06:45,120 --> 00:06:50,319
lock box not into just a general ram so

181
00:06:48,720 --> 00:06:52,000
apologies for this blurry thing that's

182
00:06:50,319 --> 00:06:54,240
just what it looks like in the original

183
00:06:52,000 --> 00:06:56,560
document here but yeah save into a lock

184
00:06:54,240 --> 00:06:58,560
box well what is the lock box lock box

185
00:06:56,560 --> 00:07:00,319
is a concept there could be many

186
00:06:58,560 --> 00:07:02,240
different implementations of a lock box

187
00:07:00,319 --> 00:07:04,240
like the smm based lockbox a read-only

188
00:07:02,240 --> 00:07:06,400
variable lockbox or an ec embedded

189
00:07:04,240 --> 00:07:08,720
controller based lockbox

190
00:07:06,400 --> 00:07:12,000
edk2 provides a default lockbox

191
00:07:08,720 --> 00:07:14,639
implementation of the smm lockbox

192
00:07:12,000 --> 00:07:16,800
so if the attack is rewriting the s3

193
00:07:14,639 --> 00:07:20,639
resume script then the defense is

194
00:07:16,800 --> 00:07:23,039
protect the s3 resume script somehow

195
00:07:20,639 --> 00:07:24,400
and of the some house that they listed

196
00:07:23,039 --> 00:07:26,319
and they basically say you know go off

197
00:07:24,400 --> 00:07:29,520
and do it somehow it's up to you

198
00:07:26,319 --> 00:07:32,000
examples are read-only nvram so i could

199
00:07:29,520 --> 00:07:34,560
imagine that working for instance via

200
00:07:32,000 --> 00:07:37,039
using protected range registers to take

201
00:07:34,560 --> 00:07:38,800
some chunk of memory right to nvram and

202
00:07:37,039 --> 00:07:41,120
then cover it with a protected range

203
00:07:38,800 --> 00:07:41,919
register if you have any left if you

204
00:07:41,120 --> 00:07:44,240
have

205
00:07:41,919 --> 00:07:47,599
the capability to extend your existing

206
00:07:44,240 --> 00:07:48,879
ranges over the top of of some nvram

207
00:07:47,599 --> 00:07:50,639
variables

208
00:07:48,879 --> 00:07:52,479
but that would be one way that you might

209
00:07:50,639 --> 00:07:54,160
store stuff there and then lock it with

210
00:07:52,479 --> 00:07:55,360
protected range registers for instance

211
00:07:54,160 --> 00:07:57,440
so that

212
00:07:55,360 --> 00:07:58,560
plagueis cannot just come in and write

213
00:07:57,440 --> 00:08:00,639
to it

214
00:07:58,560 --> 00:08:02,240
another way could be via an embedded

215
00:08:00,639 --> 00:08:04,479
controller and that'll be just straight

216
00:08:02,240 --> 00:08:05,919
up completely proprietary however the

217
00:08:04,479 --> 00:08:07,360
vendor implements their embedded

218
00:08:05,919 --> 00:08:09,360
controller then they have to work out

219
00:08:07,360 --> 00:08:11,039
some sort of protocol between their bios

220
00:08:09,360 --> 00:08:13,120
the embedding controller both for saving

221
00:08:11,039 --> 00:08:15,120
off the resume script and for ultimately

222
00:08:13,120 --> 00:08:17,199
reading it back in and executing it at

223
00:08:15,120 --> 00:08:19,199
s3 resume time

224
00:08:17,199 --> 00:08:21,919
and then the one that was you know most

225
00:08:19,199 --> 00:08:24,639
considered is the smm lockbox

226
00:08:21,919 --> 00:08:27,280
so one if it's an smm lockbox that means

227
00:08:24,639 --> 00:08:29,759
that if you can break into smm anyway

228
00:08:27,280 --> 00:08:30,720
anyhow then that means you can rewrite

229
00:08:29,759 --> 00:08:33,440
the

230
00:08:30,720 --> 00:08:35,120
resume script right so plagueis running

231
00:08:33,440 --> 00:08:37,680
in the operating system instead of just

232
00:08:35,120 --> 00:08:39,919
writing to you know acpi non-volatile

233
00:08:37,680 --> 00:08:42,320
storage just some ram instead plagueis

234
00:08:39,919 --> 00:08:43,680
could attack up into smm and rewrite the

235
00:08:42,320 --> 00:08:46,080
boot script and then they would once

236
00:08:43,680 --> 00:08:48,959
again have this opportunity to execute

237
00:08:46,080 --> 00:08:50,959
in the context of a unlocked system

238
00:08:48,959 --> 00:08:52,800
so that's another way where again

239
00:08:50,959 --> 00:08:54,640
breaking into smm doesn't necessarily

240
00:08:52,800 --> 00:08:56,640
guarantee you can break into the bios

241
00:08:54,640 --> 00:08:59,920
but in this particular case with this

242
00:08:56,640 --> 00:09:02,480
particular configuration it might

243
00:08:59,920 --> 00:09:05,680
and then the other way and the way that

244
00:09:02,480 --> 00:09:08,800
cory and rafal chose to attack this is

245
00:09:05,680 --> 00:09:11,440
via a dispatch call out so the resume

246
00:09:08,800 --> 00:09:13,360
script itself may be protected inside of

247
00:09:11,440 --> 00:09:14,640
smm but what if it has a callout

248
00:09:13,360 --> 00:09:16,320
vulnerability

249
00:09:14,640 --> 00:09:18,080
so they said out of all of the systems

250
00:09:16,320 --> 00:09:20,240
they looked at at the time none of them

251
00:09:18,080 --> 00:09:22,320
were actually using a lock box except

252
00:09:20,240 --> 00:09:24,800
the intel developer system which was of

253
00:09:22,320 --> 00:09:27,440
course you know typical edk ii skeleton

254
00:09:24,800 --> 00:09:28,800
based so it was using the smm lockbox

255
00:09:27,440 --> 00:09:31,440
but

256
00:09:28,800 --> 00:09:34,399
bootscript in sm ram dispatch opcode

257
00:09:31,440 --> 00:09:36,720
calling out to acpi non-volatile storage

258
00:09:34,399 --> 00:09:38,959
that unprotected memory where plagueis

259
00:09:36,720 --> 00:09:41,360
could of course just stick some jump

260
00:09:38,959 --> 00:09:43,120
code that would jump out to complete

261
00:09:41,360 --> 00:09:46,080
arbitrary code execution

262
00:09:43,120 --> 00:09:49,360
and i found this particularly ironic

263
00:09:46,080 --> 00:09:51,040
in the fact that smram acpi nvram and

264
00:09:49,360 --> 00:09:55,519
shell code

265
00:09:51,040 --> 00:09:57,920
2014 december 2014 you know 2015 that's

266
00:09:55,519 --> 00:10:00,800
exactly the same diagram that we had

267
00:09:57,920 --> 00:10:02,640
back in 2009 when invisible things labs

268
00:10:00,800 --> 00:10:05,440
found their first

269
00:10:02,640 --> 00:10:07,680
call out vulnerabilities sm ram acpi

270
00:10:05,440 --> 00:10:09,360
non-volatile storage to shellcode the

271
00:10:07,680 --> 00:10:10,560
more things change the more they stay

272
00:10:09,360 --> 00:10:12,240
the same

273
00:10:10,560 --> 00:10:14,320
so the defense for that would of course

274
00:10:12,240 --> 00:10:16,480
be to audit and try to remove any of

275
00:10:14,320 --> 00:10:18,720
those sort of callouts from the thing

276
00:10:16,480 --> 00:10:20,800
and also you could imagine going to the

277
00:10:18,720 --> 00:10:23,680
smm threat tree and adding all of those

278
00:10:20,800 --> 00:10:26,720
sort of mitigations and things so if you

279
00:10:23,680 --> 00:10:29,680
used that msr that makes it so that if

280
00:10:26,720 --> 00:10:31,040
the code calls to code outside of smm

281
00:10:29,680 --> 00:10:32,959
then it's going to you know cause an

282
00:10:31,040 --> 00:10:34,399
exception that would have been helpful

283
00:10:32,959 --> 00:10:36,000
that would have just automatically you

284
00:10:34,399 --> 00:10:37,600
know if they had been using that kind of

285
00:10:36,000 --> 00:10:39,279
thing they would have found these kind

286
00:10:37,600 --> 00:10:41,120
of problems of

287
00:10:39,279 --> 00:10:42,880
dispatching to something that's outside

288
00:10:41,120 --> 00:10:44,240
of smm just automatically because it

289
00:10:42,880 --> 00:10:45,839
would have crashed when they were doing

290
00:10:44,240 --> 00:10:47,600
the development now at this point in the

291
00:10:45,839 --> 00:10:50,079
research they had code execution in the

292
00:10:47,600 --> 00:10:52,399
context of a unlocked system they didn't

293
00:10:50,079 --> 00:10:54,320
really have to break into smm in the

294
00:10:52,399 --> 00:10:56,240
original implementation especially with

295
00:10:54,320 --> 00:10:58,160
those dispatch op codes giving them easy

296
00:10:56,240 --> 00:11:01,120
access but just for the heck of it they

297
00:10:58,160 --> 00:11:03,360
decided to anyways so you know bias

298
00:11:01,120 --> 00:11:05,440
control was unlocked they could set bios

299
00:11:03,360 --> 00:11:07,279
write enable all they wanted but they

300
00:11:05,440 --> 00:11:10,000
decided to you know show that they could

301
00:11:07,279 --> 00:11:12,160
still break in just for funsies so what

302
00:11:10,000 --> 00:11:14,640
they had found in the context of the

303
00:11:12,160 --> 00:11:16,320
resume environment is that smrr's were

304
00:11:14,640 --> 00:11:19,120
actually still set and that prevented

305
00:11:16,320 --> 00:11:22,240
you know a direct write into smm via the

306
00:11:19,120 --> 00:11:24,720
cpu but t-seg was on set t-seg had

307
00:11:22,240 --> 00:11:27,440
become unlocked and then they could just

308
00:11:24,720 --> 00:11:29,120
do dma via some peripheral device this

309
00:11:27,440 --> 00:11:31,200
is a trick if you go look at some of

310
00:11:29,120 --> 00:11:32,880
rafale's papers see this is a trick he's

311
00:11:31,200 --> 00:11:33,839
been using for a long time operating

312
00:11:32,880 --> 00:11:35,680
system

313
00:11:33,839 --> 00:11:38,079
asks a peripheral such as a hard drive

314
00:11:35,680 --> 00:11:41,120
to do dma for it and it just points it

315
00:11:38,079 --> 00:11:44,000
at something like sm ram now a natural

316
00:11:41,120 --> 00:11:47,200
question might be how did bios's reset

317
00:11:44,000 --> 00:11:49,760
the registers before uefi and the answer

318
00:11:47,200 --> 00:11:51,120
is i don't know now theoretically this

319
00:11:49,760 --> 00:11:52,720
could be a research opportunity if

320
00:11:51,120 --> 00:11:55,440
you're really big into you know

321
00:11:52,720 --> 00:11:57,200
exploiting super old systems

322
00:11:55,440 --> 00:11:59,120
basically i don't know when i don't care

323
00:11:57,200 --> 00:12:01,760
at this point but you know if you really

324
00:11:59,120 --> 00:12:03,839
want it would be worth looking into

325
00:12:01,760 --> 00:12:05,680
so at this point i feel obliged to

326
00:12:03,839 --> 00:12:09,279
mention that you don't necessarily have

327
00:12:05,680 --> 00:12:11,440
to go down the s3 resume script path

328
00:12:09,279 --> 00:12:13,920
on that wake so we saw that diagram you

329
00:12:11,440 --> 00:12:16,639
know sec to pei that's s3 aware that

330
00:12:13,920 --> 00:12:18,800
goes to the bootscript executor well it

331
00:12:16,639 --> 00:12:20,240
doesn't have to be that way if it all

332
00:12:18,800 --> 00:12:23,360
really depends on the implementation of

333
00:12:20,240 --> 00:12:25,440
the bios the pei phase could just say

334
00:12:23,360 --> 00:12:26,880
you know what i see that i'm not doing a

335
00:12:25,440 --> 00:12:28,399
firmware update so i'm just going to go

336
00:12:26,880 --> 00:12:30,959
ahead and lock stuff right here right

337
00:12:28,399 --> 00:12:32,480
now before it even gets to the point of

338
00:12:30,959 --> 00:12:34,959
running the

339
00:12:32,480 --> 00:12:36,399
the bootscript executor so a biospender

340
00:12:34,959 --> 00:12:38,240
of course you know knows their own code

341
00:12:36,399 --> 00:12:39,440
knows what happens in what order knows

342
00:12:38,240 --> 00:12:40,560
whether they have to keep things

343
00:12:39,440 --> 00:12:43,120
unlocked

344
00:12:40,560 --> 00:12:44,959
the s3 boot script is useful to do all

345
00:12:43,120 --> 00:12:46,959
of that shortcutting but you know the

346
00:12:44,959 --> 00:12:48,560
real question is why do you have to wait

347
00:12:46,959 --> 00:12:50,720
until after that in order to lock your

348
00:12:48,560 --> 00:12:52,560
bios theoretically you don't it really

349
00:12:50,720 --> 00:12:55,120
just depends on the implementation

350
00:12:52,560 --> 00:12:56,880
now the attack against that would be a

351
00:12:55,120 --> 00:12:59,040
attacker could try to

352
00:12:56,880 --> 00:13:00,800
gain code execution if the thing is

353
00:12:59,040 --> 00:13:02,240
actually you know executing from ram at

354
00:13:00,800 --> 00:13:04,560
that point

355
00:13:02,240 --> 00:13:07,120
and you know game code execution before

356
00:13:04,560 --> 00:13:09,279
it actually sets those hard-coded locks

357
00:13:07,120 --> 00:13:12,399
and the defense against that would be

358
00:13:09,279 --> 00:13:14,480
using an iomu like intel vtd to defend

359
00:13:12,399 --> 00:13:16,079
against dma attacks now realistically

360
00:13:14,480 --> 00:13:18,079
what this actually should be pointing at

361
00:13:16,079 --> 00:13:19,200
is just you know break into the bios

362
00:13:18,079 --> 00:13:22,079
somehow

363
00:13:19,200 --> 00:13:24,480
that's kind of the catch-all that i used

364
00:13:22,079 --> 00:13:26,639
in the context of the original breaking

365
00:13:24,480 --> 00:13:28,320
into spy flash for instance

366
00:13:26,639 --> 00:13:30,000
uh the only reason i mentioned this is

367
00:13:28,320 --> 00:13:32,480
just to you know point out that's what

368
00:13:30,000 --> 00:13:35,839
we did at apple is we you know added in

369
00:13:32,480 --> 00:13:38,720
or by say we but corey added in the

370
00:13:35,839 --> 00:13:40,560
capability to do vtd in order to defend

371
00:13:38,720 --> 00:13:42,079
the system against all these sort of

372
00:13:40,560 --> 00:13:45,120
early attacks

373
00:13:42,079 --> 00:13:47,519
and with that we have completed

374
00:13:45,120 --> 00:13:49,600
this outline of our class we've seen the

375
00:13:47,519 --> 00:13:50,720
s3 attacks we've seen how that can

376
00:13:49,600 --> 00:13:52,399
augment

377
00:13:50,720 --> 00:13:55,120
the capability of an attacker to break

378
00:13:52,399 --> 00:13:58,160
into flash or to smm because a whole

379
00:13:55,120 --> 00:14:00,720
bunch of those locks go away when you go

380
00:13:58,160 --> 00:14:02,800
down to sleep and wake back up they of

381
00:14:00,720 --> 00:14:04,800
course have to go away for things like

382
00:14:02,800 --> 00:14:06,639
you know straight up reboot reboots but

383
00:14:04,800 --> 00:14:08,480
the novelty of these attacks is the

384
00:14:06,639 --> 00:14:10,800
recognition that even going into these

385
00:14:08,480 --> 00:14:13,040
lower power states where things like ram

386
00:14:10,800 --> 00:14:15,279
still persist and attackers can you know

387
00:14:13,040 --> 00:14:16,480
set up the environment to their liking

388
00:14:15,279 --> 00:14:18,000
before they

389
00:14:16,480 --> 00:14:21,440
go down so that they can potentially

390
00:14:18,000 --> 00:14:25,440
take control upon coming up that gives a

391
00:14:21,440 --> 00:14:25,440
very powerful capability indeed

