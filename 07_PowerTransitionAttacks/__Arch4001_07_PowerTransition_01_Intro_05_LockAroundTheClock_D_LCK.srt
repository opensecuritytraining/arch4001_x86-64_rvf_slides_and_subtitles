1
00:00:00,320 --> 00:00:04,560
all right let's look at another lock bit

2
00:00:02,320 --> 00:00:07,600
the d-lock bit which was in the context

3
00:00:04,560 --> 00:00:09,760
of smm and the restriction of you know

4
00:00:07,600 --> 00:00:11,759
being able to access the compatible

5
00:00:09,760 --> 00:00:14,080
c-seg register space all right so what

6
00:00:11,759 --> 00:00:16,400
does this one say well it says this can

7
00:00:14,080 --> 00:00:18,480
only be cleared by a full reset so

8
00:00:16,400 --> 00:00:20,880
what's full reset well we actually saw

9
00:00:18,480 --> 00:00:23,920
that already full reset was the case

10
00:00:20,880 --> 00:00:26,560
where you're using this cf-9 register

11
00:00:23,920 --> 00:00:28,960
in order to reset the cpu

12
00:00:26,560 --> 00:00:32,239
and your system reset bit needs to be

13
00:00:28,960 --> 00:00:34,880
set to 1 and then you need to set the

14
00:00:32,239 --> 00:00:37,680
full reset bit to 1 and that will be a

15
00:00:34,880 --> 00:00:40,000
full reset and what the full reset said

16
00:00:37,680 --> 00:00:42,320
was that it's going to power it's going

17
00:00:40,000 --> 00:00:44,399
to assert it's going to drive high the

18
00:00:42,320 --> 00:00:46,239
sleep s3 s4 and s5

19
00:00:44,399 --> 00:00:49,760
for three to five seconds so that it

20
00:00:46,239 --> 00:00:49,760
basically powers them all down

